<?php
class Linkset extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library(array('common', 'mysess'));
		$this->load->helper('url');
		
		$this->load->model('linkset_model');
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     pages
	////
	////
	
	public function index($userId = NULL, $page = 0) {
		if($userId != NULL  && !$this->_is_exist_user($userId)) {
			$this->_show_error(MSG_WRONG_ACCESS);
			return;
		}

		$this->_common_header(array("linkset/list"), array("sns", "linkset/search"));
		$this->_header($userId);
		$this->_bar($userId);
		$this->_tagcloud($userId);

		$wheres = $this->_make_wheres($userId);
		$this->_list($wheres, $page);

		$searchingTargetUri = $userId ? $userId."/" : $userId;
		$this->_footer($userId."/", $page, $wheres, $searchingTargetUri);
		$this->_common_footer();
	}

	public function guide() {
		if($this->mysess->loggedin() == TRUE) {
			redirect('linkset/'.$this->mysess->get_id());
			return;
		}

		$this->_common_header(array(), array("sns"));
		$this->_header();
		$this->_index();
		$this->_common_footer();
	}
	
	public function keyword($userId = NULL, $keyword = NULL, $by = NULL, $page = 0) {
		if($keyword === NULL) {
			$this->_show_error(MSG_WRONG_ACCESS);
			return;
		}

		if($userId != NULL  && !$this->_is_exist_user($userId)) {
			$this->_show_error(MSG_WRONG_ACCESS);
			return;
		}
		
		$this->_common_header(array("linkset/list"), array("sns", "linkset/search"));
		$this->_header($userId);
		$this->_bar($userId);
		$this->_tagcloud($userId);
		
		$wheres = $this->_make_wheres(urldecode($userId), NULL, urldecode($keyword), $by);
		$this->_list($wheres, $page);
		
		$uri = $userId."/keyword/".$keyword."/";
		if($by != NULL) $uri .= $by."/";
		$searchingTargetUri = $userId ? $userId."/" : $userId;
		$this->_footer($uri, $page, $wheres, $searchingTargetUri);
		$this->_common_footer();
	}

	public function tag($userId = NULL, $tag = NULL, $page = 0) {
		if($tag === NULL) {
			$this->_show_error(MSG_WRONG_ACCESS);
			return;
		}

		if($userId != NULL  && !$this->_is_exist_user($userId)) {
			$this->_show_error(MSG_WRONG_ACCESS);
			return;
		}

		if($tag !== NULL) {
			$this->_common_header(array("linkset/list"), array("sns", "linkset/search"));
			$this->_header($userId);
			$this->_bar($userId);
			$this->_tagcloud($userId);

			$wheres = $this->_make_wheres(urldecode($userId), urldecode($tag));
			$this->_list($wheres, $page);

			$searchingTargetUri = $userId ? $userId."/" : $userId;
			$this->_footer($userId."/tag/".$tag."/", $page, $wheres, $searchingTargetUri);
		}
		$this->_common_footer();
	}
	
	public function write($itemId = NULL) {
		if(($itemId !== NULL && !$this->_is_valid_item($itemId)) || ($itemId === NULL && !$this->_is_available())) {
			$this->_show_error(MSG_NO_AUTHORITY);
			return;
		}
		
		$this->_common_header(array("linkset/editor"), array("linkset/write_and_edit"));
		$this->_header($this->mysess->get_id());
		$this->_editor($itemId);
		$this->_common_footer();
	}
	
	public function delete($itemId = NULL) {
		if($itemId === NULL || !$this->_is_valid_item($itemId)) {
			$this->_show_error(MSG_NO_AUTHORITY);
			return;
		}
		
		$this->_common_header(array("linkset/delete"), array("linkset/delete"));
		$this->_header($this->mysess->get_id());
		$this->_delete($itemId);
		$this->_common_footer();	
	}

	public function admin($userId) {
		if(!$this->_is_valid_user($userId)) {
			$this->_show_error(MSG_NO_AUTHORITY);
			return;
		}

		$this->_common_header(array("linkset/admin"), array("sns", "linkset/admin"));
		$this->_header($userId);
		$this->_bar($userId);

		$this->_admin($userId);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     for ajax (diectly echo json)
	////
	////
	
	public function writing() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if($this->_is_available()) {
			$url   = $this->input->post('url');
			$title = $this->input->post('title');

			$param['invalid'] = 0;
			for($i = 0; $i < sizeof($url); $i++) {
				$tag = $this->input->post('tags'.($i+1));
				$url[$i] = urldecode($url[$i]);

				if(!$this->_is_valid_values($title[$i], $url[$i], $tag)) {
					$param['invalid'] += 1;
					continue;
				}

				if(strpos($url[$i], "://") === FALSE) {
					$url[$i] = "http://".$url[$i];
				}

				if($title[$i] == "") {
					$title[$i] = $this->_inactive_html($this->_get_title_by_url($url[$i]))	;
				}

				$this->linkset_model->put($this->mysess->get_id(), $title[$i], $url[$i], $this->input->post('tags'.($i+1)), time());
			}
			
			$param['success'] = TRUE;
			$param['userId'] = $this->mysess->get_id();
		} else {
			$param['success'] = FALSE;
		}

		$this->common->print_ajax_result($param);
	}
	
	public function editing() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$itemId = $this->input->post('item_id');
		$title = $this->input->post('title');
		$url   = $this->input->post('url');
		$title = $title[0];
		$url   = $url[0];

		if($this->_is_valid_item($itemId) && $this->_is_valid_values($title, $url, $this->input->post('tags1'))) {
			$deleteTags = $this->input->post('deletetags');
			if(is_array($deleteTags)) {
				for($i = 0; $i < sizeof($deleteTags); $i++) {
					$this->linkset_model->delete_tag($itemId, $deleteTags[$i], $this->mysess->get_id());
				}
			}

			if(strpos($url, "://") === FALSE) {
				$url = "http://".$this->input->post('url');
			}

			if($title == "") {
				$title = $this->_inactive_html($this->_get_title_by_url($url));
			}

			$this->linkset_model->edit($itemId, $this->mysess->get_id(), $title, $url, $this->input->post('tags1'));

			$param['success'] = TRUE;
			$param['userId'] = $this->mysess->get_id();
		} else {
			$param['success'] = FALSE;
		}
		
		$this->common->print_ajax_result($param);
	}
	
	public function deleting() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if($this->_is_valid_item($this->input->post('item_id'))){
			$this->linkset_model->delete($this->input->post('item_id'));
			$param['success'] = TRUE;
		} else {
			$param['success'] = FALSE;
		}
		
		$this->common->print_ajax_result($param);
	}

	public function administrating_basic() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if($this->_is_valid_user($this->input->post('userid'))) {
			$this->linkset_model->set_property($this->input->post('userid'),
				                               $this->input->post('nof_item'),
				                               $this->input->post('nof_tag'));
			$param['success'] = TRUE;
		} else {
			$param['success'] = FALSE;
		}

		$this->common->print_ajax_result($param);
	}

	public function administrating_favoritag() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$userId = $this->input->post('userid');
		$tags = $this->input->post('tags');
		if($this->_is_valid_user($userId) && $this->_is_exist_tags($userId, $tags)) {
			$this->linkset_model->delete_favoritag($userId, $this->input->post('deletetags'));
			$this->linkset_model->put_favoritag($userId, $tags);

			if(is_array($tags)) {
				for($i = 0; $i < sizeof($tags); $i++) {
					$param['tags'][$i]['name'] = $tags[$i];
					$param['tags'][$i]['tag_id'] = $this->linkset_model->get_tag_id($userId, $tags[$i]);
				}
			}

			$param['success'] = TRUE;
		} else {
			$param['success'] = FALSE;
		}

		$this->common->print_ajax_result($param);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     set datas for views
	////
	////
	
	private function _common_header($css = "", $script = "", $pageTitle = "") {
		$this->output->set_header("Content-Type: text/html; charset=UTF-8;");
		$return['pageTitle'] = ":linkset".$pageTitle;
		$return['css'][0] = "linkset/base";
		if(is_array($css)) {
			for($i = 0; $i < sizeof($css); $i++) {
				$return['css'][$i+1] = $css[$i];
			}
		}
		
		$return['script'] = $script;
		
		$return['loggedin'] = $this->mysess->loggedin();
		$return['nickname'] = $this->mysess->get_name();
		$return['id'] = $this->mysess->get_id();
		$return['needLoginLink'] = TRUE;
		$return['needJoinLink'] = TRUE;
		$return['needHomeLink'] = TRUE;
		
		$this->load->view('templates/header', $return);
	}

	private function _header($userId = NULL, $desc = NULL) {
		$return['desc'] = $desc == NULL ? LINKSET_DESCRIPTION : $desc;
		$return['writer'] = $userId;
		
		$this->load->view('linkset/header', $return);
	}

	private function _bar($userId = NULL) {
		$return['isAvailable'] = $this->_is_available();
		$return['isMyPage'] = $this->mysess->get_id() === $userId;
		$return['userId'] = $userId;
		
		$this->load->view('linkset/bar', $return);
	}
	
	private function _list($wheres = NULL, $page = 0) {
		$return['uri'] = "tag/";
		$return['showWriters'] = TRUE;
		$itemPerPage = LINKSET_DEFAULT_OF_ITEM_PER_PAGE;

		if(is_array($wheres)) {
			if(array_key_exists('user_id', $wheres) && $wheres['user_id'] != NULL) {
				$return['uri'] = $wheres['user_id']."/".$return['uri'];
				$return['showWriters'] = FALSE;

				$itemPerPage = $this->linkset_model->get_items_per_page($wheres['user_id']);
			}

			if(array_key_exists('tag', $wheres)) {
				$return['key'] = "태그";
				$return['value'] = $wheres['tag'];
			} else if(array_key_exists('keyword', $wheres)) {
				$return['key'] = "검색어";
				$return['value'] = $wheres['keyword'];
			} else {
				$return['key'] = "";
			}
		} else {
			$return['key'] = "";
		}

		$return['count'] = $this->linkset_model->get_count($wheres);
		$return['links'] = array();
		
		if($return['count'] != 0) {

			$startOfWritings = $itemPerPage * $page;     // Start of WRITings number in page

			$return['links'] = $this->linkset_model->get($wheres, $startOfWritings, $itemPerPage);
			for($i = 0; $i < sizeof($return['links']); $i++) {
				$return['links'][$i]['tags'] = $this->linkset_model->get_item_tags($return['links'][$i]['item_id']);
				$return['links'][$i]['isWriter'] = $this->_is_valid_item($return['links'][$i]['item_id']);
			}
		}
		
		$this->load->view('linkset/list', $return);
	}

	private function _footer($uri = NULL, $page = 0, $wheres = NULL, $searchingTargetUri = "") {		
		$return['needPager'] = TRUE;

		$return['first'] = NULL;
		$return['prev'] = NULL;
		$return['next'] = NULL;
		$return['last'] = NULL;
		
		$return['searchingTargetUri'] = '/linkset/'.$searchingTargetUri;
		
		if(is_array($wheres) && array_key_exists('keyword', $wheres)) {
			$return['keyword'] = $wheres['keyword'];
			$return['stChecked'] = $wheres['st'] ? "checked" : "";
			$return['scChecked'] = $wheres['sc'] ? "checked" : "";
		} else {
			$return['keyword'] = "";
			$return['stChecked'] = "checked";
			$return['scChecked'] = "";
		}

		if(is_array($wheres) && array_key_exists('user_id', $wheres) && $wheres['user_id'] != NULL) {
			$itemPerPage = $this->linkset_model->get_items_per_page($wheres['user_id']);
		} else {
			$itemPerPage = LINKSET_DEFAULT_OF_ITEM_PER_PAGE;
		}
			
		$return['uri'] = $uri."page/";

		$totalOfPage  = ceil($this->linkset_model->get_count($wheres) / $itemPerPage);
		
		if($page > 0) {
			// 주소창에 임의로 총 페이지수보다 많은 페이지를 쳤을 경우
			// 이전 페이지는 나오지 않게 한다.
			// 나와봐야 의미가 없으니까.
			if($page < $totalOfPage + 1) {
				$return['prev'][0] = $page - 1;
				$return['prevLabel'] = $return['prev'][0] + 1;
			}
			
			if($page > 1) {
				// 주소창에 임의로 총 페이지수보다 많은 페이지를 쳤을 경우
				// 이전 페이지는 나오지 않게 한다.
				// 나와봐야 의미가 없으니까.
				if($page < $totalOfPage) {
					$return['prev'][1] = $page - 2;
					$return['prevprevLabel'] = $return['prev'][1] + 1;
				}
				
				if($page > 2) {
					$return['first'] = 0;
					$return['firstLabel'] = 1;
				}
			}
		}

		if($page < $totalOfPage - 1) {
			$return['next'][0] = $page + 1;
			$return['nextLabel'] = $return['next'][0] + 1;
			
			if($page < $totalOfPage - 2) {
				$return['next'][1] = $page + 2;
				$return['nextnextLabel'] = $return['next'][1] + 1;
				
				if($page < $totalOfPage - 3) {
					$return['last'] = $totalOfPage - 1;
					$return['lastLabel']  = $totalOfPage;
				}
			}
		}
		
		$return['nowLabel'] = $page + 1;
		
		$this->load->view('linkset/footer', $return);
	}

	private function _editor($itemId = NULL) {
		if($itemId !== NULL) {
			$writing = $this->linkset_model->get(array("item_id" => $itemId));
			$writing = $writing[0];
		}
		
		$param['itemId'] = $itemId;
		$param['title']  = $itemId !== NULL ? $writing['title'] : "";
		$param['url']    = $itemId !== NULL ? $writing['url'] : "";

		$param['tags'] = NULL;
		if($itemId !== NULL) {
			$param['tags'] = $this->linkset_model->get_item_tags($itemId);
		}		

		$this->load->view('linkset/editor', $param);
	}

	private function _tagcloud($userId = NULL) {
		$tags = $this->linkset_model->get_tags($userId, $this->linkset_model->get_tags_in_menu($userId));
		$param['uri'] = $userId != NULL ? "$userId/tag" : "all/tag";

		$param['favoritags'] = $userId != NULL ? $this->linkset_model->get_favoritag($userId) : NULL;

		if(sizeof($tags) === 0) {
			$param['tags'] = array();
			return $param;
		}

		$i = 0;
		$weight[$i]['value'] = $tags[0]['weight'];
		$weight[$i]['count'] = 0;

		for($j = 1; $j < sizeof($tags); $j++) {
			if($weight[$i]['value'] == $tags[$j]['weight']) {
				$weight[$i]['count'] += 1;
			} else {
				$i += 1;
				$weight[$i]['value'] = $tags[$j]['weight'];
				$weight[$i]['count'] = 0;
			}
		}

		$i = 0;
		if(sizeof($weight) > LINKSET_TAG_LEVEL1_LIMIT + LINKSET_TAG_LEVEL2_LIMIT + LINKSET_TAG_LEVEL3_LIMIT + LINKSET_TAG_LEVEL4_LIMIT) {
			$limit = LINKSET_TAG_LEVEL1_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level1";
			}
			$limit += LINKSET_TAG_LEVEL2_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level2";
			}
			$limit += LINKSET_TAG_LEVEL3_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level3";
			}
			$limit += LINKSET_TAG_LEVEL4_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level4";
			}

		} else if(sizeof($weight) > LINKSET_TAG_LEVEL1_LIMIT + LINKSET_TAG_LEVEL2_LIMIT + LINKSET_TAG_LEVEL3_LIMIT) {
			$limit = LINKSET_TAG_LEVEL1_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level2";
			}
			$limit += LINKSET_TAG_LEVEL2_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level3";
			}
			$limit += LINKSET_TAG_LEVEL3_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level4";
			}

		} else if(sizeof($weight) > LINKSET_TAG_LEVEL1_LIMIT + LINKSET_TAG_LEVEL2_LIMIT) {
			$limit = LINKSET_TAG_LEVEL1_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level3";
			}
			$limit += LINKSET_TAG_LEVEL2_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level4";
			}

		} else if(sizeof($weight) > LINKSET_TAG_LEVEL1_LIMIT) {
			for(; $i < LINKSET_TAG_LEVEL1_LIMIT; $i++) {
				$class[$weight[$i]['value']] = "level4";
			}

		}

		for(; $i < sizeof($weight); $i++) {
			$class[$weight[$i]['value']] = "level5";
		}

		$param['tags'] = $tags;
		$param['class'] = $class;

		$this->load->view('linkset/tagcloud', $param);
	}

	private function _delete($itemId) {
		$data['itemId'] = $itemId;

		$this->load->view('linkset/delete', $data);
	}

	private function _admin($userId) {
		$result = $this->linkset_model->get_property($userId);
		$return['userId'] = $userId;
		$return['nof_item']  = $result['items_per_page'];
		$return['nof_tag']   = $result['tags_in_menu'];

		$return['tags'] = $this->linkset_model->get_favoritag($userId);

		$this->load->view('linkset/admin', $return);
	}

	private function _index() {
		$this->load->view('linkset/index');
	}

	private function _common_footer() {
		$this->load->view('templates/footer');
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     private functions
	////
	////
	
	private function _make_wheres($userId = NULL, $tag = NULL, $keyword = NULL, $by = NULL) {
		$wheres = "";
		if($userId !== NULL && $userId !== "") {
			$wheres['user_id'] = $userId;
		}

		if($tag !== NULL && $tag !== "") {
			$wheres['tag'] = $tag;
		}

		if($keyword !== NULL && $keyword != "") {
			$wheres['keyword'] = $keyword;
			if($by !== NULL) {
				$wheres['st'] = substr($by, 0,1) !== "n" ? TRUE : FALSE;
				$wheres['sc'] = substr($by, 1,1) !== "n" ? TRUE : FALSE;
			} else {
				$wheres['st'] = TRUE;
				$wheres['sc'] = TRUE;
			}
		}
		
		return $wheres;
	}
	
	private function _is_available() {
		return $this->mysess->loggedin();
	}

	private function _is_exist_user($userId) {
		return $this->linkset_model->get_property($userId) !== NULL;
	}

	private function _is_exist_tags($userId, $tags) {
		if(is_array($tags)) {
			foreach($tags as $tag) {
				if(!$this->linkset_model->is_exist_tag($userId, $tag)) {
					return FALSE;
				}
			}
		}

		return TRUE;
	}

	private function _is_valid_item($itemId) {
		return $this->mysess->loggedin() && $this->mysess->get_id() === $this->linkset_model->get_item_user($itemId);
	}

	private function _is_valid_user($userId) {
		return $this->mysess->loggedin() && $this->mysess->get_id() === $userId;
	}

	private function _is_valid_values($title, $url, $tags) {
		if($title !== NULL && mb_strlen($title) > LINKSET_TITLE_MAXLENGTH) {
			return FALSE;
		}

		if($url !== NULL && (mb_strlen($url) === 0 || mb_strlen($url) > LINKSET_URL_MAXLENGTH)) {
			return FALSE;
		}

		if(is_array($tags)) {
			foreach($tags as $tag) {
				if(mb_strlen($tag) > LINKSET_TAG_MAXLENGTH) {
					return FALSE;
				}
			}
		}

		return TRUE;
	}

	private function _show_error($errorMessage) {
		$this->load->view('templates/header', $this->_common_header());
		$this->load->view('linkset/header', $this->_header(NULL, "ERROR"));
		$this->load->view('templates/error', array("url" => LINKSET_URL, "errorMessage" => $errorMessage));
	}

	private function _get_title_by_url($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8"));
		$html = curl_exec($ch);
		curl_close($ch);

		$charset = NULL;
		$endOfMeta = 0;
		while(($startOfMeta = stripos($html, "<meta", $endOfMeta)) !== FALSE) {
			$endOfMeta = stripos($html, ">", $startOfMeta);
			$meta = substr($html, $startOfMeta, $endOfMeta - $startOfMeta);
			if(($startOfCharset = stripos($meta, "charset=")) !== FALSE) {
				$startOfCharset += 8;
				$endOfCharset = stripos($meta, "\"", $startOfCharset);
				$charset = substr($meta, $startOfCharset, $endOfCharset - $startOfCharset);
			}
		}

		$startOfTitle = stripos($html, "<title>");
		$endOfTitle = stripos($html, "</title>") - $startOfTitle;
		$title = substr($html, $startOfTitle + 7, $endOfTitle - 7);
		$title = substr($title, 0, LINKSET_TITLE_MAXLENGTH);

		if(strcasecmp($charset, "MS949") == 0) {
			$title = iconv("euc-kr", "UTF-8", $title);
		} else if(strcasecmp($charset, "euc-kr") == 0) {
			$title = iconv("euc-kr", "UTF-8", $title);
		} else if(strcasecmp($charset, "UTF-8") != 0 && $charset != NULL) {
			// ks_c_5601-1987 등;
			$title = "제목 자동 완성 기능 사용이 불가능한 페이지입니다.";
		}

		return $title != NULL ? $title : LINKSET_NO_TITLE;
	}

	private function _inactive_html($string){
		$string = stripslashes($string);
		$string = str_replace("<", "&lt;", $string);
		$string = str_replace(">", "&gt;", $string);
		$string = str_replace("\n", " ", $string);
		return $string;
	}
}
