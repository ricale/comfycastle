<?php
class Blog extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library(array('common', 'mysess', 'upload'));
		$this->load->helper('url');
		
		$this->load->model('blog_model');
		$this->load->model('blog_comment_model', 'cmt_model');

		$this->jsarray = array("comment", "sns", "hmd", "hmd.ricaleinline", "blog/using", "blog/search");
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     pages
	////
	////
	
	public function index($page = 0) {
		$this->_common_header(array("blog/view"), $this->jsarray);
		$this->_header(BLOG_DESCRIPTION);
		$this->_bar();
		$this->_contents(NULL, $page);
		$this->_footer("index", $page);
		$this->_common_footer();
	}
	
	public function article($writingId) {
		$this->_common_header(array("blog/view"), $this->jsarray);
		$this->_header(BLOG_DESCRIPTION);
		$this->_bar();
		$this->_contents(array("writingId" => $writingId));
		$this->_footer("writing", $writingId);
		$this->_common_footer();
	}
	
	public function category($category = "", $keyword = NULL, $by = NULL, $page = 0) {
		$this->_common_header(array("blog/list"), array("blog/search"));
		$this->_header(BLOG_DESCRIPTION);
		$this->_bar();
		
		if($category === "") {
			$wheres['categoryId'] = "";
		} else {
			$wheres = $this->_make_wheres(urldecode($keyword), $by, $this->blog_model->get_category_id(urldecode($category)));
		}

		$this->_list($wheres, $page);
		
		if($category === "") {
			$uri = "category/";
			$searchingTargetUri = NULL;
		} else {
			$uri = "category/".$category."/";
			$searchingTargetUri = $uri;
			if($keyword != NULL) {
				$uri .= "keyword/".$keyword."/";
				if($by != NULL) {
					$uri .= $by."/";
				}
			}

		}
		
		$this->_footer($uri, $page, $wheres, $searchingTargetUri);
		$this->_common_footer();
	}
	
	public function keyword($keyword = NULL, $by = NULL, $page = 0) {
		if($keyword === NULL) {
			$this->_show_error(MSG_WRONG_ACCESS);
			return;
		}
		
		$this->_common_header(array("blog/list"), array("blog/search"));
		$this->_header(BLOG_DESCRIPTION);
		$this->_bar();
		
		$wheres = $this->_make_wheres(urldecode($keyword), $by);
		$this->_list($wheres, $page); 
//		$this->_contents($wheres, $page);
		
		$uri = "keyword/".$keyword."/";
		if($by != NULL) $uri .= $by."/";
		$this->_footer($uri, $page, $wheres);
		$this->_common_footer();
	}

	public function tag($tag = NULL, $page = 0) {
		if($tag !== NULL) {
			$this->_common_header(array('blog/list'), array('blog/search'));
			$this->_header(BLOG_DESCRIPTION);
			$this->_bar();

			$wheres = array("tag" => urldecode($tag));
			$this->_list($wheres, $page);
			$this->_footer("tag/".$tag."/", $page, $wheres);
		} else {
			$this->_common_header(array('blog/tagcloud'), array('blog/search'));
			$this->_header(BLOG_DESCRIPTION);
			$this->_bar();
			$this->_tagcloud();
			$this->_footer_no_pager();
		}
		$this->_common_footer();
	}
	
	public function write($writingId = NULL) {
		if(!$this->_is_admin()) {
			$this->_show_error(MSG_NO_AUTHORITY);
			return;
		}
		
		$this->_common_header(array("blog/editor"), array("blog/write_and_edit"));
		$this->_header($writingId === NULL ? "[작성]" : "[수정]");
		$this->_editor($writingId);
		$this->_common_footer();
	}
	
	public function delete($writingId = NULL) {
		if(!$this->_is_admin() || $writingId === NULL) {
			$this->_show_error(MSG_NO_AUTHORITY);
			return;
		}
		
		$this->_common_header(array("blog/delete"), array("blog/delete"));
		$this->_header("[삭제]");
		$this->_delete($writingId);
		$this->_common_footer();
	}

	public function menu() {
		$this->_common_header();
		$this->_header("[설정]");
		$this->_menu();
		$this->_common_footer();
	}

	public function set_default() {
		$this->_common_header();
		$this->_header("[기본 설정]");
		$this->_common_footer();
	}

	public function set_category() {
		$this->_common_header();
		$this->_header("[카테고리 설정]");
		$this->_category();
		
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     for ajax (directly echo json)
	////
	////
	
	public function writing() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if($this->_is_admin() && $this->_is_valid_values($this->input->post('title'), 
			                                             $this->input->post('content'), 
			                                             $this->input->post('tags'))) {
			for($i = 0; $i < UPLOADS_PER_WRITING; $i++) {
				if($this->upload->do_upload('userfile'.$i)) {
					$uploadData[$i] = $this->upload->data();
				} else {
					$uploadData[$i] = NULL;
				}
				$param['uploads'][$i] = $this->upload->display_errors();
			}

			$this->blog_model->put($this->input->post('title'),
				                   $this->input->post('category_id'),
				                   $this->input->post('content'),
				                   $this->input->post('autonl'),
				                   time(),
				                   $this->input->post('tags'),
				                   $uploadData);	
			$param['success'] = TRUE;
		} else {
			$param['success'] = FALSE;
			$param['uploads'] = FALSE;
		}

		$this->common->print_ajax_result($param);
	}
	
	public function editing() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if($this->_is_admin() && $this->_is_valid_values($this->input->post('title'),
			                                             $this->input->post('content'),
			                                             $this->input->post('tags'))){

			// 사용자가 삭제한 파일을 삭제하고 새로 업로드한 파일을 업로드한다.
			for($i = 0; $i < UPLOADS_PER_WRITING; $i++) {
				if($this->input->post('deleteupload'.$i) !== FALSE && $this->input->post('deleteupload'.$i) !== "") {
					unlink(UPLOADS_PATH.$this->input->post('deleteupload'.$i));
					$this->blog_model->delete_uploaded_file($this->input->post('deleteupload'.$i));
				}

				if($this->upload->do_upload('userfile'.$i)) {
					$uploadData[$i]  = $this->upload->data();
				} else {
					$uploadData[$i]   = NULL;
				}
				$param['uploads'][$i] = $this->upload->display_errors();
			}

			$writingId = $this->input->post('writing_id');

			$deleteTags = $this->input->post('deletetags');
			if(is_array($deleteTags)) {
				for($i = 0; $i < sizeof($deleteTags); $i++) {
					$this->blog_model->delete_tag($writingId, $deleteTags[$i]);
				}
			}

			$this->blog_model->edit($writingId,
			                        $this->input->post('title'),
			                        $this->input->post('category_id'),
			                        $this->input->post('content'),
			                        $this->input->post('autonl'),
			                        time(),
			                        $this->input->post('tags'),
			                        $uploadData);
			$param['success'] = TRUE;
		} else {
			$param['success'] = FALSE;
			$param['uploads'] = FALSE;
		}
		
		$this->common->print_ajax_result($param);
	}
	
	public function deleting() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if($this->_is_admin()) {
			$uploads = $this->blog_model->get_writing_uploads($this->input->post('writing_id'));
			for($i = 0; $i < sizeof($uploads); $i++) {
				unlink(UPLOADS_PATH.$uploads[$i]['file_name']);
			}

			$this->blog_model->delete($this->input->post('writing_id'));
			$param['success'] = TRUE;
		} else {
			$param['success'] = FALSE;
		}
		
		$this->common->print_ajax_result($param);
	}
	
	public function update_comments() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$result = $this->cmt_model->get($_REQUEST['writing_id'], $_REQUEST['timestamp']);
		
		$i = 0;
		$param['content'] = NULL;
		foreach($result as $row) {
			$param['comment_id'][$i] = $row['comment_id'];
			$param['family'][$i] = $row['family'];
			$param['id'][$i]     = $row['user_id'];
			$param['times'][$i]  = $row['mod_time'];
			$param['name'][$i]   = $row['name'];
			
			$row['content'] = stripslashes($row['content']);
			$row['content'] = str_replace("<", "&lt;", $row['content']);
			$row['content'] = str_replace(">", "&gt;", $row['content']);
			$ex = "/(&lt;)(img( [a-z]+=[\"\'][^ ]*[\"\'])+[\/]?)(&gt;)/i";
			$row['content'] = preg_replace($ex, '<\\2>', $row['content']);
			$ex = "/(&lt;)(a( [a-z]+=[\"\'][^ ]*[\"\'])+)(&gt;)/i";
			$row['content'] = preg_replace($ex, '<\\2>', $row['content']);
			$ex = "/(&lt;)(\/a)(&gt;)/i";
			$row['content'] = preg_replace($ex, '<\\2>', $row['content']);
			$row['content'] = nl2br($row['content']);	
			
			$param['content'][$i] = $row['content'];
			
			$param['isdelete'][$i] = !$row['orderby'];
			$i++;
		}
		$param['session'] = $this->mysess->get_id();
		$param['isall'] = !($_REQUEST['timestamp']);
		$param['updateTime'] = $param['isall'] ? time() : NULL;
		
		$this->common->print_ajax_result($param);
	}

	public function write_comment() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if(!$this->mysess->loggedin()) {
			$id   = "";
			$name = $_REQUEST['name'];	
			$pw   = crypt($_REQUEST['pw'], PASSWORD_CRYPT);
		} else {
			$id   = $this->mysess->get_id();
			$name = $this->mysess->get_name();
			$pw   = "";
		}
		
		$ex = "#([^\"\'])(http[:]\/\/[-a-z0-9]+(\.[-a-z0-9]+)*(\/[^\\\/:\*\"<>\|\f\n\r\t\v]+(\.[^\\\/:\"<>\|\f\n\r\t\v]+)*)*\/?)#i";
		$content = preg_replace($ex, '\\1<a href="\\2" target="_blank">\\2<\/a>', $_REQUEST['content']);
		$ex = "#^http:\/\/[-a-z0-9]+(\.[-a-z0-9]+)*(\/[^\\\/:\*\"<>\|\f\n\r\t\v]+(\.[^\\\/:\"<>\|\f\n\r\t\v]+)*)*\/?#i";
		$content = preg_replace($ex, '<a href="\\0" target="_blank">\\0</a>', $content);
		
		$this->cmt_model->put($_REQUEST['writing_id'], $id, $name, $pw, $content, time());
		
		$param['isvalid'] = TRUE;
		
		$this->common->print_ajax_result($param);
	}
	
	public function edit_comment() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$commentId = $_REQUEST['comment_id'];
		$param['isvalid'] = FALSE;
		
		if($this->mysess->loggedin()) {
			if($this->cmt_model->get_comment_id($commentId) == $this->mysess->get_id()) {
				$this->cmt_model->edit($commentId, $_REQUEST['content']);
				$param['isvalid'] = TRUE;
			}
		} else {		
			if($this->cmt_model->get_comment_pw($commentId) == crypt($_REQUEST['pw'], PASSWORD_CRYPT)) {
				$this->cmt_model->edit($commentId, $_REQUEST['content']);
				$param['isvalid'] = TRUE;
			}
		}
		
		$this->common->print_ajax_result($param);
	}
	
	public function delete_comment() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$commentId = $_REQUEST['comment_id'];
		$param['isvalid'] = FALSE;
		
		if($this->mysess->loggedin()) {
			if($this->cmt_model->get_comment_id($commentId) == $this->mysess->get_id()) {
				$this->cmt_model->delete($commentId);
				$param['isvalid'] = TRUE;
			}
		} else {
			if($this->cmt_model->get_comment_pw($commentId) == crypt($_REQUEST['pw'], PASSWORD_CRYPT)) {
				$this->cmt_model->delete($commentId);
				$param['isvalid'] = TRUE;
			}
		}
		
		$this->common->print_ajax_result($param);
	}
	
	public function reply_comment() {
		if(!$this->common->is_valid_access()) {
			return;
		}
		

		if(!$this->mysess->loggedin()) {
			$id   = "";
			$name = $this->input->post('name');	
			$pw   = crypt($this->input->post('pw'), PASSWORD_CRYPT);
		} else {
			$id   = $this->mysess->get_id();
			$name = $this->mysess->get_name();
			$pw   = "";
		}
		
		$ex = "#([^\"\'])(http[:]\/\/[-a-z0-9]+(\.[-a-z0-9]+)*(\/[^\\\/:\*\"<>\|\f\n\r\t\v]+(\.[^\\\/:\"<>\|\f\n\r\t\v]+)*)*\/?)#i";
		$content = preg_replace($ex, '\\1<a href="\\2" target="_blank">\\2<\/a>', $this->input->post('content'));
		$ex = "#^http:\/\/[-a-z0-9]+(\.[-a-z0-9]+)*(\/[^\\\/:\*\"<>\|\f\n\r\t\v]+(\.[^\\\/:\"<>\|\f\n\r\t\v]+)*)*\/?#i";
		$content = preg_replace($ex, '<a href="\\0" target="_blank">\\0</a>', $content);
		
		$this->cmt_model->put($this->input->post('writing_id'), $id, $name, $pw, $content, time(), $this->input->post('parent'));
		
		$param['isvalid'] = TRUE;

		$this->common->print_ajax_result($param);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     set datas for views
	////
	////
	
	private function _common_header($css = "", $script = "", $pageTitle = "") {
		$this->output->set_header("Content-Type: text/html; charset=UTF-8;");
		$return['pageTitle'] = ":blog".$pageTitle;
		$return['css'][0] = "blog/base";
		if(is_array($css)) {
			for($i = 0; $i < sizeof($css); $i++) {
				$return['css'][$i+1] = $css[$i];
			}
		}
		
		$return['script'] = $script;
		
		$return['loggedin'] = $this->mysess->loggedin();
		$return['nickname'] = $this->mysess->get_name();
		$return['id'] = $this->mysess->get_id();
		$return['needLoginLink'] = TRUE;
		$return['needJoinLink'] = TRUE;
		$return['needHomeLink'] = TRUE;
		
		$this->load->view('templates/header', $return);
	}

	private function _header($desc) {
		$data['desc'] = $desc;

		$this->load->view('blog/header', $data);
	}
	
	private function _bar() {
		$returnData['category'] = $this->blog_model->get_categorys();

		for($i = 0; $i < sizeof($returnData['category']); $i++) {
			$returnData['category'][$i]['count'] = $this->blog_model->get_writings_count(array("categoryId" => $returnData['category'][$i]['category_id']));

			$recent = time() - $this->blog_model->get_recent_time(array("categoryId" => $returnData['category'][$i]['category_id']));
			$returnData['category'][$i]['new'] = $recent <= BLOG_RECENT_TIME;
		}
		
		$returnData['isAdmin'] = $this->_is_admin();
		if($returnData['isAdmin']) {			
			$returnData['admin'] = array(array('menu' => '글작성', 'uri' => 'write'), 
				                         array('menu' => '관리',   'uri' => 'menu'));
		}

		$this->load->view('blog/bar', $returnData);
	}
	
	private function _list($wheres = NULL, $page = 0) {
		if(is_array($wheres)) {
			if(array_key_exists('tag', $wheres)) {
				$returnData['key'] = "태그";
				$returnData['value'] = $wheres['tag'];

			} else if(array_key_exists('categoryId', $wheres)) {
				$returnData['key'] = "카테고리";
				$returnData['value'] = $wheres['categoryId'] !== "" ? $this->blog_model->get_category_name($wheres['categoryId']) : "모든글";

				if(array_key_exists('keyword', $wheres)) {
					$returnData['key'] .= ", 검색어";
					$returnData['value'] .= ", ".$wheres['keyword'];
				}

			} else if(array_key_exists('keyword', $wheres)) {
				$returnData['key'] = "검색어";
				$returnData['value'] = $wheres['keyword'];
			}
		}

		$returnData['writingsCount'] = $this->blog_model->get_writings_count($wheres);

		$returnData['items'] = array();
		
		if($returnData['writingsCount'] != 0) {
			// 카테고리 글 목록 상태이든, 검색 글 목록 상태이든, 글 내용을 보여주지 않고 글 목록만 보여주는 형식으로 바뀌었기 때문에
			// 각각의 상태에 따라 글의 수를 달리 하는 것이 의미가 없어졌다. 따라서 수정한다.
			//$writingPerPage = is_array($wheres) && array_key_exists('keyword', $wheres) ? BLOG_LISTITEM_PER_PAGE_IN_SEARCH : BLOG_LISTITEM_PER_PAGE_IN_CATEGORY;
			$writingPerPage = BLOG_LISTITEM_PER_PAGE_IN_CATEGORY;
			$startOfWritings = $writingPerPage * $page;
			
			$result = $this->blog_model->get($wheres, $startOfWritings, $writingPerPage);

			$i = 0;
			foreach($result as $row) {
				$returnData['items'][$i]['writingId'] = $row['writing_id'];
				$returnData['items'][$i]['title'] = $row['title'];
					
				$returnData['items'][$i]['time'] = date('Y.m.d', $row['time']);

				$returnData['items'][$i]['new'] = time() - $row['time'] <= BLOG_RECENT_TIME;
				
				$returnData['items'][$i]['commentsCount'] = $this->cmt_model->get_comments_count($row['writing_id']);
				$i++;
			}
		}
		
		$this->load->view('blog/list', $returnData);
	}

	private function _contents($wheres = NULL, $page = 0) {
		$totalOfWritings = $this->blog_model->get_writings_count($wheres);
		
		if($totalOfWritings != 0) {
			// writingsPerPage는 글 하나만 선택해 볼 때 (article 함수) 값이 1인것이 의미론적으로는 맞지만, 몇이든 결과가 같기 때문에 따로 (값을 1을 주는) 로직을 만들진 않는다.
			$writingPerPage = is_array($wheres) && array_key_exists('keyword', $wheres) ? BLOG_LISTITEM_PER_PAGE_IN_SEARCH : BLOG_WRITINGS_PER_PAGE;
			$startOfWritings = $writingPerPage * $page;
			
			$result = $this->blog_model->get($wheres, $startOfWritings, $writingPerPage);
			
			$i = 0;
			foreach($result as $row) {
				$returnData['writings'][$i]['writingId'] = $row['writing_id'];
				$returnData['writings'][$i]['title'] = $row['title'];
				$returnData['writings'][$i]['category'] = $this->blog_model->get_category_name($row['category_id']);
				
				if($row['autonl']) {
					$returnData['writings'][$i]['plainContent'] = "";
					$returnData['writings'][$i]['markdownContent'] = $row['content'];
				} else {
					$returnData['writings'][$i]['plainContent'] = $row['content'];
					$returnData['writings'][$i]['markdownContent'] = "";
				}
					
				$returnData['writings'][$i]['time'] = date('Y.m.d(H:i:s)', $row['time']);
				$returnData['writings'][$i]['modTime'] = date('Y.m.d(H:i:s)', $row['mod_time']);
				
				$returnData['writings'][$i]['commentsCount'] = $this->cmt_model->get_comments_count($row['writing_id']);

				$returnData['writings'][$i]['uploads'] = $this->blog_model->get_writing_uploads($row['writing_id']);

				$returnData['writings'][$i]['tags'] = $this->blog_model->get_writing_tags($row['writing_id']);
				$i++;
			}
			
			$returnData['count'] = $i;
			$returnData['isAdmin'] = $this->mysess->loggedin() && $this->_is_admin();
			
			$this->load->view('blog/contents', $returnData);
			
		} else { 
			$returnData['count'] = 0;

			$this->load->view('blog/contents', $returnData);
		}
	}

	private function _delete($writingId) {
		$data['writingId'] = $writingId;

		$this->load->view('blog/delete', $data);
	}

	private function _footer($uri, $page = 0, $wheres = NULL, $searchingTargetUri = "") {		
		$returnData['needPager'] = TRUE;

		$returnData['first'] = NULL;
		$returnData['prev'] = NULL;
		$returnData['next'] = NULL;
		$returnData['last'] = NULL;
		
		$returnData['searchingTargetUri'] = '/blog/'.$searchingTargetUri;
		
		if($wheres !== NULL && array_key_exists('keyword', $wheres)) {
			$returnData['keyword'] = $wheres['keyword'];
			$returnData['stChecked'] = $wheres['st'] ? "checked" : "";
			$returnData['scChecked'] = $wheres['sc'] ? "checked" : "";
		} else {
			$returnData['keyword'] = "";
			$returnData['stChecked'] = "checked";
			$returnData['scChecked'] = "checked";
		}
			
		if($uri == "writing") {
			$pages = $this->blog_model->get_nearby_writings($page);
			
			$returnData['uri'] = "";
			$returnData['first'] = $pages['max'];
			$returnData['prev'] = $pages['above'];
			$returnData['next'] = $pages['below'];
			$returnData['last'] = $pages['min'];
			
			$returnData['firstLabel']    = '&lt';
			$returnData['prevprevLabel'] = '&lt;';
			$returnData['prevLabel']     = '&lt;';
			$returnData['nowLabel'] = $page;
			$returnData['nextLabel']     = '&gt;';
			$returnData['nextnextLabel'] = '&gt;';
			$returnData['lastLabel']     = '&gt;';
			
		} else {
			if($uri == "index") {
				$returnData['uri'] = "page/";
			} else {
				$returnData['uri'] = $uri."page/";
			}

			if(is_array($wheres)) {
			/*	if(array_key_exists('keyword', $wheres)) {
					$writingPerPage = BLOG_LISTITEM_PER_PAGE_IN_SEARCH;
				} else {
					$writingPerPage = BLOG_LISTITEM_PER_PAGE_IN_CATEGORY;
				}
				*/
				$writingPerPage = BLOG_LISTITEM_PER_PAGE_IN_CATEGORY;
			} else {
				$writingPerPage = BLOG_WRITINGS_PER_PAGE;
			}
			
			$totalOfPage  = ceil($this->blog_model->get_writings_count($wheres) / $writingPerPage);
			
			if($page > 0) {
				if($page < $totalOfPage + 1) {
					$returnData['prev'][0] = $page - 1;
					$returnData['prevLabel'] = $returnData['prev'][0] + 1;
				}
				
				if($page > 1) {
					if($page < $totalOfPage) {
						$returnData['prev'][1] = $page - 2;
						$returnData['prevprevLabel'] = $returnData['prev'][1] + 1;
					}
					
					if($page > 2) {
						$returnData['first'] = 0;
						$returnData['firstLabel'] = 1;
					}
				}
			}
			
			if($page < $totalOfPage - 1) {
				$returnData['next'][0] = $page + 1;
				$returnData['nextLabel'] = $returnData['next'][0] + 1;
				
				if($page < $totalOfPage - 2) {
					$returnData['next'][1] = $page + 2;
					$returnData['nextnextLabel'] = $returnData['next'][1] + 1;
					
					if($page < $totalOfPage - 3) {
						$returnData['last'] = $totalOfPage - 1;
						$returnData['lastLabel']  = $totalOfPage;
					}
				}
			}
			
			$returnData['nowLabel'] = $page + 1;
		}
		
		$this->load->view('blog/footer', $returnData);
	}

	private function _footer_no_pager() {
		$param['needPager'] = FALSE;
		$param['searchingTargetUri'] = "";
		$param['keyword'] = "";
		$param['stChecked'] = "checked";
		$param['scChecked'] = "checked";

		$this->load->view('blog/footer', $param);
	}

	private function _editor($writingId = NULL) {
		if($writingId !== NULL) {
			$writing = $this->blog_model->get(array("writingId" => $writingId));
			$writing = $writing[0];
		}
		
		$param['category'] = $this->blog_model->get_categorys();
		for($i = 0; $i < sizeof($param['category']); $i++) {
			$param['category'][$i]['selected'] = $writingId !== NULL && $param['category'][$i]['category_id'] == $writing['category_id'] ? "selected" : "";
		}
		
		$param['writingId'] = $writingId;
		$param['title']      = $writingId !== NULL ? $writing['title'] : "";
		$param['anlChecked'] = $writingId === NULL || $writing['autonl'] ? "checked" : "" ;
		$param['content']    = $writingId !== NULL ? $writing['content'] : "";

		$param['uploads'] = array();
		if($writingId !== NULL) {
			$param['uploads'] = $this->blog_model->get_writing_uploads($writingId);
		}

		$param['tags'] = array();
		if($writingId !== NULL) {
			$param['tags'] = $this->blog_model->get_writing_tags($writingId);
		}		

		$this->load->view('blog/editor', $param);
	}

	private function _tagcloud() {
		$tags = $this->blog_model->get_tags();

		if(sizeof($tags) === 0) {
			$param['tags'] = array();
			return $param;
		}

		$i = 0;
		$weight[$i]['value'] = $tags[0]['weight'];
		$weight[$i]['count'] = 0;

		for($j = 1; $j < sizeof($tags); $j++) {
			if($weight[$i]['value'] == $tags[$j]['weight']) {
				$weight[$i]['count'] += 1;
			} else {
				$i += 1;
				$weight[$i]['value'] = $tags[$j]['weight'];
				$weight[$i]['count'] = 0;				
			}
		}

		if(sizeof($weight) > BLOG_TAG_LEVEL1_LIMIT + BLOG_TAG_LEVEL2_LIMIT + BLOG_TAG_LEVEL3_LIMIT + BLOG_TAG_LEVEL4_LIMIT) {
			$limit = BLOG_TAG_LEVEL1_LIMIT;
			for($i = 0; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level1";
			}
			$limit += BLOG_TAG_LEVEL2_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level2";
			}
			$limit += BLOG_TAG_LEVEL3_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level3";
			}
			$limit += BLOG_TAG_LEVEL4_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level4";
			}
			for(; $i < sizeof($weight); $i++) {
				$class[$weight[$i]['value']] = "";
			}

		} else if(sizeof($weight) > BLOG_TAG_LEVEL1_LIMIT + BLOG_TAG_LEVEL2_LIMIT + BLOG_TAG_LEVEL3_LIMIT) {
			$limit = BLOG_TAG_LEVEL1_LIMIT;
			for($i = 0; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level2";
			}
			$limit = BLOG_TAG_LEVEL2_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level3";
			}
			$limit = BLOG_TAG_LEVEL3_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level4";
			}
			for(; $i < sizeof($weight); $i++) {
				$class[$weight[$i]['value']] = "";
			}

		} else if(sizeof($weight) > BLOG_TAG_LEVEL1_LIMIT + BLOG_TAG_LEVEL2_LIMIT) {
			$limit = BLOG_TAG_LEVEL1_LIMIT;
			for($i = 0; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level3";
			}
			$limit = BLOG_TAG_LEVEL2_LIMIT;
			for(; $i < $limit; $i++) {
				$class[$weight[$i]['value']] = "level4";
			}
			for(; $i < sizeof($weight); $i++) {
				$class[$weight[$i]['value']] = "";
			}

		} else if(sizeof($weight) > BLOG_TAG_LEVEL1_LIMIT) {
			for($i = 0; $i < BLOG_TAG_LEVEL1_LIMIT; $i++) {
				$class[$weight[$i]['value']] = "level4";
			}
			for(; $i < sizeof($weight); $i++) {
				$class[$weight[$i]['value']] = "";
			}

		} else {
			for($i = 0; $i < sizeof($weight); $i++) {
				$class[$weight[$i]['value']] = "";
			}
		}

		$param['tags'] = $tags;
		$param['class'] = $class;

		$this->load->view('blog/tagcloud', $param);
	}

	private function _menu() {
		$this->load->view('blog/menu');
	}

	private function _category() {
		$param['categorys'] = $this->blog_model->get_categorys();

		$this->load->view('blog/category', $param);
	}

	private function _common_footer() {
		$this->load->view('templates/footer');
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     private functions
	////
	////
	
	private function _make_wheres($keyword, $by, $categoryId = NULL) {
		if($keyword !== NULL && $keyword != "") {
			$wheres['keyword'] = $keyword;
			if($by !== NULL) {
				$wheres['st'] = substr($by, 0,1) !== "n" ? TRUE : FALSE;
				$wheres['sc'] = substr($by, 1,1) !== "n" ? TRUE : FALSE;
			} else {
				$wheres['st'] = TRUE;
				$wheres['sc'] = TRUE;
			}
		}
		
		if($categoryId !== NULL) {
			$wheres['categoryId'] = $categoryId;
		}
		
		return $wheres;
	}
	
	private function _is_admin() {
		if($this->mysess->get_id() == BLOG_ADMIN_ID) {
			return TRUE;
		}
		return FALSE;
	}

	private function _is_valid_values($title, $content, $tags) {
		if($title == NULL || mb_strlen($title) > BLOG_TITLE_MAXLENGTH) {
			return FALSE;
		}

		if($content == NULL) {
			return FALSE;
		}

		if(is_array($tags)) {
			foreach($tags as $tag) {
				if(mb_strlen($tag) > BLOG_TAG_MAXLENGTH) {
					return FALSE;
				}
			}
		}

		return TRUE;
	}
	
	private function _show_error($errorMessage) {
		$this->_common_header();
		$this->_header("ERROR!");
		$this->load->view('templates/error', array("url" => BLOG_URL, "errorMessage" => $errorMessage));
	}
}