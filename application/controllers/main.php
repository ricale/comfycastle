<?php
class Main extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('mysess');
		$this->load->helper('url');
	}
	
	public function index() {		
		$this->_commonheader(array("main"), array("sns"));
		
		$data['menu'] = array("여기는?introduce", "블로그blog", "링크모음linkset", "마크다운에디터hme");
		$data['href'] = array("introduce", BLOG_URL, LINKSET_URL, HME_URL);

		$data['inactivate'] = array("게시판board");
		
		$this->load->view('main', $data);
		$this->load->view('templates/footer');
	}

	public function introduce() {
		$this->_commonheader(array("introduce"), array("hmd", "hmd.ricaleinline", "hme/using"));
		$this->load->view('introduce');
		$this->load->view('templates/footer');
	}

	private function _commonheader($css = "", $script = "") {
		$this->output->set_header("Content-Type: text/html; charset=UTF-8;");
		$data['pageTitle'] = "";
		$data['css']    = $css;
		$data['script'] = $script;

		$data['loggedin'] = $this->mysess->loggedin();
		$data['nickname'] = $this->mysess->get_name();
		$data['id'] = $this->mysess->get_id();
		$data['needLoginLink'] = TRUE;
		$data['needJoinLink'] = TRUE;
		$data['needHomeLink'] = TRUE;

		$this->load->view('templates/header', $data);
	}
}
