<?php
class Account extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library(array('common', 'mysess', 'email'));
		$this->load->helper('url');
		
		$this->load->model('account_model');
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     pages
	////
	////
	
	public function login($uriString = "") {
		$uri = '/'.str_replace(TOKEN_FOR_MAKE_URI_FROM_URIS, '/', $uriString);
		if($this->mysess->loggedin()) {
			redirect($uri);
		}
		
		$this->_common_header(array('account/login'), array('account/login'), "login");
		$this->_header('login', ':login');
		$this->_login($uri);
		$this->_common_footer();
	}
	
	public function logout($uriString = NULL) {
		$uri = '/'.str_replace(TOKEN_FOR_MAKE_URI_FROM_URIS, '/', $uriString);
		$this->mysess->logout();
		redirect($uri);
	}
	
	public function join() {
		if($this->mysess->loggedin()) {
			redirect("/");
		}
		
		$this->_common_header(array('account/form', 'account/join'), array('account/validation', 'account/join'), "join");
		$this->_header('join', ':account', '계정을 생성합니다.');
		$this->_join();
		$this->_common_footer();
	}

	public function menu() {
		if(!$this->mysess->loggedin()) {
			redirect("/");
		}

		$this->_common_header();
		$this->_header('menu', ':account');
		$this->_menu();
		$this->_common_footer();
	}

	public function edit() {
		if(!$this->mysess->loggedin()) {
			redirect("/");
		}

		$this->_common_header(array('account/form', 'account/join'), array('account/validation', 'account/edit'));
		$this->_header('menu', ':account','회원 정보를 수정합니다.');
		$this->_edit();
		$this->_common_footer();
	}

	public function change() {
		$this->_common_header(array('account/form'), array('account/validation', 'account/change'));
		$this->_header('menu', ':account', '비밀 번호를 변경합니다.');
		$this->_change();
		$this->_common_footer();
	}

	public function delete() {
		$this->_common_header(array('account/form'), array('account/delete'));
		$this->_header('menu', ':account', '계정을 삭제합니다.');
		$this->_delete();
		$this->_common_footer();
	}

	public function certify($key = NULL) {
		$this->_common_header(array());
		$this->_header('', ':account');

		$result = $this->_is_certify($key);
		if($result === FALSE) {
			$this->_error(SITE_URL, "인증 코드가 잘못되었거나 이미 인증되었습니다.");

		} else {
			$this->_certify($result);
		}

		$this->_common_footer();
	}

	public function never_certify() {
		$this->_common_header(array());
		$this->_header('', ':account');
		$this->_never_certified();
		$this->_common_footer();
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     for ajax (directly echo json)
	////
	////

	public function logining() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$id = $this->input->post('id');
		$pw = $this->input->post('pw');

		$result = $this->_is_valid_account($id, $pw);
		
		if($result == TRUE) {
			$param['isvalid'] = TRUE;

			$result = $this->account_model->get($id);
			if($result['certify'] != FALSE) {
				$this->mysess->login($id, $result['name']);
				$param['iscertify'] = TRUE;

			} else {
				$param['iscertify'] = FALSE;
			}

		} else {
			$param['isvalid']   = FALSE;
			$param['iscertify'] = FALSE;
		}
		
		$this->common->print_ajax_result($param);
	}

	public function joining() {
		if(!$this->common->is_valid_access()) {
			return;
		}

		
		$id = $this->input->post('id');
		$pw = $this->input->post('pw');
		$name = $this->input->post('name');
		$mail = $this->input->post('mail');
		$note = $this->input->post('note');

		if($this->_is_valid_values($id, $pw, $name, $mail)) {
			$key = $this->_crypt_certify_key($id.$mail);
			$this->account_model->insert($id, $this->_crypt_password($pw), $name, $mail, $note, $key, time());
			$param['send'] = $this->_send_mail_for_certify($name, $mail, $key);
			$param['isvalid'] = TRUE;

		} else {
			$param['isvalid'] = FALSE;
		}
		
		$this->common->print_ajax_result($param);
	}

	public function editing() {
		if(!$this->common->is_valid_access()) {
			return;
		}

		$id   = $this->input->post('id');
		$pw   = $this->input->post('pw');
		$name = $this->input->post('name');
		$mail = $this->input->post('mail');
		$note = $this->input->post('note');

		$result = $this->_is_valid_account($id, $pw);
		if($result == TRUE) {
			$origMail = $this->account_model->get_mail($id);

			if($origMail != $mail) {
				$key = $this->_crypt_certify_key($id.$mail);
				$this->account_model->update($id, $name, $note, time(), $mail, $key);
				$param['send'] = $this->_send_mail_for_certify($name, $mail, $key);

			} else {
				$this->account_model->update($id, $name, $note, time());
				$param['send'] = FALSE;
			}

			$this->mysess->refresh($name);

			$param['isvalid'] = TRUE;
		} else {
			$param['isvalid'] = FALSE;
		}

		$this->common->print_ajax_result($param);
	}

	public function changing() {
		if(!$this->common->is_valid_access()) {
			return;
		}

		$id    = $this->mysess->get_id();
		$pw    = $this->input->post('original_pw');
		$newPw = $this->_crypt_password($this->input->post('pw'));

		if($this->_is_valid_account($id, $pw)) {
			$this->account_model->update_pw($id, $newPw);
			$param['isvalid'] = TRUE;
		} else {
			$param['isvalid'] = FALSE;
		}
		
		$this->common->print_ajax_result($param);
	}

	public function deleting() {
		if(!$this->common->is_valid_access()) {
			return;
		}

		$id = $this->mysess->get_id();
		$pw = $this->input->post('pw');

		if($this->_is_valid_account($id, $pw)) {
			$this->account_model->delete($id);
			$param['isvalid'] = TRUE;
		} else {
			$param['isvalid'] = FALSE;
		}

		$this->mysess->logout();
		
		$this->common->print_ajax_result($param);
	}

	public function exist_id() {
		$id = $this->input->post('id');
		if($id == NULL) {
			return FALSE;
		}

		if(!$this->account_model->exist_id($id)) {
			$result['exist'] = FALSE;
		} else {
			$result['exist'] = TRUE;
		}

		$this->common->print_ajax_result($result);
	}

	public function exist_mail($id = NULL) {
		$mail = $this->input->post('mail');
		if($mail == NULL) {
			return FALSE;
		}

		$result = $this->account_model->exist_mail($mail);

		if($result === FALSE) {
			$param['exist'] = FALSE;

		} else {
			if($id !== NULL) {
				if($id == $result) {
					$param['exist'] = FALSE;
				} else {
					$param['exist'] = TRUE;
				}

			} else {
				$param['exist'] = TRUE;
			}
		}

		$this->common->print_ajax_result($param);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     set datas for views
	////
	////

	private function _common_header($css = NULL, $script = NULL, $pagename = "") {
		$this->output->set_header("Content-Type: text/html; charset=UTF-8;");
		
		$data['css'][0] = "account/base";
		if(is_array($css)) {
			for($i = 0; $i < sizeof($css); $i++) {
				$data['css'][$i+1] = $css[$i];
			}
		}
		$data['script'] = $script;
		
		$data['loggedin'] = $this->mysess->loggedin();
		$data['nickname'] = $this->mysess->get_name();
		$data['id'] = $this->mysess->get_id();
		$data['needLoginLink'] = $pagename == "login" ? FALSE: TRUE;
		$data['needJoinLink'] = $pagename == "join" ? FALSE: TRUE;
		$data['needHomeLink'] = TRUE;

		$data['pageTitle'] = NULL;

		$this->load->view('templates/header', $data);
	}

	private function _header($uri, $title, $desc = NULL) {
		$data['uri'] = $uri;
		$data['title'] = $title;
		$data['desc'] = $desc;

		$this->load->view('account/header', $data);
	}

	private function _login($uri) {
		$data['url'] = $uri;

		$this->load->view('account/login', $data);
	}

	private function _join() {
		$this->load->view('account/join');
	}

	private function _menu() {
		$this->load->view('account/menu');
	}

	private function _edit($id = NULL) {
		$id = $this->mysess->get_id();
		$result = $this->account_model->get($id);

		$return['id']   = $result['id'];
		$return['name'] = $result['name'];
		$return['mail'] = $result['mail'];
		$return['note'] = $result['note'];

		$this->load->view('account/edit', $return);
	}

	private function _delete() {
		$this->load->view('account/delete');
	}

	private function _change() {
		$this->load->view('account/change');
	}

	private function _error($url, $message) {
		$data["url"] = $url;
		$data["errorMessage"] = $message;
		$this->load->view('templates/error', $data);
	}

	private function _certiry($result) {
		$this->load->view('account/certify', $result);
	}

	private function _never_certified() {
		$this->load->view('account/never_certify');
	}

	private function _common_footer() {
		$this->load->view('templates/footer');
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     private functions
	////
	////

	private function _crypt_password($pw) {
		return crypt($pw, PASSWORD_CRYPT);
	}

	private function _crypt_certify_key($base) {
		return crypt($base, CERTIFY_CRYPT);
	}

	private function _is_certify($key) {
		if($key == NULL) {
			return FALSE;
		}

		if($this->account_model->exist_key($key) == FALSE) {
			return FALSE;
		} else {
			$result['name'] = $this->account_model->get_name_by_certify_key($key);
			$this->account_model->update_certify($key);
			return $result;
		}
	}

	private function _is_valid_account($id, $pw) {
		$result = $this->account_model->get_pw($id);

		if($result != FALSE) {
			$pw = $this->_crypt_password($pw);
			
			if($pw == $result) {
				return TRUE;
			}
		}
	
		return FALSE;
	}

	private function _send_mail_for_certify($name, $mail, $key) {
		$title = SITE_TITLE." 회원 가입 이메일 인증";
		$url = ACCOUNT_URL."certify/$key";
		$content = "$url
                    위의 링크를 클릭하시면 인증이 정상적으로 완료됩니다.
                    감사합니다.";
		$this->email->from('sendonly@comfycastle.net');
		$this->email->to($mail);
		$this->email->subject($title);
		$this->email->message($content);

		$this->email->send();

		return $this->email->print_debugger();
	}

	private function _is_valid_values($id, $pw, $name, $mail) {
		if($id == "" || strlen($id) < ID_MINLENGTH || strlen($id) > ID_MAXLENGTH) {
			return FALSE;
		}

		if($pw == "" || strlen($pw) > PASSWORD_MAXLENGTH) {
			return FALSE;
		}

		if($name == "" || strlen($name) > NAME_MAXLENGTH) {
			return FALSE;
		}

		if($mail == "" || strlen($mail) > MAIL_MAXLENGTH || !preg_match("/^[-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)+$/i", $mail)) {
			return FALSE;
		}

		return TRUE;
	}
}