<?php
class Hme extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library(array('common', 'mysess'));

		$this->load->helper('url');
	}

	public function index() {
		$this->_common_header(array('hme'), array('hmd', 'hmd.ricaleinline','hme/using'));
		
		$this->load->view('hme');
		$this->load->view('templates/footer');
	}

	private function _common_header($css = "", $script = "", $pageTitle = "") {
		$this->output->set_header("Content-Type: text/html; charset=UTF-8;");
		$return['pageTitle'] = ":hme".$pageTitle;
		
		$return['css']    = $css;
		$return['script'] = $script;
		
		$return['loggedin'] = $this->mysess->loggedin();
		$return['nickname'] = $this->mysess->get_name();
		$return['id'] = $this->mysess->get_id();
		$return['needLoginLink'] = TRUE;
		$return['needJoinLink'] = TRUE;
		$return['needHomeLink'] = TRUE;
		
		$this->load->view('templates/header', $return);
	}
}