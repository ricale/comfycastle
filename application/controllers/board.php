<?php
class Board extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library(array('common', 'mysess', 'upload'));
		$this->load->helper('url');
		
		$this->load->model('freeboard_model');
		$this->load->model('freeboard_comment_model', 'cmt_model');
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     pages
	////
	////

	public function index($page = 0) {
		$this->load->view('templates/header', $this->_commonheader(array("board/list"), array("board/search")));
		$this->load->view('freeboard/header', array('subTitle' => '[목록]', 'desc' => '뉴욕 헤럴드 트리뷴!'));
		$this->load->view('freeboard/bar', $this->_bar());
		$this->load->view('freeboard/list', $this->_list(NULL, $page));
		$this->load->view('freeboard/footer', $this->_footer("index", $page));
		$this->load->view('templates/footer');
	}
	
	public function article($number, $listUri = "") {
		$this->load->view('templates/header', $this->_commonheader(array("board/view"), array("comment", "board/use_commentjs")));
		$this->load->view('freeboard/header', array('subTitle' => '['.$number.'번글]', 'desc' => ''));
		$this->load->view('freeboard/bar', $this->_bar());
		$this->load->view('freeboard/content', $this->_content($number, $listUri));
		$this->load->view('templates/footer');
	}
	
	public function keyword($keyword = NULL, $by = NULL, $page = 0) {
		$this->load->view('templates/header', $this->_commonheader(array("board/list"), array("board/search")));
		$this->load->view('freeboard/header', array('subTitle' => '[검색]', 'desc' => '검색어:'.urldecode($keyword)));
		$this->load->view('freeboard/bar', $this->_bar());
		
		$wheres = $this->_make_wheres(urldecode($keyword), $by);
		$this->load->view('freeboard/list', $this->_list($wheres, $page));
		
		$uri = "keyword/".$keyword."/";
		if($by != NULL) $uri .= $by."/";
		$this->load->view('freeboard/footer', $this->_footer($uri, $page, $wheres));
		$this->load->view('templates/footer');
	}
	
	public function write($number = NULL) {
		$this->load->view('templates/header', $this->_commonheader(array("board/editor"), array("board/write_and_edit")));
		$this->load->view('freeboard/header', array('subTitle' => $number === NULL ? '[작성]' : '[수정]', 'desc' => ''));
		$this->load->view('freeboard/editor', $this->_editor($number));
		$this->load->view('templates/footer');
	}
	
	public function delete($number) {
		$this->load->view('templates/header', $this->_commonheader(array("board/delete"), array("board/delete")));
		$this->load->view('freeboard/header', array('subTitle' => '[삭제]', 'desc' => ''));
		$this->load->view('freeboard/delete', array('number' => $number, 'isLogin' => $this->mysess->loggedin()));
		$this->load->view('templates/footer');
	}
	
	public function reply($number) {
		if($this->freeboard_model->get_orderby($number) != 0) {
			$this->load->view('templates/header', $this->_commonheader("", array("board/write_and_edit")));
			$this->load->view('freeboard/header', array('subTitle' => '[답글]', 'desc' => '답글의 답글 기능은 현재 지원되지 않습니다.'));
			$this->load->view('templates/footer');
			return;
		}
		
		$this->load->view('templates/header', $this->_commonheader(array("board/editor"), array("board/write_and_edit")));
		$this->load->view('freeboard/header', array('subTitle' => '[답글]', 'desc' => $number.'번글의 답글'));
		$this->load->view('freeboard/editor', $this->_editor($number, TRUE));
		$this->load->view('templates/footer');
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     for ajax (directly echo json)
	////
	////
	
	public function writing($isReply = NULL) {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if($this->input->post('title') && $this->input->post('content')) {
			$uploadData = NULL;
			if($this->upload->do_upload()) {
				$uploadData = $this->upload->data();
			}
			$param['upload'] = $this->upload->display_errors();

			if($this->mysess->loggedin()) {
				$param['number'] = $this->freeboard_model->put(
					$this->mysess->get_id(),
					$this->mysess->get_name(),
                    NULL,
                    $this->input->post('title'),
                    $this->input->post('content'),
                    $this->input->post('autonl'),
					time(),
                    $uploadData,
                    $isReply === NULL ? NULL : $this->input->post('number'));
			} else {
				$param['number'] = $this->freeboard_model->put(
					NULL,
                    $this->input->post('name'),
                    crypt($this->input->post('pw'), PASSWORD_CRYPT),
                    $this->input->post('title'),
                    $this->input->post('content'),
                    $this->input->post('autonl'),
					time(),
                    $uploadData,
                    $isReply === NULL ? NULL : $this->input->post('number'));
			}
			
			$param['isvalid'] = TRUE;
		} else {
			$param['isvalid'] = FALSE;
			$param['upload'] = FALSE;
			$param['number'] = FALSE;
		}

		$this->common->print_ajax_result($param);
	}

	public function editing() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$param['number'] = $this->input->post('number');
		if($this->input->post('title') && $this->input->post('content')) {
			if( ($this->mysess->loggedin() && $this->freeboard_model->get_writer_id($param['number']) == $this->mysess->get_id())
			   || ($this->freeboard_model->get_writing_password($param['number']) == crypt($this->input->post('pw'), PASSWORD_CRYPT)) ) {
				$this->freeboard_model->edit($param['number'],
				                             $this->input->post('title'),
				                             $this->input->post('content'),
				                             $this->input->post('autonl'),
											 time());
				$param['isvalid'] = TRUE;
			} else {
				$param['isvalid'] = FALSE;
			}
		} else {
			$param['isvalid'] = FALSE;
		}

		$param['upload'] = FALSE;
		
		$this->common->print_ajax_result($param);
	}

	public function deleting() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$number = $this->input->post('number');
		if( ($this->mysess->loggedin() && $this->freeboard_model->get_writer_id($number) == $this->mysess->get_id())
		   || ($this->freeboard_model->get_writing_password($number) == crypt($this->input->post('pw'), PASSWORD_CRYPT)) ) {
			$this->freeboard_model->delete($number);
			$param['isvalid'] = TRUE;
		} else {
			$param['isvalid'] = FALSE;
		}
		
		$this->common->print_ajax_result($param);
	}
	
	public function update_comments() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$result = $this->cmt_model->get($this->input->post('number'), $this->input->post('timestamp'));
		
		$i = 0;
		$param['content'] = NULL;
		foreach($result as $row) {
			$param['number'][$i] = $row['number'];
			$param['family'][$i] = $row['family'];
			$param['id'][$i]     = $row['id'];
			$param['times'][$i]  = $row['time'];
			$param['name'][$i]   = $row['name'];
			
			$row['content'] = stripslashes($row['content']);
			$row['content'] = str_replace("<", "&lt;", $row['content']);
			$row['content'] = str_replace(">", "&gt;", $row['content']);
			$ex = "/(&lt;)(img( [a-z]+=[\"\'][^ ]*[\"\'])+[\/]?)(&gt;)/i";
			$row['content'] = preg_replace($ex, '<\\2>', $row['content']);
			$ex = "/(&lt;)(a( [a-z]+=[\"\'][^ ]*[\"\'])+)(&gt;)/i";
			$row['content'] = preg_replace($ex, '<\\2>', $row['content']);
			$ex = "/(&lt;)(\/a)(&gt;)/i";
			$row['content'] = preg_replace($ex, '<\\2>', $row['content']);
			$row['content'] = nl2br($row['content']);	
			
			$param['content'][$i] = $row['content'];
			
			$param['isdelete'][$i] = !$row['orderby'];
			$i++;
		}
		$param['session'] = $this->mysess->get_id();
		$param['isall'] = !($this->input->post('timestamp'));
		$param['time'] = $param['isall'] ? time() : NULL;
		
		$this->common->print_ajax_result($param);
	}

	public function write_comment() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		if(!$this->mysess->loggedin()) {
			$id   = "";
			$name = $this->input->post('name');	
			$pw   = crypt($this->input->post('pw'), PASSWORD_CRYPT);
		} else {
			$id   = $this->mysess->get_id();
			$name = $this->mysess->get_name();
			$pw   = "";
		}
		
		$ex = "#([^\"\'])(http[:]\/\/[-a-z0-9]+(\.[-a-z0-9]+)*(\/[^\\\/:\*\"<>\|\f\n\r\t\v]+(\.[^\\\/:\"<>\|\f\n\r\t\v]+)*)*\/?)#i";
		$content = preg_replace($ex, '\\1<a href="\\2" target="_blank">\\2<\/a>', $this->input->post('content'));
		$ex = "#^http:\/\/[-a-z0-9]+(\.[-a-z0-9]+)*(\/[^\\\/:\*\"<>\|\f\n\r\t\v]+(\.[^\\\/:\"<>\|\f\n\r\t\v]+)*)*\/?#i";
		$content = preg_replace($ex, '<a href="\\0" target="_blank">\\0</a>', $content);
		
		$this->cmt_model->put($this->input->post('number'), $id, $name, $pw, $content, time());
		
		$param['isvalid'] = TRUE;
		
		$this->common->print_ajax_result($param);
	}
	
	public function edit_comment() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$number = $this->input->post('number');
		$param['isvalid'] = FALSE;
		
		if($this->mysess->loggedin()) {
			if($this->cmt_model->get_comment_id($number) == $this->mysess->get_id()) {
				$this->cmt_model->edit($number, $this->input->post('content'));
				$param['isvalid'] = TRUE;
			}
		} else {		
			if($this->cmt_model->get_comment_pw($number) == crypt($this->input->post('pw'), PASSWORD_CRYPT)) {
				$this->cmt_model->edit($number, $this->input->post('content'));
				$param['isvalid'] = TRUE;
			}
		}
		
		$this->common->print_ajax_result($param);
	}
	
	public function delete_comment() {
		if(!$this->common->is_valid_access()) {
			return;
		}


		$number = $this->input->post('number');
		$param['isvalid'] = FALSE;
		
		if($this->mysess->loggedin()) {
			if($this->cmt_model->get_comment_id($number) == $this->mysess->get_id()) {
				$this->cmt_model->delete($number);
				$param['isvalid'] = TRUE;
			}
		} else {
			if($this->cmt_model->get_comment_pw($number) == crypt($this->input->post('pw'), PASSWORD_CRYPT)) {
				$this->cmt_model->delete($number);
				$param['isvalid'] = TRUE;
			}
		}
		
		$this->common->print_ajax_result($param);
	}
	
	public function reply_comment() {
		if(!$this->common->is_valid_access()) {
			return;
		}

		
		if(!$this->mysess->loggedin()) {
			$id   = "";
			$name = $this->input->post('name');	
			$pw   = crypt($this->input->post('pw'), PASSWORD_CRYPT);
		} else {
			$id   = $this->mysess->get_id();
			$name = $this->mysess->get_name();
			$pw   = "";
		}
		
		$ex = "#([^\"\'])(http[:]\/\/[-a-z0-9]+(\.[-a-z0-9]+)*(\/[^\\\/:\*\"<>\|\f\n\r\t\v]+(\.[^\\\/:\"<>\|\f\n\r\t\v]+)*)*\/?)#i";
		$content = preg_replace($ex, '\\1<a href="\\2" target="_blank">\\2<\/a>', $this->input->post('content'));
		$ex = "#^http:\/\/[-a-z0-9]+(\.[-a-z0-9]+)*(\/[^\\\/:\*\"<>\|\f\n\r\t\v]+(\.[^\\\/:\"<>\|\f\n\r\t\v]+)*)*\/?#i";
		$content = preg_replace($ex, '<a href="\\0" target="_blank">\\0</a>', $content);
		
		$this->cmt_model->put($this->input->post('number'), $id, $name, $pw, $content, time(), $this->input->post('parent'));
		
		$param['isvalid'] = TRUE;
		$this->common->print_ajax_result($param);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     set datas for views
	////
	////
	
	private function _commonheader($css = "", $script = "") {
		$this->output->set_header("Content-Type: text/html; charset=UTF-8;");
		$return['css'][0] = "board/base";
		if(is_array($css)) {
			for($i = 0; $i < sizeof($css); $i++) {
				$return['css'][$i+1] = $css[$i];
			}
		}
			
		$return['script'] = $script;
		
		$return['loggedin'] = $this->mysess->loggedin();
		$return['nickname'] = $this->mysess->get_name();
		$return['id'] = $this->mysess->get_id();
		$return['needLoginLink'] = TRUE;
		$return['needJoinLink'] = TRUE;
		$return['needHomeLink'] = TRUE;
		
		return $return;
	}
	
	private function _bar() {
		$param['menu'] = array("글작성");
		$param['href'] = array("write");
		
		return $param;
	}
	
	private function _list($wheres = NULL, $page = 0) {
		$returnData['writingsCount'] = $this->freeboard_model->get_writings_count($wheres);
		
		if($returnData['writingsCount'] != 0) {
			$writingPerPage = FREEBOARD_LISTITEM_PER_PAGE;
			$startOfWritings = $writingPerPage * $page;
			
			$result = $this->freeboard_model->get($wheres, $startOfWritings, $writingPerPage);
			
			$i = 0;
			foreach($result as $row) {
				$returnData['number'][$i] = $row['number'];
				$returnData['title'][$i] = $row['title'];
				$returnData['name'][$i] = $row['name'];
				$returnData['time'][$i] = date('Y.m.d(H:i:s)', $row['time']);
				$returnData['hit'][$i] = $row['hit'];
				
				$uriString = uri_string();
				$returnData['uri'][$i] = $row['number'];
				if($uriString != "board") {
					$returnData['uri'][$i] .= '/'.str_replace('/', TOKEN_FOR_MAKE_URI_FROM_URIS, substr($uriString, strpos($uriString, '/') + 1));
				}
				
				$returnData['commentsCount'][$i] = $this->cmt_model->get_comments_count($row['number']);
				
				$returnData['rowClass'][$i] = $row['orderby'] != 0 ? 'reply' : 'normal';
				$i++;
			}
			
			$returnData['count'] = $i;
			return $returnData;
		}
		
		$returnData['number'] = array();
		return $returnData;
	}
	
	private function _content($number, $listUri) {
		$result = $this->freeboard_model->get(array('number' => $number));
		$result = $result[0];
			
		if($result['autonl']) {
			$result['content'] = nl2br($result['content']);
		}
			
		$result['time'] = date('Y.m.d(H:i:s)', $result['time']);
		$result['commentsCount'] = $this->cmt_model->get_comments_count($result['number']);
		$result['isWritten'] = ($this->mysess->loggedin() && $this->mysess->get_id() == $result['id'])
		                       || (!$this->mysess->loggedin() && $result['id'] == "");
							   
		$result['isReply'] = $result['orderby'] != 0;
		
		$result['listUri'] = str_replace(TOKEN_FOR_MAKE_URI_FROM_URIS, '/', $listUri);
		
		return $result;
	}
	
	private function _editor($number = NULL, $isReply = FALSE) {
		if($number !== NULL) {
			$writing = $this->freeboard_model->get(array("number" => $number));
			$writing = $writing[0];
		}
		
		$param['number'] = $number;
		$param['isReply'] = $isReply;
		
		$param['isLogin'] = $this->mysess->loggedin();
		
		if(!$param['isLogin']) {
			$param['name']         = $number !== NULL && !$isReply ? $writing['name'] : "";
			$param['nameReadOnly'] = $number !== NULL && !$isReply ? "readonly" : "";
		}
		
		if($number !== NULL) {
			$param['title']   = $isReply ? BOARD_PREFIX_OF_REPLY_TITLE.$writing['title'] : $writing['title'];
			$param['content'] = $isReply ? BOARD_PREFIX_OF_REPLY_CONTENT.$writing['content'] : $writing['content'];
		} else {
			$param['title']   = "";
			$param['content'] = "";
		}
		
		$param['anlChecked'] = $number === NULL || $writing['autonl'] ? "checked" : "" ;

		return $param;
	}

	private function _footer($uri, $page = 0, $wheres = NULL, $searchingTargetUri = "") {
		$returnData['first'] = NULL;
		$returnData['prev'] = NULL;
		$returnData['next'] = NULL;
		$returnData['last'] = NULL;
		
		$returnData['searchingTargetUri'] = "/board/".$searchingTargetUri;
		
		if($wheres !== NULL && array_key_exists('keyword', $wheres)) {
			$returnData['keyword'] = $wheres['keyword'];
			$returnData['stChecked'] = $wheres['st'] ? "checked" : "";
			$returnData['scChecked'] = $wheres['sc'] ? "checked" : "";
		} else {
			$returnData['keyword'] = "";
			$returnData['stChecked'] = "checked";
			$returnData['scChecked'] = "checked";
		}
			
		if($uri == "writing") {
			$pages = $this->freeboard_model->get_nearby_writings($page);
			
			$returnData['uri'] = "";
			$returnData['first'] = $pages['max'];
			$returnData['prev'] = $pages['above'];
			$returnData['next'] = $pages['below'];
			$returnData['last'] = $pages['min'];
			
			$returnData['firstLabel']    = '&lt';
			$returnData['prevprevLabel'] = '&lt;';
			$returnData['prevLabel']     = '&lt;';
			$returnData['nowLabel'] = $page."번글";
			$returnData['nextLabel']     = '&gt;';
			$returnData['nextnextLabel'] = '&gt;';
			$returnData['lastLabel']     = '&gt;';
			
		} else {
			if($uri == "index") {
				$returnData['uri'] = "page/";
			} else {
				$returnData['uri'] = $uri."page/";
			}
			
			if($page > 0) {
				$returnData['prev'][0] = $page - 1;
				$returnData['prevLabel'] = $returnData['prev'][0] + 1;
				
				if($page > 1) {
					$returnData['prev'][1] = $page - 2;
					$returnData['prevprevLabel'] = $returnData['prev'][1] + 1;
					
					if($page > 2) {
						$returnData['first'] = 0;
						$returnData['firstLabel'] = 1;
					}
				}
			}
			
			$totalOfPage = ceil($this->freeboard_model->get_writings_count($wheres) / FREEBOARD_LISTITEM_PER_PAGE);
			if($page < $totalOfPage - 1) {
				$returnData['next'][0] = $page + 1;
				$returnData['nextLabel'] = $returnData['next'][0] + 1;
				
				if($page < $totalOfPage - 2) {
					$returnData['next'][1] = $page + 2;
					$returnData['nextnextLabel'] = $returnData['next'][1] + 1;
					
					if($page < $totalOfPage - 3) {
						$returnData['last'] = $totalOfPage - 1;
						$returnData['lastLabel']  = $totalOfPage;
					}
				}
			}
			
			$returnData['nowLabel'] = $page + 1;
		}
		
		return $returnData;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	////
	////
	////     private functions
	////
	////

	private function _make_wheres($keyword, $by, $category = NULL) {
		if($keyword !== NULL && $keyword != "") {
			$wheres['keyword'] = $keyword;
			if($by !== NULL) {
				$wheres['st'] = substr($by, 0,1) !== "n" ? TRUE : FALSE;
				$wheres['sc'] = substr($by, 1,1) !== "n" ? TRUE : FALSE;
			} else {
				$wheres['st'] = TRUE;
				$wheres['sc'] = TRUE;
			}
		}
		
		if($category !== NULL && $category != "") {
			$wheres['category'] = $category;
		}
		
		return $wheres;
	}
}