<?php


$config['upload_path'] = './uploads/';
$config['allowed_types'] = 'gif|jpg|png|zip|hwp|ppt|pptx|doc|docx|xls|xlsx|pdf|txt';
$config['max_size'] = '5120';
$config['max_filename'] = '50';
$config['encrypt_name'] = TRUE;