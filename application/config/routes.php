<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

////////// linkset

$route['linkset/all/keyword/(:any)/(:any)/page/(:num)'] = 'linkset/keyword//$1/$2/$3';
$route['linkset/all/keyword/(:any)/page/(:num)'] = 'linkset/keyword//$1//$2';
$route['linkset/all/keyword/(:any)/(:any)'] = 'linkset/keyword//$1/$2';
$route['linkset/all/keyword/(:any)'] = 'linkset/keyword//$1';
$route['linkset/all/keyword'] = 'linkset/keyword';

$route['linkset/all/tag/(:any)/page/(:num)'] = 'linkset/tag//$1/$2';
$route['linkset/all/tag/(:any)'] = 'linkset/tag//$1';
$route['linkset/all/tag'] = 'linkset/tag';

$route['linkset/all'] = 'linkset';

$route['linkset/write'] = 'linkset/write';
$route['linkset/edit/(:num)'] = 'linkset/write/$1';
$route['linkset/delete/(:num)'] = 'linkset/delete/$1';
$route['linkset/page/(:num)'] = "linkset/index//$1";
$route['linkset/writing'] = 'linkset/writing';
$route['linkset/editing'] = 'linkset/editing';
$route['linkset/deleting'] = 'linkset/deleting';
$route['linkset/administrating/basic'] = 'linkset/administrating_basic';
$route['linkset/administrating/favoritag'] = 'linkset/administrating_favoritag';

$route['linkset/(:any)/admin'] = 'linkset/admin/$1';

$route['linkset/(:any)/keyword/(:any)/(:any)/page/(:num)'] = 'linkset/keyword/$1/$2/$3/$4';
$route['linkset/(:any)/keyword/(:any)/page/(:num)'] = 'linkset/keyword/$1/$2//$4';
$route['linkset/(:any)/keyword/(:any)/(:any)'] = 'linkset/keyword/$1/$2/$3';
$route['linkset/(:any)/keyword/(:any)'] = 'linkset/keyword/$1/$2';
$route['linkset/(:any)/keyword'] = 'linkset/keyword/$1';

$route['linkset/(:any)/tag/(:any)/page/(:num)'] = 'linkset/tag/$1/$2/$3';
$route['linkset/(:any)/tag/(:any)'] = 'linkset/tag/$1/$2';
$route['linkset/(:any)/tag'] = 'linkset/tag/$1';

$route['linkset/(:any)/page/(:num)'] = 'linkset/index/$1/$2';
$route['linkset/(:any)'] = 'linkset/index/$1';

$route['linkset'] = 'linkset/guide';

////////// board

$route['board/comment/show']   = 'board/update_comments';
$route['board/comment/write']  = 'board/write_comment';
$route['board/comment/edit']   = 'board/edit_comment';
$route['board/comment/delete'] = 'board/delete_comment';
$route['board/comment/reply']  = 'board/reply_comment';

$route['board/edit/(:num)'] = 'board/write/$1';

$route['board/keyword/(:any)/(:any)/page/(:num)'] = 'board/keyword/$1/$2/$3';
$route['board/keyword/(:any)/page/(:num)'] = 'board/keyword/$1//$2';

$route['board/page/(:num)'] = "board/index/$1";
$route['board/(:num)/(:any)'] = "board/article/$1/$2";
$route['board/(:num)'] = "board/article/$1";

////////// blog

$route['blog/admin/default']  = 'blog/set_default';
$route['blog/admin/category'] = 'blog/set_category';

$route['blog/comment/show']   = 'blog/update_comments';
$route['blog/comment/write']  = 'blog/write_comment';
$route['blog/comment/edit']   = 'blog/edit_comment';
$route['blog/comment/delete'] = 'blog/delete_comment';
$route['blog/comment/reply']  = 'blog/reply_comment';

$route['blog/edit/(:num)'] = 'blog/write/$1';

$route['blog/category/(:any)/keyword/(:any)/(:any)/page/(:num)'] = 'blog/category/$1/$2/$3/$4';
$route['blog/category/(:any)/keyword/(:any)/page/(:num)'] = 'blog/category/$1/$2//$3';
$route['blog/category/(:any)/keyword/(:any)/(:any)'] = 'blog/category/$1/$2/$3';
$route['blog/category/(:any)/keyword/(:any)'] = 'blog/category/$1/$2';

$route['blog/category/(:any)/page/(:num)'] = 'blog/category/$1///$2';
$route['blog/category/page/(:num)'] = 'blog/category////$1';

$route['blog/keyword/(:any)/(:any)/page/(:num)'] = 'blog/keyword/$1/$2/$3';
$route['blog/keyword/(:any)/page/(:num)'] = 'blog/keyword/$1//$2';

$route['blog/page/(:num)'] = "blog/index/$1";
$route['blog/(:num)'] = "blog/article/$1";

////////////// account

$route['account/certify/never'] = 'account/never_certify';

$route['introduce'] = 'main/introduce';
$route['default_controller'] = 'main';

/* End of file routes.php */
/* Location: ./application/config/routes.php */