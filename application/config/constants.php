<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
 * 
 */
 
define('SITE_TITLE', 'comfycastle');
define('BLOG_TITLE', ':blog');
define('LINKSET_TITLE', ':linkset');
define('FREEBOARD_TITLE', '자유게시판');

define('SITE_URL',      'http://'.$_SERVER['HTTP_HOST']);
define('ACCOUNT_URL',   'http://'.$_SERVER['HTTP_HOST'].'/account/');
define('BLOG_URL',      'http://'.$_SERVER['HTTP_HOST'].'/blog/');
define('LINKSET_URL',   'http://'.$_SERVER['HTTP_HOST'].'/linkset/');
define('HME_URL',       'http://'.$_SERVER['HTTP_HOST'].'/hme/');
define('FREEBOARD_URL', 'http://'.$_SERVER['HTTP_HOST'].'/board/');

define('UPLOADS_URL',   'http://'.$_SERVER['HTTP_HOST'].'/uploads/');
define('IMAGE_URL',     'http://'.$_SERVER['HTTP_HOST'].'/image/');

define('UPLOADS_PATH', './uploads/');

define('PASSWORD_CRYPT', 21);
define('CERTIFY_CRYPT', 12);
define('TOKEN_FOR_MAKE_URI_FROM_URIS', '-');
define('UPLOADS_PER_WRITING', 5);

/*
 * message
 */

define('MSG_WRONG_ACCESS', "잘못된 접근입니다. URL을 확인해주세요.");
define('MSG_NO_AUTHORITY', "접근 권한이 없습니다.");

/*
 * length restriction 
 */

define('ID_MINLENGTH', 4);
define('ID_MAXLENGTH', 15);
define('PASSWORD_MAXLENGTH', 20);
define('NICK_MAXLENGTH', 15);
define('NAME_MAXLENGTH', 15);
define('MAIL_MAXLENGTH', 50);
define('TITLE_MAXLENGTH', 100);

/*
 * database table name 
 */
 
define('ACCOUNT_TABLE_NAME', 'member');
define('BLOG_TABLE_NAME',          'weblog');
define('BLOG_COMMENT_TABLE_NAME',  'weblog_cmt');
define('BLOG_CATEGORY_TABLE_NAME', 'weblog_category');
define('BLOG_UPLOADS_TABLE_NAME',  'weblog_uploads');
define('BLOG_TAGS_TABLE_NAME',     'weblog_tag');
define('BLOG_J_WRITING_TAG_TABLE_NAME','weblog_j_writing_tag');
define('FREEBOARD_TABLE_NAME',         'freeboard');
define('FREEBOARD_COMMENT_TABLE_NAME', 'freeboard_cmt');
define('FREEBOARD_UPLOADS_TABLE_NAME', 'freeboard_uploads');

/*
 * blog 
 */
 
define('BLOG_WRITINGS_PER_PAGE', 1);
define('BLOG_LISTITEM_PER_PAGE_IN_SEARCH', 5);
define('BLOG_LISTITEM_PER_PAGE_IN_CATEGORY', 20);
define('BLOG_ADMIN_ID', 'ricale');
define('BLOG_DESCRIPTION', 'written by ricale');
define('BLOG_TAG_LEVEL1_LIMIT', 1);
define('BLOG_TAG_LEVEL2_LIMIT', 2);
define('BLOG_TAG_LEVEL3_LIMIT', 3);
define('BLOG_TAG_LEVEL4_LIMIT', 4);
define('BLOG_TITLE_MAXLENGTH', 100);
define('BLOG_TAG_MAXLENGTH', 20);
define('BLOG_RECENT_TIME', 86400);

/*
 * linkset
 */

define('LINKSET_TABLE_NAME',      'link');
define('LINKSET_TAGS_TABLE_NAME', 'link_tag');
define('LINKSET_TAG_OWNER_TABLE_NAME', 'link_tag_owner');
define('LINKSET_FAVORITAG_TABLE_NAME', 'link_favoritag');
define('LINKSET_PROPERTY_TABLE_NAME',   'link_property');
define('LINKSET_J_ITEM_TAG_TABLE_NAME', 'link_j_item_tag');

define('LINKSET_ADMIN_ID', 'ricale');
define('LINKSET_DESCRIPTION', "유용한 링크들을 모아둡니다.");
define('LINKSET_DEFAULT_OF_ITEM_PER_PAGE', 20);
define('LINKSET_TAG_LEVEL1_LIMIT', 2);
define('LINKSET_TAG_LEVEL2_LIMIT', 3);
define('LINKSET_TAG_LEVEL3_LIMIT', 2);
define('LINKSET_TAG_LEVEL4_LIMIT', 1);
define('LINKSET_NO_TITLE', "제목없음");
define('LINKSET_TITLE_MAXLENGTH', 100);
define('LINKSET_URL_MAXLENGTH', 255);
define('LINKSET_TAG_MAXLENGTH', 20);

define('LINKSET_MIN_OF_ITEMS_PER_PAGE',  5);
define('LINKSET_MAX_OF_ITEMS_PER_PAGE', 50);
define('LINKSET_MIN_OF_TAGS_IN_MENU',  5);
define('LINKSET_MAX_OF_TAGS_IN_MENU', 50);

/*
 * freeboard
 */
 
define('FREEBOARD_LISTITEM_PER_PAGE', 10);
define('BOARD_PREFIX_OF_REPLY_TITLE', "RE: ");
define('BOARD_PREFIX_OF_REPLY_CONTENT', "\n\n\n\n\n\n\n--------------------[원본글]--------------------\n\n");


/* End of file constants.php */
/* Location: ./application/config/constants.php */
