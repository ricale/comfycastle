<?php
class Blog_model extends CI_Model {

	public function __construct() {
		$this->load->database();

		$this->blogTable = BLOG_TABLE_NAME;
		$this->tagTable  = BLOG_TAGS_TABLE_NAME;
		$this->cateTable = BLOG_CATEGORY_TABLE_NAME;
		$this->uploTable = BLOG_UPLOADS_TABLE_NAME;
		$this->commTable = BLOG_COMMENT_TABLE_NAME;
		$this->blogTagJoinTable = BLOG_J_WRITING_TAG_TABLE_NAME;
	}
	
	public function get_writings_count($wheres) {
		if(is_array($wheres) && array_key_exists('tag', $wheres)) {
			$wheres['tag'] = addslashes($wheres['tag']);
			$query = $this->db->query("SELECT weight
				                       FROM   $this->tagTable
				                       WHERE  name = '${wheres['tag']}'");

			if($query->num_rows() === 0) {
				return 0;
			}

			$result = $query->row_array();
			return $result['weight'];
			
		} else {
			$query = $this->db->query("SELECT COUNT(*)
				                       FROM   $this->blogTable ".
				                       $this->_make_where($wheres));

			$result = $query->row_array();
			return $result["COUNT(*)"];
		}
	}
	
	public function get_nearby_writings($writingId) {
		$query = $this->db->query("SELECT writing_id
			                       FROM   $this->blogTable
			                       WHERE  writing_id > $writingId
			                       ORDER BY writing_id
			                       LIMIT 0, 2");

		if($query->num_rows() == 2) {
			$result = $query->result_array();
			$above[0] = $result[0]['writing_id'];
			$above[1] = $result[1]['writing_id'];
			
			$query = $this->db->query("SELECT MAX(writing_id)
				                       FROM   $this->blogTable");

			$result = $query->row_array();
			$max = $result['MAX(writing_id)'];
			if($max == $above[1]) {
				$max = NULL;
			}
			
		} else if($query->num_rows() == 1) {
			$result = $query->result_array();
			$above[0] = $result[0]['writing_id'];
			$max = NULL;
			
		} else {
			$above = NULL;
			$max = NULL; 
		}
		
		$query = $this->db->query("SELECT writing_id
			                       FROM   $this->blogTable
			                       WHERE  writing_id < $writingId
			                       ORDER BY writing_id DESC
			                       LIMIT 0, 2");

		if($query->num_rows() == 2) {
			$result = $query->result_array();
			$below[0] = $result[0]['writing_id'];
			$below[1] = $result[1]['writing_id'];
			
			$query = $this->db->query("SELECT MIN(writing_id)
				                       FROM   $this->blogTable");

			$result = $query->row_array();
			$min = $result['MIN(writing_id)'];
			if($min == $below[0]) {
				$min = NULL;
			}
			
		} elseif($query->num_rows() == 1) {
			$result = $query->result_array();
			$below[0] = $result[0]['writing_id'];
			$min = NULL;
			
		} else {
			$below = NULL;
			$min = NULL; 
		}
		
		$data['max'] = $max;
		$data['min'] = $min;
		$data['above'] = $above;
		$data['below'] = $below;
		
		return $data;
	}
	
	public function get_categorys() {
		$query = $this->db->query("SELECT *
			                       FROM   $this->cateTable");

		$result = $query->result_array();
		
		return $result; 
	}
	
	public function get_category_name($categoryId) {
		$query = $this->db->query("SELECT name
			                       FROM   $this->cateTable
			                       WHERE  category_id = '$categoryId'");

		if($query->num_rows() === 0) {
			return FALSE;
		}

		$result = $query->row_array();
		return $result['name'];
	}
	
	public function get_category_id($name) {
		$query = $this->db->query("SELECT category_id
			                       FROM   $this->cateTable
			                       WHERE  name = '$name'");

		if($query->num_rows() === 0) {
			return FALSE;
		}
		
		$result = $query->row_array();
		return $result['category_id']; 
	}

	public function get_writing_uploads($writingId) {
		$query = $this->db->query("SELECT *
			                       FROM   $this->uploTable
			                       WHERE  writing_id = '$writingId'");

		if($query->num_rows() == 0) {
			return NULL;
		}

		return $query->result_array();
	}

	public function get_writing_tags($writingId) {
		$query = $this->db->query("SELECT tag_id, name, weight
			                       FROM   $this->tagTable
			                       WHERE  tag_id IN (SELECT tag_id
			                       	                 FROM $this->blogTagJoinTable
			                       	                 WHERE writing_id = $writingId)");

		if($query->num_rows() === 0) {
			return array();
		}

		return $query->result_array();
	}

	public function get_tags() {
		$query = $this->db->query("SELECT *
			                       FROM   $this->tagTable
			                       ORDER BY weight DESC, name");

		$result = $query->result_array();
		return $result;
	}

	public function get_recent_time($wheres) {
		$where = $this->_make_where($wheres);
		$query = $this->db->query("SELECT time
			                       FROM $this->blogTable 
			                       $where
			                       ORDER BY time DESC
			                       LIMIT 1");

		if($query->num_rows() == 0) {
			return NULL;
		}

		$result = $query->row_array();
		return $result['time'];
	}
	
	public function get($wheres, $start = 0, $count = 1) {
		if(is_array($wheres) && array_key_exists('tag', $wheres)) {
			$tagname = addslashes($wheres['tag']);

			$query = $this->db->query("SELECT DISTINCT writing_id, title, content, time, mod_time
				                       FROM $this->blogTable
				                       WHERE writing_id IN (SELECT writing_id
				                       	                    FROM   $this->blogTagJoinTable
				                       	                    WHERE  tag_id IN (SELECT tag_id
				                       	                    	              FROM $this->tagTable
				                       	                    	              WHERE name = '$tagname'))
			                           ORDER BY writing_id DESC");

			if($query->num_rows() === 0) {
				return NULL;
			}

			return $query->result_array();

		} else {
			$where = $this->_make_where($wheres);
			$query = $this->db->query("SELECT *
				                       FROM $this->blogTable 
				                       $where
				                       ORDER BY writing_id DESC
				                       LIMIT $start, $count");

			$result = $query->result_array();
		}
		
		return $result;
	}
	
	public function put($title, $categoryId, $content, $autonl, $time, $tags, $uploadData) {
		$title   = addslashes($title);
		$content = addslashes($content);

		$this->db->query("INSERT INTO $this->blogTable (title, category_id, content, autonl, time, mod_time)
				          VALUES ('$title', '$categoryId', '$content', '$autonl', '$time', '$time');");
		
		$query = $this->db->query("SELECT writing_id
			                       FROM   $this->blogTable
			                       WHERE  title = '$title'
			                              AND content = '$content'
			                              AND time = '$time'");

		$result = $query->row_array();

		$this->_put_tags($result['writing_id'], $tags);
		$this->_put_uploads_data($result['writing_id'], $uploadData);
	}
	
	public function edit($writingId, $title, $categoryId, $content, $autonl, $modTime, $tags, $uploadData) {
		$title   = addslashes($title);
		$content = addslashes($content);

		$this->db->query("UPDATE $this->blogTable
			              SET    title = '$title', category_id = '$categoryId', content = '$content', autonl = '$autonl', mod_time = '$modTime'
			              WHERE  writing_id = '$writingId'");

		$this->_put_tags($writingId, $tags);
		$this->_put_uploads_data($writingId, $uploadData);
	}
	
	public function delete($writingId) {
		$query = $this->db->query("SELECT tag_id
			                       FROM $this->blogTagJoinTable
			                       WHERE writing_id = '$writingId'");
		
		$result = $query->result_array();
		foreach($result as $row) {
			$this->delete_tag($writingId, $row['tag_id']);
		}

		$this->db->query("DELETE FROM $this->uploTable
			              WHERE writing_id = '$writingId'");
		$this->db->query("DELETE FROM $this->commTable
			              WHERE writing_id = '$writingId'");
		$this->db->query("DELETE FROM $this->blogTable
			              WHERE writing_id = '$writingId'");
	}

	// 
	public function delete_tag($writingId, $tagId) {
		$this->db->query("DELETE FROM $this->blogTagJoinTable
			              WHERE writing_id = '$writingId' AND tag_id = '$tagId'");

		$query = $this->db->query("SELECT weight
			                       FROM $this->tagTable
			                       WHERE tag_id = '$tagId'");

		$result = $query->row_array();
		if($result['weight'] == 1) {
			$this->db->query("DELETE FROM $this->tagTable
				              WHERE tag_id = '$tagId'");
		} else {
			$this->db->query("UPDATE $this->tagTable
				              SET    weight = weight-1
				              WHERE  tag_id = '$tagId'");
		}
	}

	public function delete_uploaded_file($filename) {
		//모델에서 삭제하면 permission denied가 뜬다. 왜?
		//unlink(UPLOADS_PATH.$this->input->post($filename));
		$filename = addslashes($filename);
		$this->db->query("DELETE FROM $this->uploTable
			              WHERE file_name = '$filename'");
	}

	// return, break, continue 삼대장이 모여있는 함수
	private function _put_tags($writingId, $tags) {
		if(!is_array($tags)) {
			return;
		}

		$tags = array_unique($tags);

		foreach($tags as $tag) {
			$tag = addslashes($tag);

			// 쿼리 결과가 없다. = 해당 아이디의 해당 태그는 존재하지 않는다.
			// 쿼리 결과 값이 있으나 결과값 중 1은 없다. = 해당 아이디의 해당 태그는 존재하나 이 글에는 없다.
			// 쿼리 결과 값이 있고 결과값 중 1이 있다. = 해당 아이디의 해당 태그가 이 글에 존재한다.
			$query = $this->db->query("SELECT DISTINCT writing_id = $writingId r
				                       FROM   $this->tagTable
				                       NATURAL JOIN $this->blogTagJoinTable
				                       WHERE  name = '$tag'");

			$result = $query->result_array();

			$alreadyExist = FALSE;
			foreach($result as $r) {
				if(array_search(1, $r) !== FALSE) {
					$alreadyExist = TRUE;
					break;
				}
			}

			if($alreadyExist) {
				continue;
			}

			if(sizeof($result) == 0) {
				$this->db->query("INSERT INTO $this->tagTable (name)
					              VALUES ('$tag')");
			} else {
				$this->db->query("UPDATE $this->tagTable
					              SET    weight = weight + 1
					              WHERE  name = '$tag'");
			}

			$query = $this->db->query("INSERT INTO $this->blogTagJoinTable (writing_id, tag_id)
				                       VALUES ('$writingId', (SELECT tag_id
				                       					   FROM   $this->tagTable
			                                               WHERE  name = '$tag'))");

		}
	}

	private function _put_uploads_data($writingId, $uploadData) {
		for($i = 0; $i < sizeof($uploadData); $i++) {
			if(is_array($uploadData[$i])) {
				$array = $uploadData[$i];
				$array['file_name'] = addslashes($array['file_name']);
				$array['orig_name'] = addslashes($array['orig_name']);

				$this->db->query("INSERT INTO $this->uploTable
					              (writing_id, file_name, file_type, file_size, orig_name, is_image, image_width, image_height, image_type)
				                  VALUES ('$writingId', '${array['file_name']}', '${array['file_type']}', '${array['file_size']}',
				                  	      '${array['orig_name']}', '${array['is_image']}',
				                  	      '${array['image_width']}', '${array['image_height']}', '${array['image_type']}')");
			}
		}
	}

	private function _make_where($wheres) {
		$where = "";
		if(is_array($wheres)) {
			if(array_key_exists('writingId', $wheres)) {
				$where = "WHERE writing_id='${wheres['writingId']}'";

			} else if((array_key_exists('categoryId', $wheres) && $wheres['categoryId'] !== "") || (array_key_exists('keyword', $wheres))) {
				$linker = "WHERE";
				
				if(array_key_exists('categoryId', $wheres)) {
					$where .= "$linker category_id='${wheres['categoryId']}' ";
					$linker = "AND (";
				}
				
				if(array_key_exists('keyword', $wheres)) {
					$wheres['keyword'] = addslashes($wheres['keyword']);

					if($wheres['st']) {// 검색 옵션에 제목으로 찾기가 포함되어 있다.
						$where .= "$linker title LIKE '%${wheres['keyword']}%' ";
						$linker = "OR";
					}
			
					if($wheres['sc']) {// 검색 옵션에 작성 내용	으로 찾기가 포함되어 있다.
						$where .= "$linker content LIKE '%${wheres['keyword']}%'";
					}
					
					if(array_key_exists('categoryId', $wheres)) {
						$where .= ")";
					}
				}
			}
			
		}
				 
		return $where;
	}
}