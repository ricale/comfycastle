<?php
class Account_model extends CI_Model {

	public function __construct() {
		$this->load->database();

		$this->accountTable = ACCOUNT_TABLE_NAME;
		$this->linkPropertyTable = LINKSET_PROPERTY_TABLE_NAME;
	}
	
	public function exist_id($id) {
		$query = $this->db->query("SELECT user_id
			                       FROM $this->accountTable
			                       WHERE user_id='$id'");
		
		if($query->num_rows() === 0) {
			return FALSE;
		}
		
		return TRUE;
	}

	public function exist_key($key) {
		$query = $this->db->query("SELECT user_id
			                       FROM $this->accountTable
			                       WHERE ckey='$key'");
		
		if($query->num_rows() === 0) {
			return FALSE;
		}
		
		return TRUE;
	}

	public function exist_mail($mail) {
		$query = $this->db->query("SELECT user_id
			                       FROM $this->accountTable
			                       WHERE mail='$mail'");

		if($query->num_rows() === 0) {
			return FALSE;
		}

		$result = $query->row_array();
		return $result['user_id'];
	}

	public function get_mail($id) {
		$query = $this->db->query("SELECT mail
			                       FROM $this->accountTable
			                       WHERE user_id='$id'");

		if($query->num_rows() === 0) {
			return FALSE;
		}
		
		$result = $query->row_array();
		return $result['mail'];
	}

	public function get_pw($id) {
		$query = $this->db->query("SELECT pw
			                       FROM $this->accountTable
			                       WHERE user_id='$id'");

		if($query->num_rows() === 0) {
			return FALSE;
		}
		
		$result = $query->row_array();
		return $result['pw'];
	}

	public function get_name_by_certify_key($key) {
		$query = $this->db->query("SELECT name
			                       FROM $this->accountTable
			                       WHERE ckey='$key'");

		if($query->num_rows() !== 1) {
			return FALSE;
		}
		
		$result = $query->row_array();
		return $result['name'];
	}

	public function get($id) {
		$query = $this->db->query("SELECT *
			                       FROM $this->accountTable
			                       WHERE user_id='$id'");

		if($query->num_rows() === 0) {
			return FALSE;
		}
		
		return $query->row_array();
	}
	
	public function insert($id, $pw, $name, $mail, $note, $key, $time) {		
		$this->db->query("INSERT INTO $this->accountTable (user_id, pw, name, mail, note, ckey, time, mod_time)
	                      VALUES ('$id', '$pw', '$name', '$mail', '$note', '$key', '$time', '$time')");

		$this->db->query("INSERT INTO $this->linkPropertyTable (user_id)
			              VALUES ('$id')");
	}

	public function update_certify($key) {
		$this->db->query("UPDATE $this->accountTable
			              SET ckey='', certify=true
			              WHERE ckey='$key'");
	}

	public function update_decertify($id, $key) {
		$this->db->query("UPDATE $this->accountTable
			              SET ckey='$key', certify=false
			              WHERE user_id='$id'");
	}

	public function update_pw($id, $pw) {
		$this->db->query("UPDATE $this->accountTable
			              SET pw='$pw'
			              WHERE user_id='$id'");
	}

	public function update($id, $name, $note, $time, $mail = NULL, $key = NULL) {
		if($mail != NULL) {
			$this->db->query("UPDATE $this->accountTable
				              SET name='$name', mail='$mail', note='$note', ckey='$key', certify=false, mod_time='$time'
				              WHERE user_id='$id'");

			return $key;
		} else {
			$this->db->query("UPDATE $this->accountTable
				              SET name='$name', note='$note', mod_time='$time'
				              WHERE user_id='$id'");

			return FALSE;
		}
	}

	public function delete($id) {
		$this->db->query("DELETE FROM $this->accountTable
			              WHERE user_id='$id'");
	}
}
