<?php
class Freeboard_comment_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
	
	public function get_comments_count($writing) {
		$query = $this->db->query("SELECT COUNT(*) FROM ".FREEBOARD_COMMENT_TABLE_NAME." WHERE writing=$writing AND orderby>0");
		
		$result = $query->row_array();
		return $result["COUNT(*)"];
	}
	
	public function get_comment_id($number) {
		$query = $this->db->query("SELECT id FROM ".FREEBOARD_COMMENT_TABLE_NAME." WHERE number = $number");
		
		$result = $query->row_array();
		return $result['id'];
	}
	
	public function get_comment_pw($number) {
		$query = $this->db->query("SELECT pw FROM ".FREEBOARD_COMMENT_TABLE_NAME." WHERE number = $number");
		
		$result = $query->row_array();
		return $result['pw'];
	}
	
	public function get($number, $lastModifTime) {
		$query = $this->db->query("SELECT * FROM ".FREEBOARD_COMMENT_TABLE_NAME." WHERE writing=$number AND time>$lastModifTime ORDER BY family, orderby");
		$result = $query->result_array();
		
		return $result;
	}
	
	public function put($writing, $id, $name, $pw, $content, $time, $parentNumber = NULL) {
		if($parentNumber === NULL) {
			$family = $this->_get_new_family();
			$query = $this->db->query("INSERT INTO ".FREEBOARD_COMMENT_TABLE_NAME." (writing, family, orderby, id, name, pw, content, time)
		                                          VALUES ('$writing', '$family', '1', '$id', '$name', '$pw', '$content', '$time')");
		} else {
			$orderby = $this->_get_order_infamily($parentNumber);
			$this->db->query("INSERT INTO ".FREEBOARD_COMMENT_TABLE_NAME." (writing, family, orderby, id, name, pw, content, time)
		                                          VALUES ('$writing', '$parentNumber', '$orderby', '$id', '$name', '$pw', '$content', '$time')");
		}
	}
	
	public function edit($number, $content) {
		$time = time();
		$query = $this->db->query("UPDATE ".FREEBOARD_COMMENT_TABLE_NAME." SET content='$content', time='$time' WHERE number='$number'");
	}
	
	public function delete($number) {
		$time = time();
		$query = $this->db->query("UPDATE ".FREEBOARD_COMMENT_TABLE_NAME." SET orderby=0, time='$time' WHERE number = $number");
	}
	
	private function _get_new_family() {
		$query = $this->db->query("SELECT MAX(family) FROM ".FREEBOARD_COMMENT_TABLE_NAME);
		$result = $query->row_array();
		return $result['MAX(family)'] + 1;
	}

	private function _get_parent_family($number) {
		$query = $this->db->query("SELECT MIN(family) FROM ".FREEBOARD_COMMENT_TABLE_NAME." WHERE number=$number");
		$result = $query->row_array();
		return $result['MIN(family)'];
	}
	
	private function _get_order_infamily($family) {
		$query = $this->db->query("SELECT MAX(orderby) FROM ".FREEBOARD_COMMENT_TABLE_NAME." WHERE family=$family");
		$result = $query->row_array();
		return $result['MAX(orderby)'] + 1;
	}
}

