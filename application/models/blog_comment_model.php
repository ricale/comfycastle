<?php
class Blog_comment_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
	
	public function get_comments_count($writingId) {
		$query = $this->db->query("SELECT COUNT(*)
			                       FROM ".BLOG_COMMENT_TABLE_NAME."
			                       WHERE writing_id='$writingId'
			                       AND orderby>0");
		
		$result = $query->row_array();
		return $result["COUNT(*)"];
	}
	
	public function get_comment_id($commentId) {
		$query = $this->db->query("SELECT user_id
			                       FROM ".BLOG_COMMENT_TABLE_NAME."
			                       WHERE comment_id='$commentId'");

		if($query->num_rows() === 0) {
			return FALSE;
		}
		
		$result = $query->row_array();
		return $result['user_id'];
	}
	
	public function get_comment_pw($commentId) {
		$query = $this->db->query("SELECT pw
			                       FROM ".BLOG_COMMENT_TABLE_NAME."
			                       WHERE comment_id='$commentId'");

		if($query->num_rows() === 0) {
			return FALSE;
		}
		
		$result = $query->row_array();
		return $result['pw'];
	}
	
	public function get($writingId, $lastModifTime) {
		$query = $this->db->query("SELECT *
			                       FROM ".BLOG_COMMENT_TABLE_NAME."
			                       WHERE writing_id='$writingId'
			                       AND mod_time > $lastModifTime
			                       ORDER BY orderby");

		$result = $query->result_array();
		
		return $result;
	}
	
	public function put($writingId, $userId, $name, $pw, $content, $time, $parent = NULL) {
		if($parent === NULL) {
			$family = $this->_get_new_family();
			$query = $this->db->query("INSERT INTO ".BLOG_COMMENT_TABLE_NAME." (writing_id, family, orderby, user_id, name, pw, content, time, mod_time)
		                               VALUES ('$writingId', '$family', '1', '$userId', '$name', '$pw', '$content', '$time', '$time')");
		} else {
			$orderby = $this->_get_order_infamily($parent);
			$this->db->query("INSERT INTO ".BLOG_COMMENT_TABLE_NAME." (writing_id, family, orderby, user_id, name, pw, content, time, mod_time)
		                      VALUES ('$writingId', '$parent', '$orderby', '$userId', '$name', '$pw', '$content', '$time', '$time')");
		}
	}
	
	public function edit($commentId, $content, $time) {
		$query = $this->db->query("UPDATE ".BLOG_COMMENT_TABLE_NAME."
			                       SET content='$content', mod_time='$time'
			                       WHERE comment_id='$commentId'");
	}
	
	public function delete($commentId, $time) {
		$query = $this->db->query("UPDATE ".BLOG_COMMENT_TABLE_NAME."
			                       SET orderby=0, mod_time='$time'
			                       WHERE comment_id='$commentId'");
	}
	
	private function _get_new_family() {
		$query = $this->db->query("SELECT MAX(family)
			                       FROM ".BLOG_COMMENT_TABLE_NAME);

		$result = $query->row_array();
		return $result['MAX(family)'] + 1;
	}
	
	private function _get_order_infamily($family) {
		$query = $this->db->query("SELECT MAX(orderby)
			                       FROM ".BLOG_COMMENT_TABLE_NAME."
			                       WHERE family='$family'");

		$result = $query->row_array();
		return $result['MAX(orderby)'] + 1;
	}
}