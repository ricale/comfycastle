<?php
class Linkset_model extends CI_Model {

	public function __construct() {
		$this->load->database();

		$this->linkTable = LINKSET_TABLE_NAME;
		$this->tagTable  = LINKSET_TAGS_TABLE_NAME;
		$this->tOwnTable = LINKSET_TAG_OWNER_TABLE_NAME;
		$this->favoTable = LINKSET_FAVORITAG_TABLE_NAME;
		$this->propTable = LINKSET_PROPERTY_TABLE_NAME;
		$this->itemTagJoinTable = LINKSET_J_ITEM_TAG_TABLE_NAME;
	}

	/**
	 * 조건에 맞는 글이 몇 개인지 확인해 반환한다.
	 * 결과 값이 없으면 0을 반환한다.
	 */
	public function get_count($wheres) {
		$where = $this->_make_where($wheres);
		if(is_array($wheres) && array_key_exists('tag', $wheres)) {
			$query = $this->db->query("SELECT SUM(weight)
				                       FROM   $this->tOwnTable
				                       NATURAL JOIN $this->tagTable
				                       $where");

			$result = $query->row_array();
			return $result['SUM(weight)'];

		} else {
			$query = $this->db->query("SELECT COUNT(*)
				                       FROM   $this->linkTable
				                       $where");

			$result = $query->row_array();
			return $result["COUNT(*)"];
		}
	}

	public function get_tag_id($userId, $tag) {
		$query = $this->db->query("SELECT tag_id
			                       FROM   $this->tagTable
			                       WHERE  name = '$tag'
			                          AND tag_id IN (SELECT tag_id
			                          	             FROM   $this->tOwnTable
			                          	             WHERE  user_id = $userId)");

		if($query->num_rows() == 0) {
			return NULL;
		} 

		$result = $query->row_array();
		return $result['tag_id'];
	}

	/**
	 * 특정 글(아이템)의 태그들의 배열을 반환한다.
	 * 결과 값이 없으면 NULL을 반환한다.
	 */
	public function get_item_tags($itemId) {
		$query = $this->db->query("SELECT tag_id, name
		                           FROM   $this->tagTable
		                           WHERE  tag_id IN (SELECT tag_id
		                           	                 FROM   $this->itemTagJoinTable
		                           	                 WHERE  item_id = '$itemId')");

		if($query->num_rows() == 0) {
			return NULL;
		} 

		return $query->result_array();
	}

	/**
	 * 특정 글의 작성자를 반환한다.
	 * 결과 값이 없으면 NULL을 반환한다.
	 */
	public function get_item_user($itemId) {
		$query = $this->db->query("SELECT user_id
			                       FROM   $this->linkTable
			                       WHERE  item_id = '$itemId'");

		if($query->num_rows() == 0) {
			return NULL;
		}
		
		$result = $query->row_array();
		return $result['user_id'];
	}

	/**
	 * 모든 태그를 반환하거나 특정 작성자의 모든 태그를 반환한다.
	 * 결과 값이 없으면 NULL을 반환한다.
	 */
	public function get_tags($userId = NULL, $limit = 0) {
		if($userId == NULL) {
			$query = $this->db->query("SELECT tag_id, name, SUM(weight) weight
				                       FROM   $this->tagTable
				                       NATURAL JOIN $this->tOwnTable
				                       GROUP BY name
				                       ORDER BY weight DESC, name");

			if($query->num_rows() == 0) {
				return NULL;
			}

			return $query->result_array();

		} else {
			$userId = urldecode($userId);
			$limit = $limit == 0 ? NULL : " LIMIT $limit";
			$query = $this->db->query("SELECT tag_id, name, weight
				                       FROM   $this->tagTable
				                       NATURAL JOIN $this->tOwnTable
				                       WHERE  user_id = '$userId'
				                       ORDER BY weight DESC, name
				                       $limit");

			if($query->num_rows() == 0) {
				return NULL;
			}

			return $query->result_array();
		}
	}

	public function get_property($userId) {
		$query = $this->db->query("SELECT *
			                       FROM   $this->propTable
			                       WHERE  user_id = '$userId'");

		if($query->num_rows() == 0) {
			return NULL;
		}

		return $query->row_array();
	}

	public function get_items_per_page($userId) {
		$query = $this->db->query("SELECT items_per_page
			                       FROM   $this->propTable
			                       WHERE  user_id = '$userId'");

		if($query->num_rows() == 0) {
			return NULL;
		}

		$result = $query->row_array();
		return $result['items_per_page'];
	}

	public function get_tags_in_menu($userId) {
		$query = $this->db->query("SELECT tags_in_menu
			                       FROM   $this->propTable
			                       WHERE  user_id = '$userId'");

		if($query->num_rows() == 0) {
			return NULL;
		}

		$result = $query->row_array();
		return $result['tags_in_menu'];
	}

	public function get_favoritag($userId) {
		$query = $this->db->query("SELECT tag_id, name, weight
			                       FROM   $this->tagTable
			                       NATURAL JOIN $this->tOwnTable
			                       WHERE  user_id = '$userId'
			                          AND tag_id IN (SELECT tag_id
			                                         FROM   $this->favoTable
			                                         WHERE  user_id = '$userId')
		                           ORDER BY name");

		if($query->num_rows() == 0) {
			return NULL;
		}

		return $query->result_array();
	}

	/**
	 * 조건에 맞는 글을 반환한다.
	 * 결과 값이 없으면 NULL을 반환한다.
	 */
	public function get($wheres = NULL, $start = 0, $count = 1) {
		$where = $this->_make_where($wheres);
		if(is_array($wheres) && array_key_exists('tag', $wheres)) {
			$query = $this->db->query("SELECT item_id, user_id, title, url
			                           FROM   $this->linkTable 
			                           WHERE  item_id IN (SELECT item_id
			                           	                  FROM   $this->itemTagJoinTable
			                           	                  WHERE  tag_id IN (SELECT tag_id
			                           	                 	                FROM   $this->tagTable
			                           	                 	                NATURAL JOIN $this->tOwnTable
			                           	                 	                $where))
			                           ORDER BY item_id DESC
			                           LIMIT $start, $count");

			if($query->num_rows() === 0) {
				return NULL;
			}

			return $query->result_array();

		} else {
			$query = $this->db->query("SELECT item_id, user_id, title, url
				                       FROM   $this->linkTable 
				                       $where
				                       ORDER BY item_id DESC
				                       LIMIT $start, $count");

			if($query->num_rows() == 0) {
				return NULL;
			}

			return $query->result_array();
		}
	}

	public function is_exist_tag($userId, $tag) {
		$query = $this->db->query("SELECT *
			                       FROM   $this->tagTable
			                       WHERE  name = '$tag'
			                          AND tag_id IN (SELECT tag_id
			                          	             FROM   $this->tOwnTable
			                          	             WHERE  user_id = $userId)");

		return $query->num_rows() != 0;
	}

	/**
	 * 값을 입력한다.
	 */
	public function put($userId, $title, $url, $tags, $time) {
		$title = addslashes($title);
		$url   = addslashes($url);

		$this->db->query("INSERT INTO $this->linkTable (user_id, title, url, time)
			              VALUES ('$userId', '$title', '$url', '$time');");
		
		$query = $this->db->query("SELECT item_id
			                       FROM   $this->linkTable
			                       WHERE  user_id = '$userId'
			                          AND title = '$title'
			                          AND url = '$url'
			                          AND time = '$time'");

		$result = $query->row_array();

		$this->_put_tags($result['item_id'], $userId, $tags);
	}

	public function put_favoritag($userId, $tags) {
		if(is_array($tags)) {
			$tags = array_unique($tags);

			foreach($tags as $tag) {
				$tag = addslashes($tag);

				$query = $this->db->query("INSERT INTO $this->favoTable (user_id, tag_id)
					                       VALUES ('$userId', (SELECT tag_id
					                                           FROM   $this->tagTable
					                                           WHERE  name = '$tag'))");
			}
		}
	}

	public function delete_favoritag($userId, $tagIds) {
		if(is_array($tagIds)) {
			foreach($tagIds as $tagId) {
				$this->db->query("DELETE FROM $this->favoTable
					              WHERE user_id = '$userId'
					                AND tag_id = '$tagId'");
			}
		}
	}

	public function set_property($userId, $items, $tags) {
		$this->db->query("UPDATE $this->propTable
			              SET    items_per_page = '$items', tags_in_menu = '$tags'
			              WHERE  user_id = '$userId'");
	}

	/**
	 * 값을 수정한다.
	 */
	public function edit($itemId, $userId, $title, $url, $tags) {
		$title = addslashes($title);
		$url   = addslashes($url);

		$this->db->query("UPDATE $this->linkTable
			              SET    title = '$title', url = '$url'
			              WHERE  item_id = '$itemId'");

		$this->_put_tags($itemId, $userId, $tags);
	}
	
	/**
	 * 값을 삭제한다.
	 */
	public function delete($itemId) {
		$query = $this->db->query("SELECT tag_id, user_id
			                       FROM   $this->itemTagJoinTable
			                       NATURAL JOIN $this->linkTable
			                       WHERE  item_id = '$itemId'");
		
		$result = $query->result_array();
		foreach($result as $row) {
			$this->delete_tag($itemId, $row['tag_id'], $row['user_id']);
		}

		$this->db->query("DELETE FROM $this->linkTable
			              WHERE item_id = '$itemId'");
	}

	/**
	 * 태그를 삭제한다.
	 */
	public function delete_tag($itemId, $tagId, $userId) {
		$this->db->query("DELETE FROM $this->itemTagJoinTable
			              WHERE item_id = '$itemId'
			                AND tag_id = '$tagId'");

		$query = $this->db->query("SELECT weight
			                       FROM   $this->tOwnTable
			                       WHERE  tag_id = '$tagId'
			                          AND user_id = '$userId'");

		$result = $query->row_array();
		if($result['weight'] == 1) {
			$this->db->query("DELETE FROM $this->tOwnTable
				              WHERE tag_id = '$tagId'
				                AND user_id = '$userId'");

			$this->db->query("DELETE FROM $this->favoTable
				              WHERE tag_id = '$tagId'
				                AND user_id = '$userId'");
		} else {
			$this->db->query("UPDATE $this->tOwnTable
				              SET    weight = weight-1
				              WHERE  tag_id = '$tagId'
				                 AND user_id = '$userId'");
		}

		$this->db->query("DELETE FROM $this->tagTable
			              WHERE (SELECT SUM(weight)
			              	     FROM   $this->tOwnTable
			              	     WHERE  tag_id = '$tagId'
			              	        AND user_id = '$userId') <= 0");
	}

	/**
	 * 태그를 입력한다.
	 * return, break, continue로 점칠된 더러운 함수
	 */
	private function _put_tags($itemId, $userId, $tags) {
		if(!is_array($tags)) {
			return;
		}

		$tags = array_unique($tags);

		foreach($tags as $tag) {
			$tag = addslashes($tag);

			// 결과가 없다. = 이 태그는 존재하지 않는다.
			// 결과가 있다. = 이 태그는 존재한다.
			// have 필드에 1값이 없다. = $userId 유저는 이 태그를 갖고 있지 않다.
			// have 필드에 1값이 있다. = $userId 유저는 이 태그를 갖고 있다.
			// exist 필드에 1 값이 없다. = $itemId 글은 이 태그를 갖고 있지 않다.
			// exist 필드에 1 값이 있다. = $itemId 글은 이 태그를 갖고 있다.
			$query = $this->db->query("SELECT distinct user_id = '$userId' have, item_id = '$itemId' exist
				                       FROM link_tag_owner NATURAL JOIN link_j_item_tag
				                       WHERE tag_id IN (SELECT tag_id
				                       	                FROM link_tag
				                       	                WHERE name = '$tag')");

			$results = $query->result_array();

			if(sizeof($results) == 0) {
				$this->db->query("INSERT INTO $this->tagTable (name)
					              VALUES ('$tag')");

				$tagId = $this->_get_tag_id($tag);

				$this->db->query("INSERT INTO $this->tOwnTable (user_id, tag_id)
					              VALUES ('$userId', '$tagId')");

				$query = $this->db->query("INSERT INTO $this->itemTagJoinTable (item_id, tag_id)
					                       VALUES ('$itemId', '$tagId')");
			} else {
				$alreadyHave  = FALSE;
				$alreadyExist = FALSE;

				foreach($results as $result) {

					if($result['have'] == 1) {
						$alreadyHave = TRUE;

						if($result['exist'] == 1) {
							$alreadyExist = TRUE;
							break;
						}
					}
				}

				if($alreadyExist) {
					continue;
				}

				$tagId = $this->_get_tag_id($tag);

				if($alreadyHave) {
					$this->db->query("UPDATE $this->tOwnTable
						              SET    weight = weight + 1
						              WHERE  user_id = '$userId'
						                 AND tag_id = '$tagId'");
				} else {
					$this->db->query("INSERT INTO $this->tOwnTable (user_id, tag_id)
						              VALUES ('$userId', '$tagId')");
				}

				$query = $this->db->query("INSERT INTO $this->itemTagJoinTable (item_id, tag_id)
				                       VALUES ('$itemId', '$tagId')");
			}
		}
	}

	private function _get_tag_id($tag) {
		$query = $this->db->query("SELECT tag_id
						           FROM   $this->tagTable
						           WHERE  name = '$tag'");

		$result = $query->row_array();
		return $result['tag_id'];
	}

	/**
	 * 연관 배열을 이용해 쿼리의 WHERE 절을 만든다.
	 */
	private function _make_where($wheres) {
		$where = "";
		if(is_array($wheres)) {
			if(array_key_exists('item_id', $wheres)) {
				$where = "WHERE item_id='${wheres['item_id']}'";

			} else {
				if(array_key_exists('user_id', $wheres)) {
					$where = "WHERE user_id='${wheres['user_id']}'";
					$linker = " AND (";

				} else {
					$linker = "WHERE";
				}
				
				if(array_key_exists('keyword', $wheres)) {
					$wheres['keyword'] = addslashes($wheres['keyword']);
					if($wheres['st']) {// 검색 옵션에 제목으로 찾기가 포함되어 있다.
						$where .= "$linker title LIKE '%${wheres['keyword']}%'";
						$linker = " OR";
					}
			
					if($wheres['sc']) {// 검색 옵션에 작성 내용	으로 찾기가 포함되어 있다.
						$where .= "$linker url LIKE '%${wheres['keyword']}%'";
					}
					
				} else if(array_key_exists('tag', $wheres)) {
					$wheres['tag'] = addslashes($wheres['tag']);
					$where .= "$linker name='${wheres['tag']}'";
				}

				if(array_key_exists('user_id', $wheres) && (array_key_exists('keyword', $wheres) || array_key_exists('tag', $wheres))) {
					$where .= ")";
				}

			}
			
		}
				 
		return $where;
	}
}