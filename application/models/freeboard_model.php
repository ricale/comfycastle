<?php
class Freeboard_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
	
	public function get_writings_count($wheres) {
		$query = $this->db->query("SELECT COUNT(*) FROM ".FREEBOARD_TABLE_NAME." ".$this->_make_where($wheres));
		
		$result = $query->row_array();
		return $result["COUNT(*)"];
	}
	
	public function get($wheres, $start = 0, $count = 1) {
		$query = $this->db->query("SELECT * FROM ".FREEBOARD_TABLE_NAME." ".$this->_make_where($wheres)." ORDER BY family, orderby LIMIT $start, $count");
		$result = $query->result_array();
		
		return $result;
	}
	
	public function get_writer_id($number) {
		$query = $this->db->query("SELECT id FROM ".FREEBOARD_TABLE_NAME." WHERE number = $number");
		$result = $query->row_array();
		return $result['id'];
	}
	
	public function get_writing_password($number) {
		$query = $this->db->query("SELECT pw FROM ".FREEBOARD_TABLE_NAME." WHERE number = $number");
		$result = $query->row_array();
		return $result['pw'];
	}
	
	public function get_orderby($number) {
		$query = $this->db->query("SELECT orderby FROM ".FREEBOARD_TABLE_NAME." WHERE number = $number");
		$result = $query->row_array();
		return $result['orderby'];
	}
	
	public function put($id, $name, $pw, $title, $content, $autonl, $time, $uploadData = NULL, $parentNumber = NULL) {
		if($parentNumber === NULL) {
			$family = $this->_get_min_family();
			$this->db->query("INSERT INTO ".FREEBOARD_TABLE_NAME." (family, id, name, pw, title, content, autonl, time)
			                  VALUES ('$family', '$id', '$name', '$pw', '$title', '$content', '$autonl', '$time')");
			$orderby = 0;
		} else {
			$family = $this->_get_parent_family($parentNumber);
			$this->db->query("UPDATE ".FREEBOARD_TABLE_NAME." SET orderby=orderby+1 WHERE family=$family AND orderby>0");
			$this->db->query("INSERT INTO ".FREEBOARD_TABLE_NAME." (family, orderby, id, name, pw, title, content, autonl, time)
			                  VALUES ('$family', '1', '$id', '$name', '$pw', '$title', '$content', '$autonl', '$time')");
			$orderby = 1;
		}

		$query = $this->db->query("SELECT number FROM ".FREEBOARD_TABLE_NAME." WHERE family='$family' AND orderby='$orderby'");
		$result = $query->row_array();

		if(is_array($uploadData)) {
			$this->db->query("INSERT INTO ".FREEBOARD_UPLOADS_TABLE_NAME." (writing, file_name, file_type, file_size, is_image, image_width, image_height, image_type)
			                  VALUES (${result['number']}, '${uploadData['file_name']}', '${uploadData['file_type']}', '${uploadData['file_size']}',
							          '${uploadData['is_image']}', '${uploadData['image_width']}', '${uploadData['image_height']}', '${uploadData['image_type']}')");
		}

		return $result['number'];
	}
	
	public function edit($number, $title, $content, $autonl, $time) {
		$this->db->query("UPDATE ".FREEBOARD_TABLE_NAME." SET title='$title', content='$content', autonl='$autonl', time='$time'
								   WHERE number='$number'"); 
	}
	
	public function delete($number) {
		$this->db->query("DELETE FROM ".FREEBOARD_TABLE_NAME." WHERE number = $number");
		$this->db->query("DELETE FROM ".FREEBOARD_COMMENT_TABLE_NAME." WHERE writing = $number");
	}
	
	private function _get_min_family() {
		$query = $this->db->query("SELECT MIN(family) FROM ".FREEBOARD_TABLE_NAME);
		$result = $query->row_array();
		return $result['MIN(family)'] - 1;
	}

	private function _get_parent_family($number) {
		$query = $this->db->query("SELECT family FROM ".FREEBOARD_TABLE_NAME." WHERE number=$number");
		$result = $query->row_array();
		return $result['family'];
	}
	
	private function _make_where($wheres) {
		$where = "";
		if(is_array($wheres)) {
			
			if(array_key_exists('number', $wheres)) {
				$where = "WHERE number=".$wheres['number'];
			} else if(array_key_exists('category', $wheres) || (array_key_exists('keyword', $wheres))) {
				$linker = "WHERE";
				
				if(array_key_exists('category', $wheres)) {
					$where .= "$linker category='".$wheres['category']."' ";
					$linker = "AND (";
				}
					
				if(array_key_exists('keyword', $wheres)) {					
					if($wheres['st']) {// 검색 옵션에 제목으로 찾기가 포함되어 있다.
						$where .= "$linker title LIKE '%".$wheres['keyword']."%' ";
						$linker = "OR";
					}
			
					if($wheres['sc']) {// 검색 옵션에 작성 내용	으로 찾기가 포함되어 있다.
						$where .= "$linker content LIKE '%".$wheres['keyword']."%'";
					}
					
					if(array_key_exists('category', $wheres)) {
						$where .= ")";
					}
				}

			}
			
		}
				 
		return $where;
	}
}