<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Mysess {

	public function __construct() {
        session_start();
    }

    public function login($id, $name) {
    	$_SESSION['loggedin'] = TRUE;
    	$_SESSION['id'] = $id;
    	$_SESSION['name'] = $name;
    }

    public function refresh($name) {
        $_SESSION['name'] = $name;   
    }

    public function logout() {
    	session_destroy();
    }

    public function loggedin() {
    	return array_key_exists('loggedin', $_SESSION) && $_SESSION['loggedin'];
    }

    public function get_id() {
    	return array_key_exists('id', $_SESSION) ? $_SESSION['id'] : NULL;
    }

    public function get_name() {
    	return array_key_exists('name', $_SESSION) ? $_SESSION['name'] : NULL;
    }
}

/* End of file Someclass.php */