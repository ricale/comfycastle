<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Common {

	public function __construct() {

    }

    public function print_ajax_result($array) {
    	$CI =& get_instance();
        $CI->output->set_header("Content-Type: text/html; charset=UTF-8;");
        echo json_encode($array);
    }

    public function is_valid_access() {
        $CI =& get_instance();
        return $CI->input->post('valid') != NULL ? TRUE : FALSE;
    }
}

/* End of file Someclass.php */