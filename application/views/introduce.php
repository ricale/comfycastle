<textarea id="source">
### comfycastle

- 이 웹사이트는 개인적인 웹프로그래밍 연습 공간입니다.
- _'만들고 싶은 것을 직접 만들어서 직접 써 본다'_가 이 곳의 모토입니다.
- 언제나 __구현 및 테스트__ 상태입니다. 따라서 특정 기능이 정상적으로 작동하지 않을 수도 있습니다.

##### 연습하는 사람

- ricale ,,ricale@hanmail.net,,
 - 대학생
 - 프로그래머 지망생

##### 사용하는 언어

- HTML/CSS
- PHP
 - CodeIgniter ,,PHP Framework,,
- JavaScript
 - jQuery ,,JavaScript Library,,
 - jQuery Form ,,Plugin,,

##### 권장하는 사용 환경

- __Mozilla Firefox__ 최신 버전 >= _Google Chrome_ 최신 버전 >>>>> Microsoft Explorer (권장하지 않습니다.)
- 자바스크립트 기능 활성화 상태
- 글씨체 맑은 고딕, Courier New

##### 구현 중인 것들

- account ; 회원 정보 관리
- [blog][] ; 블로그
- [linkset][] ; 링크 모음
- board ; 게시판
- [hmd][] ; 마크다운 번역 자바스크립트 컴포넌트
 - [hme][] ; hmd를 사용한 마크다운 웹에디터

##### 원격 저장소

- [comfycastle ,,(account, blog, linkset, board, hme),,][comfycastle git]
- [handmade markdown decoder ,,(hmd),,][hmd]

##### 현재 세부 사항 및 계획 세부 사항

- [현재](http://www.comfycastle.net/blog/150)
- [계획](http://comfycastle.net/blog/tag/comfycastle계획)

[blog]: http://comfycastle.net/blog
[linkset]: http://comfycastle.net/linkset
[hme]: http://comfycastle.net/hme
[hmd]: https://bitbucket.org/ricale/handmade-markdown-decoder
[comfycastle git]: https://bitbucket.org/ricale/comfycastle

</textarea>
<div id="target" class="decorate_link">

</div>