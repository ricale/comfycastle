<div id="container">
	<?php foreach($writings as $w): ?>
	<div class='article'>
		<div class='writing' id='writing<?php echo $w['writingId']; ?>'>
			<div class='title'><a href='<?php echo BLOG_URL.$w['writingId']; ?>'><?php echo $w['title']; ?></a></div>
			<div class='writeinfo'>
				<div class='category'><a href='<?php echo BLOG_URL."category/".$w['category']; ?>'><?php echo $w['category']; ?></a></div>
				<div class='time'><?php echo $w['time']; ?><span class='mod'>/<?php echo $w['modTime']; ?></span></div>
				
				<?php if($isAdmin): ?>
				<div class='edit'><a href='<?php echo BLOG_URL."edit/".$w['writingId']; ?>'>수정</a></div>
				<div class='delete'><a href='<?php echo BLOG_URL."delete/".$w['writingId']; ?>'>삭제</a></div>
				<?php endif; ?>
				
			</div>

			<?php if(is_array($w['uploads'])): ?>
			<ul class="uploads">
				<?php foreach($w['uploads'] as $uploads): ?>
				<li><a href="<?php echo UPLOADS_URL.$uploads['file_name']; ?>"><?php echo $uploads['orig_name']; ?></a></li>
				<?php endforeach; ?>
			</ul>
			<?php endif; ?>

			<div class='content decorate_link'><?php echo $w['plainContent']; ?></div>
			<textarea id="source<?php echo $w['writingId']; ?>" class="markdown_source"><?php echo $w['markdownContent']; ?></textarea>

			<div class='snsaction'>
				<div class='fbshare'><a href=''><img src='<?php echo IMAGE_URL."facebook.png"; ?>'></a></div>
				<a href="https://twitter.com/share" class="twitter-share-button"
				    data-size="large" data-url="<?php echo BLOG_URL.$w['writingId']; ?>" data-text="comfycastle: '<?php echo $w['title']; ?>'" data-lang="ko" data-count="none">트윗하기</a>
			</div>

			<?php if(sizeof($w['tags']) > 0): ?>
			<div class='tags'>
				<span>TAGS:</span>

				<ul>
					<?php foreach($w['tags'] as $tags): ?>
					<li><a href="<?php echo BLOG_URL."tag/".$tags['name']?>"><?php echo $tags['name']; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php endif; ?>

			<div class='writeaction'>
				<div class='cnt'><a href=''>댓글 <?php echo $w['commentsCount']?>개</a></div>
			</div>

		</div>
		
		<div class="fbcomment">
			<div class='fb-comments' data-href="<?php echo BLOG_URL.$w['writingId']; ?>" data-width="740" data-num-posts="10"></div>
		</div>
	</div>
	<?php endforeach; ?>
</div>
