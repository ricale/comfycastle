<div id="list">
	<div class='desc'><?php echo $key." - ".$value." : 총 ".$writingsCount; ?>개의 글</div>

	<ul>
		<?php foreach($items as $item): ?>
		<li>
			<span class='time'><?php echo $item['time']; ?></span>
			<span class='title'><a href='<?php echo BLOG_URL.$item['writingId']; ?>'><?php echo $item['title']; ?></a></span><span class='cnt'>(<?php echo $item['commentsCount']; ?>)</span><?php if($item['new']): ?><span class="new">new</span><?php endif; ?>
		</li>
		<?php endforeach; ?>
	</ul>
</div>