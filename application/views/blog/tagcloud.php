<div id="tagcloud">
	<ul>
		<?php foreach($tags as $tag): ?>
		<li class="tag <?php echo $class[$tag['weight']]; ?>">
			<a href="<?php echo BLOG_URL."tag/".$tag['name']; ?>"><?php echo $tag['name']; ?></a><span class="cnt">(<?php echo $tag['weight']; ?>)</span></li>
		<?php endforeach; ?>
	</ul>
</div>