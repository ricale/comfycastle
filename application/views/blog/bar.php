<div id="bar">
	<ul id="category">
		<?php foreach($category as $c): ?>
		<li>
			<a href="<?php echo BLOG_URL."category/".$c['name']; ?>"><?php echo $c['name']; ?></a><span class="cnt"><?php echo $c['count']?></span><?php if($c['new']): ?><span class="new">new</span><?php endif; ?>
		</li>
		<?php endforeach; ?>
	</ul>
	|
	<ul>
		<li><a href="<?php echo BLOG_URL."category/"; ?>">모든글</a></li>
		<li><a href="<?php echo BLOG_URL."tag/"; ?>">태그들</a></li>
	</ul>
	|	
	<?php if($isAdmin): ?>
	<ul class="admin">
		<?php foreach($admin as $a): ?>
		<li><a href="<?php echo BLOG_URL.$a['uri']; ?>"><?php echo $a['menu']; ?></a></li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>
</div>