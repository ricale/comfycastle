<div id="container">

	<form id="write" method="post" autocomplete="off" enctype="multipart/form-data">
		<input type="hidden" name="valid"  value="1">
		<input type="hidden" name="writing_id" value="<?php echo $writingId; ?>">
		<table>
		<tr>
			<th>분류</th>
			<td>
				<select name='category_id'>
					<?php foreach($category as $c): ?>
					<option value="<?php echo $c['category_id']; ?>" <?php echo $c['selected']; ?>><?php echo $c['name']; ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>제목</th>
			<td>
				<input type="text" name="title" maxlength="<?php echo BLOG_TITLE_MAXLENGTH; ?>" value="<?php echo $title; ?>">
				<label for="autonl">마크다운형식 </label><input id="autonl" type="checkbox" name="autonl" value="1" <?php echo $anlChecked; ?>>
			</td>
		</tr>
		<tr>
			<th>내용</th>
			<td><textarea name="content"><?php echo $content; ?></textarea></td>
		</tr>

		<tr>
			<th>태그</th>
			<td><input type="text" name="tagger"></td>
		</tr>

		<tr id="tagbox">
			<th></th>
			<td>
				<?php foreach($tags as $tag): ?>
				<span class="tagaction">
					<span class="tag"><?php echo $tag['name']; ?></span>
					<a class="delete" href="">삭제</a>
				</span>
				<input type="hidden" name="deletetags[]" value="<?php echo $tag['tag_id']; ?>" disabled>
				<?php endforeach; ?>
			</td>
		</tr>

		<?php for($i = 0; $i < sizeof($uploads); $i++): ?>
		<tr>
			<input type="hidden" name="deleteupload<?php echo $i; ?>" value="<?php echo $uploads[$i]['upload_id']; ?>" disabled>
			<th>업로드#<?php echo ($i + 1); ?></th>
			<td id="userfile<?php echo $i; ?>">
				<input type="button" value="삭제">
				<span><?php echo $uploads[$i]['orig_name']; ?></span>
			</td>
		</tr>
		<?php endfor; ?>
		<?php for($i = sizeof($uploads); $i < UPLOADS_PER_WRITING; $i++): ?>
		<tr>
			<th>업로드#<?php echo ($i + 1); ?></th>
			<td>
				<input type="file" name="userfile<?php echo $i; ?>" size="20">
			</td>
		</tr>
		<?php endfor; ?>
		<tr>
			<th></th>
			<td><input type="submit" value="완료"><span id="isvalid" class="warning"></span></td>
		</tr>
		</table>
	</form>

</div>