<div id="container">
	<?php if($key !== ""): ?>
	<div class='desc'><?php echo $key."-'".$value."' : 총 ".$count; ?>개의 링크</div>
	<?php endif; ?>

	<?php foreach($links as $link): ?>
	<div class="link">
		<div class="title"><a href="<?php echo $link['url']; ?>" target="_blank"><?php echo $link['title']; ?></a></div>
		<div class="url"><a href="<?php echo $link['url']; ?>" target="_blank"><?php echo $link['url']; ?></a></div>
		<?php if($showWriters): ?>
		<div class="info">
			<div class="name"><a href="<?php echo LINKSET_URL.$link['user_id'] ?>">by <?php echo $link['user_id']; ?></a></div>
		</div>
		<?php endif; ?>
		<div class="tag">
			<?php if(is_array($link['tags'])): ?>
			<span>TAGS:</span>
			<ul>
				<?php foreach($link['tags'] as $tag): ?>
				<li><a href="<?php echo LINKSET_URL.$uri.$tag['name']?>"><?php echo $tag['name']; ?></a></li>
				<?php endforeach; ?>
			</ul>
			<?php endif; ?>
		</div>
		<!--div class="sns"></div-->
		<?php if($link['isWriter']): ?>
		<div class="menu">|
			<div class='edit'><a href='<?php echo LINKSET_URL."edit/".$link['item_id']; ?>'>수정</a></div>
			<div class='delete'><a href='<?php echo LINKSET_URL."delete/".$link['item_id']; ?>'>삭제</a></div>
		</div>
		<?php endif; ?>
	</div>
	<?php endforeach; ?>
</div>