<div id="container">
	<p class="description">회원가입 및 로그인을 하시면 linkset의 모든 기능을 사용하실 수 있습니다.</p>
	<ul>
		<li><a href="<?php echo ACCOUNT_URL."join" ?>">회원가입</a></li>
		<li><a href="<?php echo ACCOUNT_URL."login/".str_replace('/', TOKEN_FOR_MAKE_URI_FROM_URIS, uri_string()); ?>">로그인</a></li>
	</ul>
	<p>회원가입 및 로그인하지 않으셔도 다른 분들의 linkset을 보는 것은 가능합니다.</p>
	<ul>
		<li><a href="<?php echo LINKSET_URL."all/" ?>">다른 사용자들의 linkset 보기</a></li>
	</ul>
</div>