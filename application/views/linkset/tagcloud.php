<div id="tagcloud">
	<?php if(is_array($favoritags)): ?>
	<fieldset>
		<legend>관심 태그</legend>
		<ul id="favoritag">
			<?php foreach($favoritags as $tag): ?>
			<li class="tag">
				<a href="<?php echo LINKSET_URL."$uri/${tag['name']}" ?>"><?php echo $tag['name']; ?></a><span class="cnt">(<?php echo $tag['weight']; ?>)</span>
			</li>
			<?php endforeach; ?>
		</ul>
	</fieldset>
	<?php endif; ?>

	<ul id="normal">
		<?php foreach($tags as $tag): ?>
		<li class="tag <?php echo $class[$tag['weight']]; ?>">
			<a href="<?php echo LINKSET_URL."$uri/${tag['name']}" ?>"><?php echo $tag['name']; ?></a><span class="cnt">(<?php echo $tag['weight']; ?>)</span>
		</li>
		<?php endforeach; ?>
	</ul>
</div>