<div id="container">
	<div id="desc">
		제목을 입력하지 않으시면 자동으로 해당 URL의 제목(title)이 저장됩니다.
		<br/>(단 해당 페이지의 문자셋이 UTF-8, euc-kr, MS949일 경우에만 해당됩니다.)
	</div>
	<form id="write" method="post" autocomplete="off">
		<input type="hidden" name="valid" value="1">
		<input type="hidden" name="item_id" value="<?php echo $itemId; ?>">
		<table id="link1">
		<tr>
			<th>제목</th>
			<td><input type="text" name="title[]" maxlength="<?php echo LINKSET_TITLE_MAXLENGTH; ?>" size="100" value="<?php echo $title; ?>"></td>
		</tr>
		<tr>
			<th>URL</th>
			<td><input type="text" name="url[]" maxlength="<?php echo LINKSET_URL_MAXLENGTH; ?>" size="100" value="<?php echo $url; ?>"></td>
		</tr>

		<tr>
			<th>태그</th>
			<td><input type="text" name="tagger1"></td>
		</tr>

		<tr class="tagbox">
			<th></th>
			<td>
				<?php for($i = 0; $i < sizeof($tags); $i++): ?>
				<span class="tagaction">
					<span class="tag"><?php echo $tags[$i]['name']; ?></span>
					<a class="delete" href="">삭제</a>
				</span>
				<input type="hidden" name="deletetags[]" value="<?php echo $tags[$i]['tag_id']; ?>" disabled>
				<?php endfor; ?>
			</td>
		</tr>
		</table>

		<?php if($itemId === NULL): ?>
		<input type="button" value="추가">
		<?php endif; ?>
		<input type="submit" value="완료"><span id="isvalid" class="warning"></span>
	</form>

</div>