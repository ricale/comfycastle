<?php if($isAvailable): ?>
<div id="bar">
	<ul>
		<li><a href="<?php echo LINKSET_URL."write"; ?>">링크하기</a></li>
		<?php if($isMyPage): ?>
		<li><a href="<?php echo LINKSET_URL."all"; ?>">모두의링크</a></li>
		<li><a href="<?php echo LINKSET_URL.$userId."/admin"; ?>">설정</a></li>
		<?php endif; ?>
		<?php if(!$isMyPage): ?>
		<li><a href="<?php echo LINKSET_URL; ?>">내링크</a></li>
		<?php endif; ?>
	</ul>
</div>
<?php endif; ?>