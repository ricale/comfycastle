<div id="header">
	<h1><a href="<?php echo LINKSET_URL; ?>"><?php echo LINKSET_TITLE; ?></a></h1>
	<?php if($writer != NULL): ?>
	<div class="writer">by <?php echo $writer; ?></div>
	<?php endif; ?>
	<div class="desc"><?php echo $desc; ?></div>
	<div id="fblike">
		<div class="fb-like" data-href="http://comfycastle.net/linkset/" data-send="true" data-layout="button_count" data-width="100" data-show-faces="true"></div>
	</div>
</div>