<div id="container">
	<form id="admin" method="post" autocomplete="off">
	<fieldset>
		<input type="hidden" name="valid" value="1">
		<legend>설정</legend>
		<div>
			<label for="nof_item">페이지 당 글(링크) 수</label>
			<select name="nof_item" id="nof_item">
				<?php for($i = LINKSET_MIN_OF_ITEMS_PER_PAGE; $i <= LINKSET_MAX_OF_ITEMS_PER_PAGE; $i++): ?>
				<option value="<?php echo $i; ?>" <?php if($i == $nof_item) echo "selected"; ?>><?php echo $i; ?></option>
				<?php endfor; ?>
			</select>
		</div>
		<div>
			<label for="nof_tag">메뉴에 표시될 태그 수</label>
			<select name="nof_tag" id="nof_tag">
				<option value="0" <?php if(0 === $nof_tag) echo "selected"; ?>>제한없음</option>
				<?php for($i = LINKSET_MIN_OF_TAGS_IN_MENU; $i <= LINKSET_MAX_OF_TAGS_IN_MENU; $i++): ?>
				<option value="<?php echo $i; ?>" <?php if($i == $nof_tag) echo "selected"; ?>><?php echo $i; ?></option>
				<?php endfor; ?>
			</select>
		</div>

		<input type="hidden" name="userid" value="<?php echo $userId; ?>">
		<input type="submit" value="저장">
		<div class="isvalid"></div>
	</fieldset>
	</form>

	<form id="favoritag" method="post" autocomplete="off">
	<fieldset>
		<input type="hidden" name="valid" value="1">
		<legend>관심 태그</legend>
		<div>
			<label for="tagger">추가</label>
			<input type="text" name="tagger">
		</div>

		<div id="tagbox">
		</div>

		<div id="tagbox_already">
			<?php foreach($tags as $tag): ?>
			<span class="tagaction">
				<span class="tag"><?php echo $tag['name']; ?></span>
				<a class="delete" href="">삭제</a>
			</span>
			<input type="hidden" name="deletetags[]" value="<?php echo $tag['tag_id']; ?>" disabled>
			<?php endforeach; ?>
		</div>

		<input type="hidden" name="userid" value="<?php echo $userId; ?>">
		<input type="submit" value="저장">
		<div class="isvalid"></div>
	</fieldset>
	</form>
</div>