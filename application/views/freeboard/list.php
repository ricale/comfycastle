<div id="container">
	<table id="writings">
	<thead>
		<tr>
			<th class="number">번호</th>
			<th class="title">제목</th>
			<th class="name">글쓴이</th>
			<th class="time">날짜</th>
			<th class="hit">조회수</th>
		</tr>
	</thead>
	<tbody>
		<?php for($i = 0; $i < sizeof($number); $i++): ?>
		<tr class="<?php echo $rowClass[$i]; ?>">
			<td class='number'><?php echo $number[$i]; ?></td>
			<td class='title'>
				<a href='<?php echo FREEBOARD_URL.$uri[$i] ?>'><?php echo $title[$i]; ?></a>
				<span class='comment'>(<?php echo $commentsCount[$i]; ?>)</span>
			</td>
			<td class='name'><?php echo $name[$i]; ?></td>
			<td class='time'><?php echo $time[$i]; ?></td>
			<td class='hit'><?php echo $hit[$i]; ?></td>
		</tr>
		<?php endfor; ?>
	</tbody>
	</table>
</div>