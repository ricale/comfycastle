<div id="footer">
	<ul id="pager">		
		<?php if($first !== NULL): ?>
		<li class="first"><a href='<?php echo FREEBOARD_URL.$uri.$first; ?>'><?php echo $firstLabel; ?></a> ..</li>
		<?php endif; ?>
		
		<?php if($prev !== NULL): ?>
		<?php if(array_key_exists(1, $prev)): ?>
		<li class="prevprev"><a href='<?php echo FREEBOARD_URL.$uri.$prev[1]; ?>'><?php echo $prevprevLabel; ?></a></li>
		<?php endif; ?>
		<li class="prev"><a href='<?php echo FREEBOARD_URL.$uri.$prev[0]; ?>'><?php echo $prevLabel; ?></a></li>
		<?php endif; ?>
		
		<li class="now"><?php echo $nowLabel; ?></li>
		
		<?php if($next !== NULL): ?>
		<li class="next"><a href='<?php echo FREEBOARD_URL.$uri.$next[0]; ?>'><?php echo $nextLabel; ?></a></li>
		<?php if(array_key_exists(1, $next)): ?>
		<li class="nextnext"><a href='<?php echo FREEBOARD_URL.$uri.$next[1]; ?>'><?php echo $nextnextLabel; ?></a></li>
		<?php endif; ?>
		<?php endif; ?>
		
		<?php if($last !== NULL): ?>
		<li class="last">.. <a href='<?php echo FREEBOARD_URL.$uri.$last; ?>'><?php echo $lastLabel; ?></a></li>
		<?php endif; ?>
	</ul>
	
	<form id="search" onsubmit="return false;">
		<input type="hidden" name="target" value="<?php echo $searchingTargetUri; ?>">
		<input type="checkbox" name="st" value="1" <?php echo $stChecked; ?>>제목
		<input type="checkbox" name="sc" value="1" <?php echo $scChecked; ?>>내용
		<input type="text" size="20" name="keyword" value="<?php echo $keyword; ?>">
		<input type="button" value="검색">
	</form>
</div>