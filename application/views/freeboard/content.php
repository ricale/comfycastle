<div id="container">
	<div class='writing' id='writing<?php echo $number; ?>'>
		<div class='title'><?php echo $title; ?></div>
		<div class="writeinfo">
			<div class='name'><?php echo $name; ?><?php if($id != "") echo "<span class='id'>(".$id.")</span>"; ?>, </div>
			<div class='time'><?php echo $time; ?>, </div>
			<div class='hit'><?php echo $hit; ?> hits</div>
		</div>
		<div class="writemenu">
			<?php if(!$isReply): ?>
			<div class='reply'><a href="<?php echo FREEBOARD_URL."reply/".$number; ?>">답글</a></div>
			<?php endif; ?>
			<div class='list'><a href="<?php echo FREEBOARD_URL.$listUri; ?>">목록</a></div>
			<?php if($isWritten): ?>
			<div class='edit'><a href="<?php echo FREEBOARD_URL."edit/".$number; ?>">수정</a></div>
			<div class='delete'><a href="<?php echo FREEBOARD_URL."delete/".$number; ?>">삭제</a></div>
			<?php endif; ?>
		</div>
		<div class='content'><?php echo $content; ?></div>
		<div class="writeaction">
			<div class='cnt'><a href=''>댓글 <?php echo $commentsCount; ?>개</a></div>
		</div>
		<div class="writemenu">
			<?php if(!$isReply): ?>
			<div class='reply'><a href="<?php echo FREEBOARD_URL."reply/".$number; ?>">답글</a></div>
			<?php endif; ?>
			<div class='list'><a href="<?php echo FREEBOARD_URL.$listUri; ?>">목록</a></div>
			<?php if($isWritten): ?>
			<div class='edit'><a href="<?php echo FREEBOARD_URL."edit/".$number; ?>">수정</a></div>
			<div class='delete'><a href="<?php echo FREEBOARD_URL."delete/".$number; ?>">삭제</a></div>
			<?php endif; ?>
		</div>
	</div>
</div>