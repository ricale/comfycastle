<div id="container">

	<form id="write" method="post" autocomplete="off" enctype="multipart/form-data">
		<input type="hidden" name="number"  value="<?php echo $number?>">
		<input type="hidden" name="isreply"  value="<?php echo $isReply?>">
		
		<table>
		<?php if(!$isLogin): ?>
		<tr>
			<th>이름</th>
			<td>
				<input type='text' name='name' maxlength='<?php echo NAME_MAXLENGTH; ?>' value='<?php echo $name; ?>' <?php echo $nameReadOnly; ?>>
			</td>
		</tr>
		<tr>
			<th>비밀번호</th>
			<td>
				<input type='password' name='pw' maxlength='<?php echo PASSWORD_MAXLENGTH; ?>'>
			</td>
		</tr>
		<?php endif; ?>
		<tr>
			<th>제목</th>
			<td>
				<input type="text" name="title" maxlength="<?php echo TITLE_MAXLENGTH; ?>" value="<?php echo $title; ?>">
				<label for="autonl">자동줄바꿈 </label><input id="autonl" type="checkbox" name="autonl" value="1" <?php echo $anlChecked; ?>>
			</td>
		</tr>
		<tr>
			<th>내용</th>
			<td>
				<textarea name="content"><?php echo $content?></textarea>
			</td>
		</tr>
		
		<tr>
			<th>업로드</th>
			<td>
				<input type="file" name="userfile" size="20">
			</td>
		</tr>
		<tr>
			<th></th>
			<td><input type="submit" value="완료"><span id="isvalid"></span></td>
		</tr>
		</table>
	</form>

</div>