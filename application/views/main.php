<div id="container">
	<h1><?php echo SITE_TITLE; ?></h1>
	<ul class="menu">
		<?php for($i = 0; $i < sizeof($menu); $i++): ?>
		<li><a href="<?php echo $href[$i]; ?>"><?php echo $menu[$i]; ?></a></li>
		<?php endfor; ?>
		<?php for($i = 0; $i < sizeof($inactivate); $i++): ?>
		<li class="inactivate"><?php echo $inactivate[$i]; ?></li>
		<?php endfor; ?>
	</ul>
	<div id="fblike">
		<div class="fb-like" data-href="http://comfycastle.net/" data-send="true" data-layout="button_count" data-width="450" data-show-faces="true"></div>
	</div>
</div>