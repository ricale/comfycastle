<!DOCTYPE html>
<html>
<head>
	<title><?php echo SITE_TITLE.$pageTitle; ?></title>

	<meta property="fb:admins" content="100003560967005"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css/base.css">
	<?php if(is_array($css)): for($i = 0; $i < sizeof($css); $i++): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css/<?php echo $css[$i]?>.css">
	<?php endfor; endif; ?>
	
	<?php if(is_array($script)): ?>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.form.js"></script> 
	<?php for($i = 0; $i < sizeof($script); $i++): ?>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/<?php echo $script[$i]; ?>.js" ></script>
	<?php endfor; ?>
	<?php endif; ?>

</head>
<body>
	<div id="fb-root"></div>

	<div id="commonheader">
		<ul class="menu">
			<?php if($loggedin): ?>
			<li><a href="<?php echo ACCOUNT_URL."menu/"; ?>"><?php echo $nickname; ?>님 환영합니다.</a></li>
			<li><a href="<?php echo ACCOUNT_URL."logout/".str_replace('/', TOKEN_FOR_MAKE_URI_FROM_URIS, uri_string()); ?>">로그아웃</a></li>
			<?php else: ?>
				<?php if($needLoginLink): ?>
				<li><a href="<?php echo ACCOUNT_URL."login/".str_replace('/', TOKEN_FOR_MAKE_URI_FROM_URIS, uri_string()); ?>">로그인</a></li>
				<?php endif; ?>
				<?php if($needJoinLink): ?>
				<li><a href="<?php echo ACCOUNT_URL."join/"; ?>">가입</a></li>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php if($needHomeLink): ?>
			<li><a href="<?php echo SITE_URL; ?>">첫페이지</a></li>
			<?php endif; ?>
		</ul>
	</div>