<form id="container" method="post" action="" autocomplete="off">
	<fieldset>
		<input type="hidden" name="valid" value="1">
		<div class="row">
			<label for="id">　아이디</label>
			<input type="text" name="id" size="30" maxlength=<?php echo ID_MAXLENGTH; ?> value="<?php echo $id; ?>" readonly>
		</div>

		<div class="row">
			<label for="pw">비밀번호</label>
			<input type="password" name="pw" size="30" maxlength=<?php echo PASSWORD_MAXLENGTH; ?>>
		</div>

		<div class="row">
			<label for="name">이　　름</label>
			<input type="text" name="name" size="30" maxlength=<?php echo NAME_MAXLENGTH; ?> value="<?php echo $name; ?>">
		</div>
	
		<div id="isvalid_name" class="isvalid row"></div>

		<div class="row">
			<label for="mail">메　　일</label>
			<input type="text" name="mail" size="30" maxlength=<?php echo MAIL_MAXLENGTH; ?> value="<?php echo $mail; ?>">
			<input type="button" name="checkmail" value="중복체크">
		</div>
			
		<div id="isvalid_mail" class="isvalid row"></div>

		<div>
			<label for="note">비　　고</label>
			<textarea name="note" cols="30" rows="4"><?php echo $note; ?></textarea>
		</div>
		
		<input id="submit" type="submit" value="수정">

		<div id="isvalid"></div>	
	</fieldset>
</form>