<form id="container" method="post" action="" autocomplete="off">
	<fieldset>
		<input type="hidden" name="valid" value="1">

		<div class="row">
			<label for="id">　아이디</label>
			<input type="text" name="id" size="30" maxlength=<?php echo ID_MAXLENGTH; ?>>
			<input type="button" name="checkid" value="중복체크">
		</div>
				
		<div id="isvalid_id" class="isvalid row"></div>
			
		<div class="row">
			<label for="pw">비밀번호</label>
			<input type="password" name="pw" size="30" maxlength=<?php echo PASSWORD_MAXLENGTH; ?>>
		</div>
		<div class="row">
			<label for="repw">확　　인</label>
			<input type="password" name="repw" size="30" maxlength=<?php echo PASSWORD_MAXLENGTH; ?>>
		</div>
			
		<div id="isvalid_password" class="isvalid row"></div>
			
		<div class="row">
			<label for="name">이　　름</label>
			<input type="text" name="name" size="30" maxlength=<?php echo NAME_MAXLENGTH; ?>>
		</div>
			

		<div id="isvalid_name" class="isvalid row"></div>
			
		<div class="row">
			<label for="mail">메　　일</label>
			<input type="text" name="mail" size="30" maxlength=<?php echo MAIL_MAXLENGTH; ?>>
			<input type="button" name="checkmail" value="중복체크">
		</div>
			
		<div id="isvalid_mail" class="isvalid row"></div>
			
		<div>
			<label for="note">비　　고</label>
			<textarea name="note" cols="30" rows="4"></textarea>
		</div>
			
		<input id="submit" type="submit" value="가입">
			
		<div id="isvalid"></div>
	</fieldset>
</form>
