$(document).ready(function() {
	var TITLE_MAXLENGTH = 100,
	    UPLOADS_PER_WRITING = 5;

	var number = $('#write').find('input[name="writing_id"]').val(),
	    url = location.protocol+'//'+location.host;
	
	url += number == "" ? '/blog/writing' : '/blog/editing';

	$('#write').ajaxForm({
		url:url,
		dataType:'json',
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['success']) {
				alert("잘못된 접근입니다.");
			} else {
			/*	var uploadsError = "";
				for(var i = 0; i < UPLOADS_PER_WRITING; i++) {
					if(result['uploads'][i] != null && result['uploads'][i] != "") {
						uploadsError += "\n 파일#" + (i + 1) + ": " + result['uploads'][i];
					}
				}

				if(uploadsError.length != 0) {
					alert("File Upload Error" + uploadsError);
				}
			*/
				parent.document.location.href = number == "" ? '/blog/' : '/blog/'+number;
			}
		}
	});
	
	$('#write').find('input[type="submit"]').click(function(event) {		
		var message  = $('#write').find('#isvalid'),
		    title    = $('#write').find('input[name="title"]').val(),
		    content  = $('#write').find('textarea[name="content"]').val(),
		    tag      = $('#write').find('.tag').val()
		    
		if(title.length == 0) {
			message.html('제목을 입력하세요.');
			event.preventDefault();
			return;
		} else if(title.length > TITLE_MAXLENGTH) {
			message.html('제목은 100자 이내로 작정해주세요.');
			event.preventDefault();
			return;
		}
		
		if(content.length == 0) {
			message.html('내용을 입력하세요.');
			event.preventDefault();
			return;
		}

		if(tag != undefined) {
			for(var i = 0; i < tag.length; i++) {
				if(tag.eq(i).text().length > TAG_MAXLENGTH) {
					message.html('태그는 '+TAG_MAXLENGTH+'자 이내로 작정해주세요.');
					event.preventDefault();
					return;
				}
			}
		}
	});

	$('#write').find('input[name="tagger"]').bind('keypress', function(event) {
		if(event.which == 44/*','*/) {
			$('#tagbox').find('td').append('<span class="tagaction"></span> ')
			    .find('.tagaction').last().append('<span class="tag">'+$(this).val()
			    	+'</span> <a class="delete" href="">삭제</a><input type="hidden" name="tags[]" value="'+$(this).val()+'">')
			    .find('.delete').click(function(event) {
			    	event.preventDefault();
			    	$(this).closest('.tagaction').remove();
			    });

			$(this).val('');
			event.preventDefault();
		}
	});

	for(var i = 0; i < UPLOADS_PER_WRITING; i++) {
		var uploads = $('#write').find('#userfile'+i);

		if(uploads.html() != undefined) {
			uploads.find('input[type="button"]').click(function(event) {
				$(this).closest('tr').find('input[type="hidden"]').prop('disabled', false);
				$(this).closest('td').html('<input type="file" name="'+ $(this).closest('td').attr('id') +'" size="20">');
			});
		}
	}

	var tag = $('.tag');
	for(var i = 0; tag.eq(i).html() !== undefined; i++) {
		tag.eq(i).closest('.tagaction').find('a.delete').click(function(event) {
			event.preventDefault();
			$(this).closest('.tagaction').next().prop('disabled', false);
			$(this).closest('.tagaction').remove();
		});
	}
});