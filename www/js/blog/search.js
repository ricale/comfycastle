if(typeof(RICALE) == typeof(undefined)) {
	var RICALE = {};
}

RICALE.searchWritings = function(event) {
	event.preventDefault();
	
	var keyword = $('#search').find('input[name="keyword"]').val();
	var st = $('#search').find('input[name="st"]:checked').val();
	var sc = $('#search').find('input[name="sc"]:checked').val();
	
	if(keyword != "" && (st != undefined || sc != undefined)) {
	 	var uri = $('#search').find('input[name="target"]').val() + "keyword/" + keyword + "/";
	 	
	 	var option = "";
	 	if(st != undefined) {
	 		option += "y";
	 	} else {
	 		option += "n";
	 	}
	 	
	 	if(sc != undefined) {
	 		option += "y";
	 	} else {
	 		option += "n";
	 	}
	 	
	 	if(option != "yy") {
	 		uri += option;
	 	}
	 	
	 	parent.document.location.href = uri;
	}
};

$(document).ready(function() {
	$('#search').find('input[type="button"]').click(function(event) {
		RICALE.searchWritings(event);
	}); // end .click(function()
	
	$('#search').find('input[type="text"]').keydown(function(event) {
		if(event.which == 13/*enter key*/) {
			RICALE.searchWritings(event);
		}
	});
}); // end .ready(function()