$(document).ready(function() {
	RICALE.pollingCmtMng.setMainUri("blog");
	$('.writeaction').find('.cnt').find('a').click(RICALE.startPollingComment);

	$('.fbshare').eq(0).click(function(event) {
		event.preventDefault();
		var caption = $(this).closest('.writing').find('.title').text(),
		    description = ($(this).closest('.writing').find('.content').text()).substring(0, 100);

		if(description.length >= 100) {
			description = description.concat('......')
		}

		RICALE.shareOnFB(location.href, 'comfycastle blog', caption, description);
	});

	if($('textarea').val() != "") {
		RICALE.hmd.run('textarea', '.content');
	}
});