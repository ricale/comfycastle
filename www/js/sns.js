// 소스 코드에 직접 놓을 것인가.
// 계속 document의 ready 이벤트에 넣어놓을 것인가.
// 속도 차이가 심하면 소스 코드(뷰)에 직접 집어넣자.
$(document).ready(function() {

	// Load the SDK's source Asynchronously
	// Note that the debug version is being actively developed and might
	// contain some type checks that are overly strict. 
	// Please report such bugs using the bugs tool.
	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/ko_KR/all.js#xfbml=1&appId=469254379814630";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));


	////
	//// 트위터 초기화
	!function(d,s,id) {
		var js,fjs=d.getElementsByTagName(s)[0];
		if(!d.getElementById(id)) {
			js=d.createElement(s);
			js.id=id;
			js.src="//platform.twitter.com/widgets.js";
			fjs.parentNode.insertBefore(js,fjs);
		}
	}(document,"script","twitter-wjs");
});

if(typeof(RICALE) == typeof(undefined)) {
	var RICALE = {};
}

RICALE.shareOnFB = function(link, name, caption, description) {
	var obj = {
		method: 'feed',
		link: link,
		picture: 'http://fbrell.com/f8.jpg',
		name: name,
		caption: caption,
	};

	if(description !== '') {
		obj.description = description;
	}

	
	FB.ui(obj, function(response) {

	});
};
