$(document).ready(function() {
	var PAGE_TITLE = '/linkset/';

	$('#admin').ajaxForm({
		url:location.protocol+'//'+location.host+PAGE_TITLE+"administrating/basic",
		dataType:'json',
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['success']) {
				$('#admin').find('.isvalid').text('유효하지 않습니다.').show().fadeOut(3000);
			} else {
				$('#admin').find('.isvalid').text('저장되었습니다.').show().fadeOut(3000);
			}
		}
	});

	$('#favoritag').ajaxForm({
		url:location.protocol+'//'+location.host+PAGE_TITLE+"administrating/favoritag",
		dataType:'json',
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['success']) {
				$('#favoritag').find('.isvalid').text('사용되고 있지 않은 태그는 등록할 수 없습니다.').show().fadeOut(3000);
			} else {
				$('#favoritag').find('.isvalid').text('저장되었습니다.').show().fadeOut(3000);

				var newHidden = $('#favoritag').find('input[name="tags\[\]"]');
				for(var i = 0; i < newHidden.length; i++) {
					newHidden.closest('.tagaction').remove();
				}

				var delHidden = $('#favoritag').find('input[name="deletetags\[\]"]');
				for(var i = 0; i < delHidden.length; i++) {
					if(delHidden.eq(i).prop('disabled') === false) {
						delHidden.eq(i).remove();
					}
				}

				for(var i = 0; i < result['tags'].length; i++) {
					$('#tagbox_already').append('<span class="tagaction"><span class="tag">'
						+ result['tags'][i]['name']
						+ '</span> <a class="delete" href="">삭제</a></span><input type="hidden" name="deletetags[]" value="'
						+ result['tags'][i]['tag_id']
						+ '" disabled> ');
				}
			}
		}
	});

	var onKeyPress = function(event) {
		if(event.which == 44/*','*/) {
			$('#tagbox').append('<span class="tagaction"></span> ')
			    .find('.tagaction').last().append('<span class="tag">'+$(this).val()
			    	+'</span> <a class="delete" href="">삭제</a><input type="hidden" name="tags[]" value="'+$(this).val()+'">')
			    .find('.delete').click(function(event) {
			    	event.preventDefault();
			    	$(this).closest('.tagaction').remove();
			    });

			$(this).val('');
			event.preventDefault();
		}
	};
	$('#favoritag').find('input[name="tagger"]').bind('keypress', onKeyPress);

	var tag = $('.tag');
	for(var i = 0; tag.eq(i).html() !== undefined; i++) {
		tag.eq(i).closest('.tagaction').find('a.delete').click(function(event) {
			event.preventDefault();
			$(this).closest('.tagaction').next().prop('disabled', false);
			$(this).closest('.tagaction').remove();
		});
	}
});