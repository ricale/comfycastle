$(document).ready(function() {
	var TITLE_MAXLENGTH = 100,
	    URL_MAXLENGTH = 255,
	    TAG_MAXLENGTH = 20,
	    PAGE_TITLE = '/linkset/';

	var number = $('#write').find('input[name="item_id"]').val(),
	    url = location.protocol+'//'+location.host;
	
	url += number == "" ? PAGE_TITLE + 'writing' : PAGE_TITLE + 'editing';

	$('#write').ajaxForm({
		url:url,
		dataType:'json',
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['success']) {
				alert("잘못된 접근입니다.");
			} else {
				if(result['invalid'] > 0) {
					alert(result['invalid'] + "개의 링크가 등록되지 못했습니다. 제목 혹은 url, 태그 값이 올바른 지 확인하십시오.");
				}
				parent.document.location.href = PAGE_TITLE + result['userId'];
			}
		}
	});
	
	$('#write').find('input[type="submit"]').click(function(event) {	
		var message = $('#write').find('#isvalid'),
		    title   = $('#write').find('input[name="title\[\]"]'),
		    url     = $('#write').find('input[name="url\[\]"]'),
		    tag     = $('#write').find('.tag');

		for(var i = 0; i < title.length; i++) {
			if(title.eq(i).length > TITLE_MAXLENGTH) {
				message.html('제목은 '+TITLE_MAXLENGTH+'자 이내로 작정해주세요.');
				event.preventDefault();
				return;
			}
		}

		if(url.eq(0).length == 0) {
			message.html('주소를 입력하세요.');
			event.preventDefault();
			return;
		} else if(url.eq(0).length > URL_MAXLENGTH) {
			message.html('주소는 '+TITLE_MAXLENGTH+'자 이내로 작정해주세요.');
			event.preventDefault();
			return;
		}

		for(var i = 1; i < url.length; i++) {
			if(url.eq(i).length > URL_MAXLENGTH) {
				message.html('주소는 '+TITLE_MAXLENGTH+'자 이내로 작정해주세요.');
				event.preventDefault();
				return;
			}
		}

		if(tag != undefined) {
			for(var i = 0; i < tag.length; i++) {
				if(tag.eq(i).text().length > TAG_MAXLENGTH) {
					message.html('태그는 '+TAG_MAXLENGTH+'자 이내로 작정해주세요.');
					event.preventDefault();
					return;
				}
			}
		}

		message.html('작성 중입니다. 잠시만 기다려주세요.');

		// 크롬에서는 disabled 하면 form 자체가 disabled 된다. 그래서 일단 주석 처리
		//$('#write').find('input[type="submit"]').prop("disabled", true);
	});

	var onKeyPress = function(event) {
		if(event.which == 44/*','*/) {
			var id = Number($(this).closest('table').attr('id').substr(4));
			$(this).closest('table').find('.tagbox').find('td').append('<span class="tagaction"></span> ')
			    .find('.tagaction').last().append('<span class="tag">'+$(this).val()
			    	+'</span> <a class="delete" href="">삭제</a><input type="hidden" name="tags'+id+'[]" value="'+$(this).val()+'">')
			    .find('.delete').click(function(event) {
			    	event.preventDefault();
			    	$(this).closest('.tagaction').remove();
			    });

			$(this).val('');
			event.preventDefault();
		}
	};
	$('#write').find('input[name="tagger1"]').bind('keypress', onKeyPress);

	$('#write').find('input[type="button"]').click(function(event) {
		var lastInput = $('#write').find('table').last(),
		    id = Number(lastInput.attr('id').substr(4)) + 1;

		lastInput.after('<table id="link'+id+'">'
			+ '<tr><th>제목</th><td><input type="text" name="title[]" maxlength="'+TITLE_MAXLENGTH+'" size="100"></td></tr>'
			+ '<tr><th>URL</th><td><input type="text" name="url[]" maxlength="'+URL_MAXLENGTH+'" size="100"></td></tr>'
			+ '<tr><th>태그</th><td><input type="text" name="tagger'+id+'"></td></tr>'
			+ '<tr class="tagbox"><th></th><td></td></tr></table>');
		lastInput.next().find('input[name="tagger'+id+'"]').bind('keypress', onKeyPress);
	});	

	var tag = $('.tag');
	for(var i = 0; tag.eq(i).html() !== undefined; i++) {
		tag.eq(i).closest('.tagaction').find('a.delete').click(function(event) {
			event.preventDefault();
			$(this).closest('.tagaction').next().prop('disabled', false);
			$(this).closest('.tagaction').remove();
		});
	}
});