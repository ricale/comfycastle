$(document).ready(function() {	

	$('#delete').ajaxForm({
		url:location.protocol+'//'+location.host+'/linkset/deleting',
		dataType:'json',
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['success']) {
				alert("잘못된 접근입니다.");
			} else {
				parent.document.location.href = '/linkset/';
			}
		}
	});
});