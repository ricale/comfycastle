if(typeof(RICALE) == typeof(undefined)) {
	/**
	 * @namespace 네임스페이스
	 * @auther ricale
 	 * @version 1
 	 * @description 다른 사람의 구현과의 충돌을 피하기 위한 최상위 네임스페이스
	 */
	var RICALE = {};
}

/** 아이디의 최고 길이 */
RICALE.ID_MAXLENGTH = 15;
/** 비밀번호의 최고 길이 */
RICALE.PASSWORD_MAXLENGTH = 20;

// replyCmt에서 현재 세션의 로그인 여부를 알기 위해 어쩔 수 없이 전역적 변수 선언
RICALE.session = null;

/**
 * @class 댓글 폴링 클래스.
 * @description RICALE 네임 스페이스의 updateCmt 메서드와 함께 사용된다.
 * @auther ricale
 * @version 1
 * @see RICALE.updateCmts
 */
RICALE.PollingComment = function() {
}


RICALE.PollingComment.prototype = {
	/** 가장 최근에 댓글이 업데이트 되었던 시간. 0이면 아직 한 번도 업데이트된 적이 없음을 뜻한다.*/
	timestamp: 0,
	/** 댓글을 폴링할 시간 간격. 밀리초 단위. */
	interval: 3000,
	/** 댓글을 폴링할 타이머 객체. interval에 의해 호출 시간 간격이 결정된다. */
	timer: null,
	/** 댓글을 폴링할 글의 번호(고유 아이디) */
	writing: null,
	/**
	 * 댓글의 폴링을 시작한다.
	 * @param writingId 댓글을 폴링할 글의 번호
	 */
	start: function(writingId)	{
		this.writing = writingId;
		RICALE.updateCmts(this.writing, this.timestamp);
	},
	/**
	 * 폴링 기능의 본체.
	 * @param isChangedByMe 값이 true라면 댓글을 한 번 업데이트할 뿐 폴링 기능을 활성화하지는 않는다.
	 */
	run: function(isChangedByMe) {
		var n = this.writing,
			t = this.timestamp;

		this.stop();
		if(isChangedByMe) {
			RICALE.updateCmts(n, t);
		} else {
			this.timer = setTimeout(function() {
				RICALE.updateCmts(n, t)
			}, this.interval);
		}
	},
	/** 댓글 폴링을 일시적으로 멈춘다. */
	stop: function() {
		if(this.timer) clearTimeout(this.timer);
	},
	/** 댓글 폴링을 끝낸다. */
	quit: function() {
		this.stop();
		this.timestamp = 0;
		this.writing = null;
	},
	/**
	 * 댓글 폴링 기능을 사용할 페이지의 uri를 저장한다.
	 * @param uri 폴링 기능을 사용할 페이지의 uri. uri/comment 에 폴링에 관련된 기능이 구현되어 있어야 한다.
	 */
	setMainUri: function(uri/* '/someuri/' */) {
		RICALE.MAIN_URI = uri;
	}
}

RICALE.pollingCmtMng = new RICALE.PollingComment();

// 아래 함수들이 PollingComment 객체에 들어가지 못한 이유는 ajax를 통해 호출되는 이벤트 리스너 콜백 함수에서
// this로 객체를 가리킬 수 없기 때문이다. 이벤트 리스너 콜백 함수이기 때문에 this는 자기 자신 혹은 이벤트를 일으킨 객체를 가리킬 뿐이다.
// 따라서 따로 분리하여 이용한다.
/**
 * ajax 기능을 통해 댓글을 업데이트 하는 함수.
 * 폴링 PollingComment 객체에 의해 주기적으로 호출된다.
 * 최초의 업데이트 때 댓글 입력 폼을 생성하며 해당 폼의 submit 이벤트에 writeCmt 함수를 이벤트 리스너로 등록한다.
 * 각각의 댓글의 해당 링크(혹은 버튼)에는 replyCmt, editCmt, removeCmt 함수를 이벤트 리스너로 등록한다.
 * @param writingId 댓글을 업데이트 할 글의 번호
 * @param timestamp 마지막으로 댓글이 업데이트되었던 시간. 이 시간 이전의 변화는 모두 무시한다.
 * @see RICALE.writeCmt
 * @see RICALE.editCmt
 * @see RICALE.removeCmt
 * @see RICALE.replyCmt
 */
RICALE.updateCmts = function(writingId, timestamp) {
	var url = location.protocol + '//' + location.host + '/' + RICALE.MAIN_URI + '/comment/show';

	$.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data: {
			'valid':1,
			'timestamp':timestamp,
			'writing_id':writingId
		},
		/*error:function(result, str, htp) {
			alert("ERROR:"+result + ";"+str + ";" + htp);
		},*/
		success:function(result) {
			var lastCmtbCnt = 0,
			    nowCmtbCnt = 0,
				late = 0; // 가장 늦게 작성된 댓글의 작성 시간
				fbUrl = location.protocol + '//' + location.host + '/' + RICALE.MAIN_URI + '/' + writingId;
			RICALE.session = result['session'];
			
			if(result['isall']) {
				$('#writing'+writingId).after('<div id="comment"><div id="commentlist"></div><form id="commentinput"></form></div>')
				
				if(!RICALE.session) {
					$('#commentinput').append('이름<input type="input" name="name"> 비밀번호<input type="password" name="pw">')
				}
				$('#commentinput').append('<textarea name="content"></textarea> <input type="submit" value="확인"><span id="isvalid"></span>')
				$('#commentinput').submit(RICALE.writeCmt);
				$('#writing'+writingId).closest('.article').find('.fbcomment').css('display', 'block');
			}
			
			if(result['content']) {
				lastCmtbCnt = $('#comment').find('.commentbox').length;
				for(var i = 0; (i < result['content'].length) && result['isdelete'][i]; i++) {
					if(!result['isall']) {
						$("#comment" + result['comment_id'][i]).remove();
					}
					
					if(result['times'][i] > late) {
						late = result['times'][i];
					} 
				}
				
				for( ; i < result['content'].length; i++) {
					var cmtClass = result['id'][i] ? "commentbox" : "guest commentbox",
					    familyId = "commentfamily" + result['family'][i],
						id       = "comment" + result['comment_id'][i],
						d      = new Date(result['times'][i]*1000), //PHP time() - 초단위, javascript Date() - 밀리초단위
						year   = d.getFullYear(),
						month  = (d.getMonth()+1) < 10 ? "0" + (d.getMonth()+1) : (d.getMonth()+1),
						day    = d.getDate()      < 10 ? "0" + d.getDate()      : d.getDate(),
						hour   = d.getHours()     < 10 ? "0" + d.getHours()     : d.getHours(),
						minute = d.getMinutes()   < 10 ? "0" + d.getMinutes()   : d.getMinutes(),
						second = d.getSeconds()   < 10 ? "0" + d.getSeconds()   : d.getSeconds(),
						time   = year+"."+month+"."+day+"("+hour+":"+minute+":"+second+")",
						writerId = result['id'][i] ? "("+result['id'][i]+")" : "",
						content = result['content'][i];//.replace(/\n/g, '<br/>');
						
					if($('#'+id).html()) {
						$('#'+id).find('.content').html(content);
						$('#'+id).find('.times').html(time);
					} else {
						if(!$('#'+familyId).html()) {
							$('#commentlist').append('<div id="'+familyId+'"></div>')
						} else {
							cmtClass += " replycomment"
						}
						
						$('#'+familyId).append('<div class="'+cmtClass+'" id="'+id+'"></div>')
							.find('#'+id).append('<div class="name">'+result['name'][i]+'</div>'
									             + '<div class="id">'+writerId+'</div>'
									             + ' <div class="time">'+time+'</div>'
									             + '<div class="menu"></div>'
									             + '<div class="content">'+content+'</div>');
						
						if(!$('#'+id).attr('class').match('replycomment')) {
							$('#'+id).find('.menu').append('<div class="reply"><a href="">답글</a></div> ')
						                       .find('.reply').find('a').click(RICALE.replyCmt);
						}
									     
						if((RICALE.session == result['id'][i]) || (!RICALE.session && !result['id'][i])) {
							$('#'+id).find('.menu').append('<div class="edit"><a href="">수정</a></div> <div class="delete"><a href="">삭제</a></div>')
								                   .find('.edit').find('a').click(RICALE.editCmt)
								                   .closest('.edit').next().find('a').click(RICALE.removeCmt);
						}
					}
					
					if(result['times'][i] > late) {
						late = result['times'][i];
					}
				}
				
				RICALE.pollingCmtMng.timestamp = late;
				
				nowCmtbCnt = $('#comment').find('.commentbox').length;
				if(lastCmtbCnt !== nowCmtbCnt) {
					$('#comment').prev().find('.writeaction').find('.cnt').find('a').html('댓글 '+nowCmtbCnt+'개');
				}
			} else {
				if(result['updateTime']) {
					RICALE.pollingCmtMng.timestamp = result['updateTime'];
				}
			}
			RICALE.pollingCmtMng.run();
		}
	});
}

/**
 * ajax 기능을 통해 비회원의 댓글을 삭제하는 함수.
 * 이벤트 리스너로서 활용되며 댓글 삭제에 성공하면 PollingComment 객체의 run(true) 함수를 호출한다.
 * @param event 이벤트 객체
 * @see RICALE.PollingComment
 * @see RICALE.removeCmt
 */
RICALE.removeGuestCmt = function(event) { // 비회원 댓글 삭제를 위한 이벤트 핸들러(ajax 사용)
	var url    = location.protocol + '//' + location.host + '/' + RICALE.MAIN_URI + '/comment/delete',
		id     = $(this).closest('.commentbox').attr('id'),
		number = Number(id.substring(7)),
		pw     = $(this).closest('.commentbox').find('input[type="password"]').val();
	
	$.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data: {
			'valid':1,
			'comment_id':number,
			'pw':pw
		},
/*		error:function(result, str, htp) {}
			alert(str);
		},*/
		success:function(result) {						
			if(!result['isvalid'])
				alert("비밀번호가 틀렸습니다.");
			else
				RICALE.pollingCmtMng.run(true);
		}
	});
};

/**
 * ajax 기능을 통해 회원의 댓글을 삭제하는 함수.
 * 이벤트 리스너로서 활용되며 댓글 삭제에 성공하면 PollingComment 객체의 run(true) 함수를 호출한다.
 * 삭제를 시도하는 댓글이 비회원의 댓글이라면 비밀번호 입력 폼을 생성하여 해당 폼의 submit 이벤트에 removeGuestCmt함수를 이벤트 리스너로 등록한다.
 * @param event 이벤트 객체
 * @see RICALE.PollingComment
 * @see RICALE.updateCmts
 * @see RICALE.removeGuestCmt
 */
RICALE.removeCmt = function(event) { // 회원 댓글 삭제를 위한 이벤트 핸들러 (ajax 활용)
	var url    = location.protocol + '//' + location.host + '/' + RICALE.MAIN_URI + '/comment/delete',
		id     = $(this).closest('.commentbox').attr('id'),
		number = Number(id.substring(7));
	
	event.preventDefault();
	
	if(!$('#'+id).hasClass('guest')) {
		$.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data: {
				'valid':1,
				'comment_id':number
			},
/*			error:function(result, str, htp)
			{
				alert(str);
			},*/
			success:function(result) {						
				if(!result['isvalid'])
					alert(id+"비정상적인 접근입니다.");
				else
					RICALE.pollingCmtMng.run(true);
			}
		});
	} else {
		if(!$('#'+id).find('.getpw').html()) {
			$('#'+id).find('.menu').after('<div class="getpw"></div>')
				.next().append('비밀번호 <input type="password"> <input type="button" value="삭제">');
		} else {
			$('#'+id).find('#commentedit').remove();
			$('#'+id).find('.content').removeClass('hidden');
			$('#'+id).find('input[type="button"]').val("삭제");
			$('#'+id).find('.getpw').find('input[type="button"]').unbind('click');
		}

		$('#'+id).find('.getpw').find('input[type="button"]').click(RICALE.removeGuestCmt);
	}
};

/**
 * ajax 기능을 통해 댓글을 수정하는 함수.
 * 이벤트 리스너로서 활용되며 댓글 수정에 성공하면 PollingComment 객체의 run(true) 함수를 호출한다.
 * @param event 이벤트 객체
 * @see RICALE.PollingComment
 * @see RICALE.editCmt
 */
RICALE.editingCmt = function(event) {
	var url     = location.protocol + '//' + location.host + '/' + RICALE.MAIN_URI + '/comment/edit',
		id      = $(this).closest('.commentbox').attr('id'),
		number  = Number(id.substring(7)),
		pw      = $('#'+id).find('input[type="password"]').val(),
		content = $('#'+id).find('textarea').val();
	
	$.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data: {
			'valid':1,
			'comment_id':number,
			'pw':pw,
			'content':content
		},
/*		error:function(result, str, htp)
		{
			alert(str);
		},*/
		success:function(result) {
			if(result['isvalid']) {				
				$('#'+id).find('.content').removeClass('hidden');
				$('#'+id).find('#commentedit').remove();
				$('#'+id).find('.getpw').remove();
				
				RICALE.pollingCmtMng.run(true);
			} else {
				alert("비밀번호가 틀렸습니다.");
			}
	  }
	});
};

/**
 * 댓글의 수정 폼을 생성하는 함수.
 * 이벤트 리스너로서 활용된다.
 * 생성한 폼의 submit 이벤트에 editingCmt 함수를 이벤트 리스너로 등록한다.
 * @param event 이벤트 객체
 * @see RICALE.updateCmts
 * @see RICALE.editingCmt
 */
RICALE.editCmt = function(event) {
	var str = "",
		id = $(this).closest('.commentbox').attr('id');
	
	event.preventDefault();

	if(!$('#'+id).find('#commentedit').html()) {
		if(!$('#'+id).find('.getpw').html()) {
			$('#'+id).find('.menu').after('<div class="getpw"><input type="button" value="수정"></div>')
				
			if($('#'+id).hasClass('guest')) {
				$('#'+id).find('.getpw').find('input[type="button"]').before('비밀번호 <input type="password"> ');
			}
			
		} else {
			$('#'+id).find('input[type="button"]').val("수정");
			$('#'+id).find('.getpw').find('input[type="button"]').unbind('click');
		}
		
		$('#'+id).find('.getpw').find('input[type="button"]').click(RICALE.editingCmt);
		
		str = ($('#'+id).find('.content').html()).replace(/<br\s?[\/]?>/g, '').replace(/&lt;/g, '<').replace(/&gt;/g, '>');
		$('#'+id).find('.content').after('<form id="commentedit"><textarea>' + str + '</textarea></form>')
		$('#'+id).find('.content').addClass('hidden');
	}
};

/**
 * ajax 기능을 통해 댓글의 댓글을 생성하는 함수.
 * 이벤트 리스너로서 활용된다.
 * @param event 이벤트 객체
 * @see RICALE.PollingComment
 * @see RICALE.replyCmt
 */
RICALE.replyingCmt = function(event) {
	var url     = location.protocol + '//' + location.host + '/' + RICALE.MAIN_URI + '/comment/reply',
		number  = Number($(this).closest('#comment').prev().attr('id').substring(7)),
		parent  = Number($(this).closest('div').attr('id').substring(13)),
		message = $('#replyinput').find('#isvalid'),
		name    = $('#replyinput').find('input[name="name"]').val(),
		pw      = $('#replyinput').find('input[name="pw"]').val(),
		content = $('#replyinput').find('textarea').val();
	
	event.preventDefault();

	///////////////////////////
	/// 입력된 댓글 정보의 유효값을 확인한다.
	///////////////////////////
	
	if(name != undefined) {
		if(name.length == 0) {
			message.html('이름을 입력하세요.');
			return;
		} else if(name.length > RICALE.ID_MAXLENGTH) {
			message.html('이름은 ' + RICALE.ID_MAXLENGTH + '자 이내로 작성해주세요.');
			return;
		}
	}
	
	if(pw != undefined) {
		if(pw.length == 0) {
			message.html('비밀번호를 입력하세요.');
			return;
		} else if(pw.length > RICALE.PASSWORD_MAXLENGTH) {
			message.html('비밀번호는 ' + RICALE.PASSWORD_MAXLENGTH + '자 이내로 작성해주세요.');
			return;
		}
	}
	
	if(content == 0) {
		message.html('내용을 입력하세요.');
		return;
	}
	
	///////////////////////////
	/// 댓글을 업로드한다.
	///////////////////////////
	$.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data: {
			'valid':1,
			'writing_id':number,
			'name':name,
			'pw':pw,
			'content':content,
			'parent':parent
		},
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			$('#replyinput').remove();
			RICALE.pollingCmtMng.run(true);
		}
	});
}

/**
 * 댓글의 댓글을 입력하는 폼을 생성하는 함수.
 * 이벤트 리스너로서 활용된다.
 * 생성한 폼의 submit 이벤트에 replyingCmt 함수를 이벤트 리스너로 등록한다.
 * @param event 이벤트 객체
 * @see RICALE.updateCmts
 * @see RICALE.replyingCmt
 */
RICALE.replyCmt = function(event) {
	var str = "",
		id = $(this).closest('.commentbox').attr('id'),
		lastId = "";
	
	event.preventDefault();
	
	if($('#replyinput').html()) {
		lastId = $('#replyinput').prev().attr('id');
		$('#replyinput').remove();
		
		if(id == lastId) {
			return;
		}
	}
	
	$('#'+id).after('<form id="replyinput"></form>');
		
	if(!RICALE.session) {
		$('#replyinput').append('이름<input type="input" name="name"> 비밀번호<input type="password" name="pw">')
	}
	$('#replyinput').append('<textarea name="content"></textarea><input type="submit" value="확인"><span id="isvalid"></span>');
	$('#replyinput').submit(RICALE.replyingCmt);
}

/**
 * ajax 기능을 통해 댓글 작성을 위한 함수.
 * 이벤트 리스너로서 활용된다.
 * updateComment()가 댓글 입력 폼을 만들면서 해당 폼의 submit 이벤트의 이벤트 리스너로 이 함수를 등록한다.
 * @param event 이벤트 객체
 * @see RICALE.PollingComment
 * @see RICALE.updateCmts
 */
RICALE.writeCmt = function(event) { // 댓글 작성을 위한 이벤트 핸들러 (ajax 활용)
	var url     = location.protocol + '//' + location.host + '/' + RICALE.MAIN_URI + '/comment/write',
		number  = Number($(this).closest('#comment').prev().attr('id').substring(7)),
		name    = $('#commentinput').find('input[name="name"]').val(),
		pw      = $('#commentinput').find('input[name="pw"]').val(),
		content = $('#commentinput').find('textarea').val();
	
	event.preventDefault();

	///////////////////////////
	/// 입력된 댓글 정보의 유효값을 확인한다.
	///////////////////////////
	var message = $('#commentinput').find('#isvalid');
	
	if(name != undefined) {
		if(name.length == 0) {
			message.html('이름을 입력하세요.');
			return;
		} else if(name.length > RICALE.ID_MAXLENGTH) {
			message.html('이름은 ' + RICALE.ID_MAXLENGTH + '자 이내로 작성해주세요.');
			return;
		}
	}
	
	if(pw != undefined) {
		if(pw.length == 0) {
			message.html('비밀번호를 입력하세요.');
			return;
		} else if(pw.length > RICALE.PASSWORD_MAXLENGTH) {
			message.html('비밀번호는 ' + RICALE.PASSWORD_MAXLENGTH + '자 이내로 작성해주세요.');
			return;
		}
	}
	
	if(content == 0) {
		message.html('내용을 입력하세요.');
		return;
	}
	
	$('#commentinput').find('input[name="name"]').val("");
	$('#commentinput').find('input[name="pw"]').val("");
	$('#commentinput').find('textarea').val("");
	
	///////////////////////////
	/// 댓글을 업로드한다.
	///////////////////////////
	$.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data: {
			'valid':1,
			'writing_id':number,
			'name':name,
			'pw':pw,
			'content':content
		},
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			RICALE.pollingCmtMng.run(true);
		}
	});
};

/**
 * 댓글 폴링 기능을 시작한다.
 * 이벤트 리스너로서 활용된다.
 * @param event 이벤트 객체
 * @see RICALE.PollingComment
 * @see RICALE.updateCmts
 */
RICALE.startPollingComment = function(event) {
	var number = Number($(this).closest('.writing').attr('id').substring(7)),
		cmtDivId = $('#comment').prev().attr('id');

	event.preventDefault();
	
	if($('#comment').html()) {
		$('#comment').closest('.article').find('.fbcomment').css('display', 'none');
		$('#comment').remove();
		RICALE.pollingCmtMng.quit();
		if($(this).closest('.writing').attr('id') !== cmtDivId)
			RICALE.pollingCmtMng.start(number);
	} else {
		RICALE.pollingCmtMng.start(number);
	}
};
