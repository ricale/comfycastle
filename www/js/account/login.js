$(document).ready(function() {
	var url = $('#container').find('input[name="url"]').val();
	
	//
	// 폼을 ajax로 연결
	$('#container').ajaxForm({
		url:location.protocol+'//'+location.host+"/account/logining",
		dataType:'json',
		beforeSubmit:function() {

		},
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['isvalid']) {
				alert("아이디나 비밀번호가 맞지 않습니다.");
			} else {
				if(result['iscertify']) {
					parent.document.location = url;
				} else {
					parent.document.location = '/account/certify/never';
				}
			}
		}
	});
	
	$('#container').find('input[type="submit"]').click(function(event) {
		var id  = $('#container').find('input[name="id"]').val(),
		    pw  = $('#container').find('input[name="pw"]').val();
		
		if(id == '') {
			$('#isvalid').html("아이디를 입력해주세요.");
			event.preventDefault();
			return;
		}
		
		if(pw == '') {
			$('#isvalid').html("비밀번호를 입력해주세요.");
			event.preventDefault();
			return;
		}
	}); // end .click(function(event)
}); // end .ready(function()
