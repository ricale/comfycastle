// 싹 다 지역변수다. (const는 지역 상수)
$(document).ready(function() {
	var MESSAGE_VALID = "사용 가능합니다.",
	    MESSAGE_PLZ_INPUT = "입력해주세요";
	    MESSAGE_OVER_MAX_LENGTH = "자 이상 입력해주세요.",
	    MESSAGE_UNDER_MIN_LENGTH = "자 이하로 입력해주세요.",
	    MESSAGE_NO_MATCH = "일치하지 않습니다.",
	    MESSAGE_ID_NO_MATCH_FORM = "알파벳, 숫자, '_', '-', '.' 이외의 문자열은 허용되지 않습니다.",
	    MESSAGE_ID_FIRST_CHAR_NO_MATCH_FORM = "첫 문자는 알파벳이어야 합니다.",
	    MESSAGE_MAIL_NO_MATCH_FORM = "형식을 맞춰주세요. (ab@cd.ef)",
	    MESSAGE_ID_ALREADY_EXIST = "이미 등록된 아이디입니다.",
	    MESSAGE_MAIL_ALREADY_EXIST = "이미 등록된 메일입니다.",
	    MESSAGE_PADEOUT_DURATION = 3000;

	var validation = new RICALE.AccountValidation(),
		idInput   = $('#container').find('input[name="id"]'),
	    mailInput = $('#container').find('input[name="mail"]'),
	    pw = $('#container').find('input[name="pw"]'),
	    re = $('#container').find('input[name="repw"]'),
	    name = $('#container').find('input[name="name"]'),
	    urlExistId   = location.protocol+'//'+location.host+"/account/exist_id/",
	    urlExistMail = location.protocol+'//'+location.host+"/account/exist_mail/";
	//
	// 각 필드의 유효성 검사 함수들
	var onBlur_isValidPassword = function() {
		switch(validation.isValidPassword(pw, re)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid_password').html('');
			break;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid_password').html(PASSWORD_MAXLENGTH + MESSAGE_UNDER_MIN_LENGTH);
			break;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid_password').html(MESSAGE_NO_MATCH);
			break;
		case RICALE.Const.VALID:
			$('#isvalid_password').html(MESSAGE_VALID);
			break;
		}
	};

	//
	// 각각의 필드의 개별적 유효성 검사.
	// 비주얼적인 효과만 있을 뿐 실질적인 효력은 없다.
	// 아이디와 메일은 중복 체크 버튼이 역할을 대신한다.

	$('#container').find('input[name="pw"]').bind('blur', onBlur_isValidPassword);
	$('#container').find('input[name="repw"]').bind('blur', onBlur_isValidPassword);

	$('#container').find('input[name="name"]').bind('blur', function() {
		switch(validation.isValidName(name)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid_name').html('');
			break;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid_name').html(NAME_MAXLENGTH + MESSAGE_OVER_MAX_LENGTH);
			break;
		case RICALE.Const.VALID:
			$('#isvalid_name').html(MESSAGE_VALID);
			break;
		}
	});

	//
	// 아이디의 유효성 검사.
	// ajax를 통한 동적 검사도 포함.
	$('#container').find('input[name="checkid"]').click(function() {
		switch(validation.isValidId(idInput)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid_id').html(MESSAGE_PLZ_INPUT);
			return;
		case RICALE.Const.INVALID_TOO_SHORT:
			$('#isvalid_id').html(ID_MINLENGTH + MESSAGE_OVER_MAX_LENGTH);
			return;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid_id').html(ID_MAXLENGTH + MESSAGE_UNDER_MIN_LENGTH);
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid_id').html(MESSAGE_ID_NO_MATCH_FORM);
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM_FIRST_CHARACTER:
			$('#isvalid_id').html(MESSAGE_ID_FIRST_CHAR_NO_MATCH_FORM);
			return;
		case RICALE.Const.VALID:
			$.ajax({
				url:urlExistId,
				dataType:'json',
				type:'POST',
				data: {
					'id':idInput.val()
				},
				error:function(result, str, htp) {
					alert("ERROR:"+result+";"+str+";"+htp);
				},
				success:function(result) {
					if(result['exist']) {
						$('#isvalid_id').html(MESSAGE_ID_ALREADY_EXIST);
					} else {
						$('#isvalid_id').html(MESSAGE_VALID);
					}
				}
			});
		}
	}); // end .click(function()

	//
	// 메일의 유효성 검사.
	// ajax를 통한 중복 검사도 포함.
	$('#container').find('input[name="checkmail"]').click(function() {
		switch(validation.isValidMail(mailInput)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid_mail').html(MESSAGE_PLZ_INPUT);
			return;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid_mail').html(MAIL_MAXLENGTH + MESSAGE_OVER_MAX_LENGTH);
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid_mail').html(MESSAGE_MAIL_NO_MATCH_FORM);
			return;
		case RICALE.Const.VALID:
			$.ajax({
				url:urlExistMail,
				dataType:'json',
				type:'POST',
				data: {
					'mail':mailInput.val()
				},
				error:function(result, str, htp) {
					alert("ERROR:"+result+";"+str+";"+htp);
				},
				success:function(result) {
					if(result['exist']) {
						$('#isvalid_mail').html(MESSAGE_MAIL_ALREADY_EXIST);
					} else {
						$('#isvalid_mail').html(MESSAGE_VALID);
					}
				}
			});
			break;
		}
	});

	//
	// 폼을 ajax로 연결
	$('#container').ajaxForm({
		url:location.protocol+'//'+location.host+"/account/joining",
		dataType:'json',
		beforeSubmit:function() {

		},
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['isvalid']) {
				alert("잘못된 접근입니다.");
			} else {
				alert("인증 메일이 발송되었습니다. 인증이 완료되면 로그인이 가능합니다.");
	//			alert(result['send']);
				parent.document.location.href = location.protocol+'//'+location.host;
			}
		}
	});
	
	//
	// 폼의 전송을 시도할 때의 유효성 검사
	$('#container').find('input[type="submit"]').click(function(event) {
		event.preventDefault();

		switch(validation.isValidId(idInput)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid').html("아이디를 " + MESSAGE_PLZ_INPUT);
			return;
		case RICALE.Const.INVALID_TOO_SHORT:
			$('#isvalid').html("아이디는 " + ID_MINLENGTH + MESSAGE_OVER_MAX_LENGTH);
			return;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid').html("아이디는 " + ID_MAXLENGTH + MESSAGE_UNDER_MIN_LENGTH);
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid').html("아이디는 " + MESSAGE_ID_NO_MATCH_FORM);
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM_FIRST_CHARACTER:
			$('#isvalid').html("아이디의 " + MESSAGE_ID_FIRST_CHAR_NO_MATCH_FORM);
			return;
		}

		switch(validation.isValidPassword(pw, re)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid').html("비밀번호를 " + MESSAGE_PLZ_INPUT);
			break;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid').html("비밀번호는 " + PASSWORD_MAXLENGTH + MESSAGE_UNDER_MIN_LENGTH);
			break;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid').html("비밀번호와 확인이 " + MESSAGE_NO_MATCH);
			break;
		}

		switch(validation.isValidName(name)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid').html("이름을 " + MESSAGE_PLZ_INPUT);
			break;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid').html("이름은 " + NAME_MAXLENGTH + MESSAGE_OVER_MAX_LENGTH);
			break;
		}

		switch(validation.isValidMail(mailInput)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid').html("메일을 " + MESSAGE_PLZ_INPUT);
			return;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid').html("메일은 " + MAIL_MAXLENGTH + MESSAGE_OVER_MAX_LENGTH);
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid').html("메일의 " + MESSAGE_MAIL_NO_MATCH_FORM);
			return;
		}

		$('#isvalid').html('정보를 서버로 전송 중입니다.');

		//
		// (어떤 방식일지는 알 수 없으나) 입력 단계에서의 중복 검사를 지나치고 값을 넘길 수도 있기 때문에
		// 값을 전송하기 전에 다시 한 번 더 중복 검사를 한다.
		$.ajax({
			url:urlExistMail,
			dataType:'json',
			type:'POST',
			data: {
				'mail':mailInput.val()
			},
			error:function(result, str, htp) {
				alert("ERROR:"+result+";"+str+";"+htp);
			},
			success:function(result) {
				if(result['exist']) {
					$('#isvalid').html(MESSAGE_MAIL_ALREADY_EXIST);
				} else {
					$.ajax({
						url:urlExistId,
						dataType:'json',
						type:'POST',
						data: {
							'id':idInput.val()
						},
						error:function(result, str, htp) {
							alert("ERROR:"+result+";"+str+";"+htp);
						},
						success:function(result) {
							if(result['exist']) {
								$('#isvalid').html(MESSAGE_ID_ALREADY_EXIST);
							} else {
								$('#container').submit();
							}
						}
					});
				}
			}
		});
	}); // end .click(function(event)
}); // end .ready(function()
