$(document).ready(function() {
	var MESSAGE_VALID = "사용 가능합니다.",
	    MESSAGE_PLZ_INPUT = "입력해주세요";
	    MESSAGE_UNDER_MIN_LENGTH = "자 이하로 입력해주세요.",
	    MESSAGE_NO_MATCH = "일치하지 않습니다.";

	var validation = new RICALE.AccountValidation(),
	    pw = $('#container').find('input[name="pw"]'),
	    repw = $('#container').find('input[name="repw"]');
	
	// 각 필드의 유효성 검사 함수들
	var onBlur_isValidPassword = function() {
		switch(validation.isValidPassword(pw, repw)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid_password').html('');
			break;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid_password').html(PASSWORD_MAXLENGTH + MESSAGE_UNDER_MIN_LENGTH);
			break;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid_password').html(MESSAGE_NO_MATCH);
			break;
		case RICALE.Const.VALID:
			$('#isvalid_password').html(MESSAGE_VALID);
			break;
		}
	};

	$('#container').find('input[name="pw"]').bind('blur', onBlur_isValidPassword);
	$('#container').find('input[name="repw"]').bind('blur', onBlur_isValidPassword);

	//
	// 폼을 ajax로 연결
	$('#container').ajaxForm({
		url:location.protocol+'//'+location.host+"/account/changing",
		dataType:'json',
		beforeSubmit:function() {

		},
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['isvalid']) {
				alert("잘못된 접근입니다.");
			} else {
				parent.document.location.href = location.protocol+'//'+location.host;
			}
		}
	});
	
	// 폼의 전송을 시도할 때의 유효성 검사
	$('#join').find('input[type="submit"]').click(function(event) {
		switch(validation.isValidPassword(pw, repw)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid').html("비밀번호를 " + MESSAGE_PLZ_INPUT);
			event.preventDefault();
			return;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid').html("비밀번호는 " + PASSWORD_MAXLENGTH + MESSAGE_UNDER_MIN_LENGTH);
			event.preventDefault();
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid').html("비밀번호와 확인이 " + MESSAGE_NO_MATCH);
			event.preventDefault();
			return;
		}

		$('#isvalid').html('정보를 서버로 전송 중입니다.');
	}); // end .click(function(event)
}); // end .ready(function()