$(document).ready(function() {
	// 폼을 ajax로 연결
	$('#container').ajaxForm({
		url:location.protocol+'//'+location.host+"/account/deleting",
		dataType:'json',
		beforeSubmit:function() {

		},
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['isvalid']) {
				alert("잘못된 접근입니다.");
			} else {
				alert("계정이 삭제되었습니다.")
				parent.document.location.href = location.protocol+'//'+location.host;
			}
		}
	});
});