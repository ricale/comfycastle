$(document).ready(function() {
	var MESSAGE_VALID = "사용 가능합니다.",
	    MESSAGE_PLZ_INPUT = "입력해주세요";
	    MESSAGE_OVER_MAX_LENGTH = "자 이상 입력해주세요.",
	    MESSAGE_MAIL_NO_MATCH_FORM = "형식을 맞춰주세요. (ab@cd.ef)";

	var validation = new RICALE.AccountValidation(),
	    mail = $('#container').find('input[name="mail"]'),
	    name = $('#container').find('input[name="name"]'),
	    urlExistMail = location.protocol+'//'+location.host+"/account/exist_mail/" + $('#container').find('input[name="id"]').val();

	// 이름의 유효성 검사
	// 유효하지 않은 이름이 입력되었다면 메시지를 출력한다.
	$('#container').find('input[name="name"]').bind('blur', function() {
		switch(validation.isValidName(name)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid_name').html('');
			break;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid_name').html(NAME_MAXLENGTH + MESSAGE_OVER_MAX_LENGTH);
			break;
		case RICALE.Const.VALID:
			$('#isvalid_name').html(MESSAGE_VALID);
			break;
		}
	});

	// 메일의 유효성 검사.
	// ajax를 통한 중복 검사도 포함하며, 유효하지 않은 메일이 입력되었다면 메시지를 출력한다.
	$('#container').find('input[name="checkmail"]').click(function() {
		switch(validation.isValidMail(mail)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid_mail').html(MESSAGE_PLZ_INPUT);
			return;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid_mail').html(MAIL_MAXLENGTH + MESSAGE_OVER_MAX_LENGTH);
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid_mail').html(MESSAGE_MAIL_NO_MATCH_FORM);
			return;
		case RICALE.Const.VALID:
			$.ajax({
				url:urlExistMail,
				dataType:'json',
				type:'POST',
				data: {
					'mail':mailInput.val()
				},
				error:function(result, str, htp) {
					alert("ERROR:"+result+";"+str+";"+htp);
				},
				success:function(result) {
					if(result['exist']) {
						$('#isvalid_mail').html(MESSAGE_MAIL_ALREADY_EXIST);
					} else {
						$('#isvalid_mail').html(MESSAGE_VALID);
					}
				}
			});
			break;
		}
	});

	// 회원정보 수정 폼을 ajax로 연결
	$('#container').ajaxForm({
		url:location.protocol+'//'+location.host+"/account/editing",
		dataType:'json',
		beforeSubmit:function() {

		},
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['isvalid']) {
				$('#isvalid').html('비밀번호가 올바르지 않습니다.');
			} else {
				if(result['send']) {
					alert("변경된 메일로 인증 메일이 발송되었습니다. 인증이 완료되면 로그인이 가능합니다.");
					parent.document.location.href = location.protocol+'//'+location.host+'/account/logout';
				}
				parent.document.location.href = location.protocol+'//'+location.host;
			}
		}
	});
	
	// 폼의 전송을 시도할 때의 유효성 검사
	// 이름, 메일의 유효성을 체크한다.
	$('#container').find('input[type="submit"]').click(function(event) {
		event.preventDefault();

		switch(validation.isValidName(name)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid').html("이름을 " + MESSAGE_PLZ_INPUT);
			return;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid').html("이름은 " + NAME_MAXLENGTH + MESSAGE_OVER_MAX_LENGTH);
			return;
		}

		switch(validation.isValidMail(mail)) {
		case RICALE.Const.INVALID_NO_INPUT:
			$('#isvalid').html("메일을 " + MESSAGE_PLZ_INPUT);
			return;
		case RICALE.Const.INVALID_TOO_LONG:
			$('#isvalid').html("메일은 " + MAIL_MAXLENGTH + MESSAGE_OVER_MAX_LENGTH);
			return;
		case RICALE.Const.INVALID_NO_MATCH_FORM:
			$('#isvalid').html("메일의 " + MESSAGE_MAIL_NO_MATCH_FORM);
			return;
		}

		$('#isvalid').html('정보를 서버로 전송 중입니다.');

		// (어떤 방식일지는 알 수 없으나) 입력 단계에서의 중복 검사를 지나치고 값을 넘길 수도 있기 때문에
		// 값을 전송하기 전에 다시 한 번 더 중복 검사를 한다.
		$.ajax({
			url:urlExistMail,
			dataType:'json',
			type:'POST',
			data: {
				'mail':mail.val()
			},
			error:function(result, str, htp) {
				alert("ERROR:"+result+";"+str+";"+htp);
			},
			success:function(result) {
				if(result['exist']) {
					$('#isvalid').html(MESSAGE_MAIL_ALREADY_EXIST);
				} else {
					$('#container').submit();
				}
			}
		});
	});
});
