if(typeof(RICALE) == typeof(undefined)) {
	/**
	 * @namespace 네임스페이스
	 * @auther ricale
 	 * @version 1
 	 * @description 다른 사람의 구현과의 충돌을 피하기 위한 최상위 네임스페이스
	 */
	var RICALE = {};
}

if(typeof(RICALE.Const) == typeof(undefined)) {
	RICALE.Const = {};
}

RICALE.Const.VALID = 0;
RICALE.Const.INVALID_NO_INPUT = 1;
RICALE.Const.INVALID_TOO_LONG = 2;
RICALE.Const.INVALID_NO_MATCH_FORM = 3;
RICALE.Const.INVALID_TOO_SHORT = 4;
RICALE.Const.INVALID_NO_MATCH_FORM_FIRST_CHARACTER = 5;

RICALE.Const.ID_MINLENGTH = 4;
RICALE.Const.ID_MAXLENGTH = 15;
RICALE.Const.PASSWORD_MAXLENGTH = 20;
RICALE.Const.NAME_MAXLENGTH = 15;
RICALE.Const.MAIL_MAXLENGTH = 50;

RICALE.AccountValidation = function() {

}

RICALE.AccountValidation.prototype = {
	isValidId: function(idInput) {
		var id = idInput.val();

		if(id.length == 0) {
			return RICALE.Const.INVALID_NO_INPUT;
		} else if(id.length < RICALE.Const.ID_MINLENGTH) {
			return RICALE.Const.INVALID_TOO_SHORT;
		} else if(id.length > RICALE.Const.ID_MAXLENGTH) {
			return RICALE.Const.INVALID_TOO_LONG;
		} else if(!id.match(/^[-_a-zA-Z0-9]+(\.[-_a-zA-Z0-9]+)*$/)) {
			return RICALE.Const.INVALID_NO_MATCH_FORM;
		} else if(!id.match(/^[a-zA-Z]/)) {
			return RICALE.Const.INVALID_NO_MATCH_FORM_FIRST_CHARACTER;
		} else {
			return RICALE.Const.VALID;
		}
	},

	isValidPassword: function(pwInput, reInput) {
		var pw = pwInput.val(),
		    re = reInput.val();

		if(pw.length == 0) {
			return RICALE.Const.INVALID_NO_INPUT;
		} else if(pw.length > RICALE.Const.PASSWORD_MAXLENGTH) {
			return RICALE.Const.INVALID_TOO_LONG;
		} else if(pw != re) {
			return RICALE.Const.INVALID_NO_MATCH_FORM;
		} else {
			return RICALE.Const.VALID;
		} // end if
	},

	isValidName: function(nameInput) {
		var name = nameInput.val();

		if(name.length == 0) {
			return RICALE.Const.INVALID_NO_INPUT;
		} else if(name.length > RICALE.Const.NAME_MAXLENGTH) {
			return RICALE.Const.INVALID_TOO_LONG;
		} else {
			return RICALE.Const.VALID;
		} // end if
	},

	isValidMail: function(mailInput) {
		var mail = mailInput.val();

		if(mail.length == 0) {
			return RICALE.Const.INVALID_NO_INPUT;
		} else if(mail.length > RICALE.Const.MAIL_MAXLENGTH) {
			return RICALE.Const.INVALID_TOO_LONG;
		} else if(!mail.match(/^[-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)+$/)) {
			return RICALE.Const.INVALID_NO_MATCH_FORM;
		} else {
			return RICALE.Const.VALID;
		} // end if
	}
}