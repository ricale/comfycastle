$(document).ready(function() {
	$('#delete').find('input[type="submit"]').click(function(event) {		
		var message = $('#delete').find('#isvalid'),
		    number  = $('#delete').find('input[name="number"]').val(),
		    pw      = $('#delete').find('input[name="pw"]').val();
		    
		event.preventDefault();

		if(pw != undefined && pw.length == 0) {
			message.html('비밀번호를 입력하세요.');
			return;
		}
		
		$.ajax({
			url:location.protocol+'//'+location.host+'/board/deleting',
			dataType:'json',
			type:'POST',
			data: {
				'number':number,
				'pw':pw
			},
			error:function(result, str, htp) {
				alert("ERROR:"+result+";"+str+";"+htp);
			},
			success:function(result) {
				if(!result['isvalid']) {
					alert("잘못된 접근입니다.");
				} else {
					parent.document.location.href = '/board/';
				}
			}
		});
	});
});