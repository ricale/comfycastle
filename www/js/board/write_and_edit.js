$(document).ready(function() {

	var url = location.protocol+'//'+location.host,
		number  = $('#write').find('input[name="number"]').val(),
		isReply = $('#write').find('input[name="isreply"]').val();

	if(number == "") {
		url += '/board/writing';
	} else {
		if(isReply == "") {
			url += '/board/editing';
		} else {
			url += '/board/writing/reply';
		}
	}

	$('#write').ajaxForm({
		url:url,
		dataType:'json',
		error:function(result, str, htp) {
			alert("ERROR:"+result+";"+str+";"+htp);
		},
		success:function(result) {
			if(!result['isvalid']) {
				alert("잘못된 접근입니다.");
			} else {
				parent.document.location.href = '/board/'+result['number'];
			}
		}
	});

	$('#write').find('input[type="submit"]').click(function(event) {
		var message = $('#write').find('#isvalid'),
		    name    = $('#write').find('input[name="name"]').val(),
		    pw      = $('#write').find('input[name="pw"]').val(),
		    title   = $('#write').find('input[name="title"]').val(),
		    content = $('#write').find('textarea[name="content"]').val();
		
		if(name != undefined && name.length == 0) {
			message.html('이름을 입력하세요.');
			event.preventDefault();
			return;
		}
		
		if(pw != undefined && pw.length == 0) {
			message.html('비밀번호를 입력하세요.');
			event.preventDefault();
			return;
		}
		    
		if(title.length == 0) {
			message.html('제목을 입력하세요.');
			event.preventDefault();
			return;
		}
		
		if(content.length == 0) {
			message.html('내용을 입력하세요.');
			event.preventDefault();
			return;
		}
	});
});